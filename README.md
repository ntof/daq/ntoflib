# n_ToF library

n_ToF acquisition file parser library.

# Installing

This library is packaged as an RPM suitable for CERN CentOS7 distribution:
```bash
# Add m_ToF repository to your distro
yum-config-manager --add-repo http://ntofci.web.cern.ch/ntofci/distfiles/cc7/nToF-Common.repo
yum-config-manager --add-repo http://ntofci.web.cern.ch/ntofci/distfiles/ntof/cc7/nToF.repo

# Install ntoflib
yum install ntoflib
```

## Installing on lxplus

To clone/compile on lxplus:
```bash
git clone https://gitlab.cern.ch/ntof/daq/ntoflib.git
mkdir ntoflib/build && cd ntoflib/build
cmake .. -DCMAKE_INSTALL_PREFIX=
make -j4 install DESTDIR=instroot

export NTOFDIR=$(pwd)/instroot
```


## Installing on local machine

To clone/compile on local machine:
```bash
git clone https://gitlab.cern.ch/ntof/daq/ntoflib.git
mkdir ntoflib/build && cd ntoflib/build
cmake .. -DCMAKE_INSTALL_PREFIX=
make -j4 install DESTDIR=instroot
```
add this to your .bashrc:
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:<pathtontoflib>/ntoflib/build/instroot/lib64


# Building

A prebuilt docker environment is ready for this project, to use it:
```bash
# Only needed first time to fetch the docker image
docker pull gitlab-registry.cern.ch/ntof/daq/builder:latest
docker tag gitlab-registry.cern.ch/ntof/daq/builder ntoflib/builder

# Start a temporary container, binding current directory inside
docker run -v "$PWD:$PWD" -w "$PWD" --rm -it ntoflib/builder /bin/bash

rm -rf build && mkdir build
cd build && cmake3 .. -DTESTS=ON

make -j4
```

# Testing

To run the tests (from the docker container):
```bash
# Enable and compile the tests
cd build
cmake3 .. -DTESTS=ON
make

# Get a kerberos token to access castor
kinit _my-login_@CERN.CH

# Run the tests
./tests/test_all
```

# Debugging

Deployed application debugging can be achieved by installing *ntoflib-debuginfo* package:
```bash
yum install ntoflib-debuginfo
```
