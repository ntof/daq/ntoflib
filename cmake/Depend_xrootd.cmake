
include(Tools)

if(CASTOR)

find_library(XROOTD_LIBRARIES XrdPosix)
find_library(XROOTDCL_LIBRARIES XrdCl)
find_path(XROOTD_INCLUDE_DIRS XrdPosix/XrdPosixXrootd.hh PATH_SUFFIXES xrootd)
assert(XROOTD_LIBRARIES AND XROOTDCL_LIBRARIES AND XROOTD_INCLUDE_DIRS
    MESSAGE "Failed to find xrootd package")

list(APPEND XROOTD_LIBRARIES ${XROOTDCL_LIBRARIES})

endif(CASTOR)
