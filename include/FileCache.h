/*
** Copyright (C) 2018 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-10-11T11:19:00+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef FILECACHE_H__
#define FILECACHE_H__

#include <map>
#include <memory>
#include <string>

#include <boost/filesystem.hpp>
#include <boost/interprocess/sync/file_lock.hpp>

namespace ntof {
namespace lib {

/**
 * @brief file caching utility
 */
class FileCache
{
public:
    /**
     * @param[in] basedir base directory for cache files
     */
    FileCache(size_t maxSize, const boost::filesystem::path &baseDir);

    class Lock;
    typedef std::shared_ptr<Lock> SharedLock;
    typedef std::map<std::string, std::weak_ptr<Lock>> LockCtnr;

    /**
     * @brief return current basedir
     * @details set an empty basedir to disable filecache.
     */
    inline const boost::filesystem::path &baseDir() const { return m_baseDir; }

    /**
     * @brief modify base directory for cache files
     * @param[in] baseDir new base directory
     *
     * @details any existing files in old baseDir will be left by FileCache,
     * it can still be modified by Readers.
     */
    void setBaseDir(const boost::filesystem::path &baseDir);

    /**
     * @brief return max cache size in bytes
     * @details if zero files will be cleaned-up whenever unlocked
     */
    inline size_t maxSize() const { return m_maxSize; }

    /**
     * @brief set maxSize
     */
    void setMaxSize(size_t size);

    /**
     * @brief lock a file for reading
     * @param[in] name file name to lock
     * @param[out] err 0 on success, an errno error on failure
     * @return a cached file object
     *
     * @details on failure err can be:
     *   - -EAGAIN: file is locked for writing
     *   - -ENOENT: no such file or directory
     */
    SharedLock readLock(const std::string &name, int &err);

    /**
     * @brief create a new file
     * @param[in] name file name to create
     * @param[in] size size of the file that will be written
     * @param[out] err 0 on success, an errno error on failure.
     * @return a cached file object
     *
     * @details on failure err can be:
     *   - -EAGAIN: failed to lock file
     *   - -EACCES: permission denied
     *   - -ENOENT: baseDir is not set
     */
    SharedLock create(const std::string &name, size_t size, int &err);

    class Lock
    {
    public:
        ~Lock();

        enum State
        {
            UNLOCKED,
            SHARED,
            EXCLUSIVE
        };

        inline State state() const { return m_state; }
        inline const boost::filesystem::path &fullpath() const
        {
            return m_fullpath;
        }
        inline const std::string &filename() const { return m_filename; }

    protected:
        explicit Lock(FileCache &cache);

        int init(const std::string &filename, bool exclusive);

        friend class FileCache;
        boost::interprocess::file_lock m_lock;
        State m_state;
        boost::filesystem::path m_fullpath;
        std::string m_filename;
        FileCache &m_cache;
    };

protected:
    friend class Lock;
    void cleanup(size_t requestedSize);

    size_t m_maxSize;
    boost::filesystem::path m_baseDir;
    LockCtnr m_locks;
};

} // namespace lib
} // namespace ntof

#endif
