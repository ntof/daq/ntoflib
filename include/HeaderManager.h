/*
 * HeaderManager.h
 *
 *  Created on: Jan 12, 2015
 *      Author: agiraud
 */

#ifndef HEADERMANAGER_H_
#define HEADERMANAGER_H_

#include <cstring>
#include <fstream>
#include <list>
#include <map>
#include <string>
#include <vector>

#include <stdint.h>

#ifndef __off_t
#define __off_t off_t
#endif

namespace ntof {
namespace lib {

/*
 * \brief Container to list the details of a header
 */
struct Header
{
    Header(const std::string &title = "",
           int32_t revisionNumber = -1,
           const std::string &detectorType = "",
           int32_t detectorId = -1,
           int32_t eventNumber = -1,
           __off_t pos = -1,
           int32_t file = -1,
           int32_t id = -1,
           bool isOnCastor = false) :
        title(title),
        revisionNumber(revisionNumber),
        detectorType(detectorType),
        detectorId(detectorId),
        eventNumber(eventNumber),
        pos(pos),
        file(file),
        id(id),
        isOnCastor(isOnCastor)
    {}

    std::string title;        //!< Title of the header
    int32_t revisionNumber;   //!< Revision number
    std::string detectorType; //!< Type of detector
    int32_t detectorId;       //!< ID of the detector
    int32_t eventNumber;      //!< ID of the event
    __off_t pos;              //!< Position of the header in the file
    int32_t file;             //!< Handler of the file
    int32_t id;               //!< ID of the header
    bool isOnCastor;          //!< Allow to know the reader to use
};

struct FILE_INFO
{
    enum Flag
    {
        ON_CASTOR = 0x01,  // Allow to know if the file is on CASTOR
        NO_CACHE = 0x02,   // Allow to know if file can be cached
        IS_FINISHED = 0x04 // The files has a ".finished" extension
    };

    FILE_INFO(int32_t file = -1,
              __off_t size = -1,
              const std::string &fullpath = "",
              const std::string &filename = "",
              bool isFinished = false,
              int32_t runNumber = -1,
              int32_t segmentNumber = -1,
              bool isOnCastor = false,
              std::string extension = "",
              uint32_t flags = 0) :
        file(file),
        size(size),
        fullpath(fullpath),
        filename(filename),
        isFinished(isFinished),
        runNumber(runNumber),
        segmentNumber(segmentNumber),
        isOnCastor(isOnCastor),
        extension(extension),
        flags(flags)
    {}

    void setFlag(Flag flag, bool value = true);
    inline bool hasFlag(Flag flag) const { return (flags & flag) != 0; }

    int32_t file;          //!< File object
    __off_t size;          //!< Size of the file
    std::string fullpath;  //!< Full path of the file
    std::string filename;  //!< Name of the file
    bool isFinished;       //!< (deprecated, use hasFlag(IS_FINISHED))
    int32_t runNumber;     //!< Run number
    int32_t segmentNumber; //!< ID of the file
    bool isOnCastor;       //!< (deprecated, use hasFlag(ON_CASTOR))
    std::string extension; //!< Extension of the file
    uint32_t flags;
};

struct EVENT_INFO
{
    EVENT_INFO(int32_t validatedNumber = 0,
               int32_t acqcCount = 0,
               int32_t file = -1) :
        validatedNumber(validatedNumber), acqcCount(acqcCount), file(file)
    {}

    int32_t validatedNumber; //!< ID of the trigger
    int32_t acqcCount;       //!< Count of ACQC
    int32_t file;            //!< File object
};

struct ACQC_INFO
{
    ACQC_INFO(const std::string &detectorType = "", uint32_t detectorId = 0) :
        detectorType(detectorType), detectorId(detectorId)
    {}

    std::string detectorType; //!< Name of the detector
    uint32_t detectorId;      //!< ID of the detector
};

class HeaderManager
{
public:
    /*!
     *  \brief Get the instance of the HeaderManager object and initialize the
     * instance if the pointer is null \return A pointer on the HeaderManager
     * object
     */
    static HeaderManager *getInstance();

    /*
     * \brief Destructor
     */
    ~HeaderManager();

    /**
     * \brief Add a new header to the list of the headers.
     * \param header the header to add
     */
    void addHeader(Header header);

    /**
     * \brief Clear the list of the headers.
     */
    void clear();

    /*
     * \brief Allow to know if the data we are reading is an Header title
     * \param memblock: string to test
     * \return Return true if it's a header title or false if it's not
     */
    bool isHeaderTitle(char *memblock);

    /*
     * \brief Get the number of event header found in the list
     * \return Return the number of event header found in the list
     */
    int32_t getNumberOfEvents();

    /*
     * \brief Get the ID of the actual ACQC header being read
     * \return Return the ID of the actual ACQC header being read
     */
    int32_t getIdAcqc() const;

    /*
     * \brief Get the ID of the actual EVEH header being read
     * \return Return the ID of the actual EVEH header being read
     */
    int32_t getIdEveh() const;

    /*
     * \brief Get the details relative to the first occurrence of an header
     * \param title: Title of the header
     * \return Return the detail of the next header or an empty header if it
     * can't be found
     */
    Header getHeader(std::string title);

    /*
     * \brief Get the details relative to the next header beyond a define
     * position \param title: Title of the header \param number: The header has
     * to be located beyond this position \return Return the detail of the next
     * header or an empty header if it can't be found
     */
    Header getHeader(std::string title, int32_t number);

    /*
     * \brief Get the details relative to the next header in the list
     * \param title: Title of the header
     * \return Return the detail of the next header or an empty header if it
     * can't be found
     */
    Header getNextHeader(std::string title);

    /*
     * \brief Get the details relative to the previous header in the list
     * \param title: Title of the header
     * \return Return the detail of the previous header or an empty header if it
     * can't be found
     */
    Header getPreviousHeader(std::string title);

    /*
     * \brief Get the details relative to the previous EVEH header
     * \return Return the detail of the previous EVEH header or an empty header
     * if it can't be found
     */
    Header getPreviousEventHeader();

    /*
     * \brief Get the details relative to the actual EVEH header
     * \param reset: Reset the ACQC
     * \return Return the detail of the actual EVEH header or an empty header if
     * it can't be found
     */
    Header getActualEventHeader(bool reset = true);

    /*
     * \brief Get the details relative to the next EVEH header
     * \return Return the detail of the next EVEH header or an empty header if
     * it can't be found
     */
    Header getNextEventHeader();

    /*
     * \brief Get the details relative to the previous ACQC header (without
     * reaching another EVEH) \return Return the detail of the previous ACQC
     * header or an empty header if it can't be found
     */
    Header getPreviousACQCHeader();

    /*
     * \brief Get the details relative to the actual ACQC header (without
     * reaching another EVEH) \return Return the detail of the actual ACQC
     * header or an empty header if it can't be found
     */
    Header getActualACQCHeader();

    /*
     * \brief Get the details relative to the next ACQC header (without reaching
     * another EVEH) \return Return the detail of the next ACQC header or an
     * empty header if it can't be found
     */
    Header getNextACQCHeader();

    /*
     * \brief Get the details relative to the ACQC header which match with
     * detector type and the detector if given in parameter (without reaching
     * another EVEH) \param detectorType: Name of the detector \param
     * detectorId: ID of the detector \return Return the detail of the ACQC
     * header or an empty header if it can't be found
     */
    Header getHeaderACQC(std::string detectorType, int32_t detectorId);

    /*
     * \brief Search for the header with the validated event number
     * \param validEvent: The header has to be located beyond this position
     * \return Return the header with an empty title if the good header has not
     * been found
     */
    Header getValidatedEventHeader(int32_t validEvent);

    /*
     * \brief Get the list of the event number found and the count of ACQC
     * \return Return the list containing all the info loaded
     */
    std::vector<EVENT_INFO> getListOfEvents();

    /*
     * \brief Get the information of an event number
     * \param key: event number used as key into the map
     * \return Return the information of the event
     */
    EVENT_INFO getEventInfo(int32_t key);

    /*
     * \brief Get the information of an event number
     * \param key: event number used as key into the map
     * \return Return the information of the event
     */
    std::vector<ACQC_INFO> getACQCList(int32_t key);

    /*
     * \brief Get the next event key
     * \return Return the next key or -1 if there is no more
     */
    int32_t getNextEventNumber();

    /*
     * \brief Get the previous event key
     * \return Return the previous key or -1 if there is no more
     */
    int32_t getPreviousEventNumber();

    /*
     * \brief Get the first event number in a file
     * \param file: Handler of the file related to the search
     * \return Return the event number or -1 if there is no one
     */
    int32_t getFirstEventOf(int32_t file);

    /*
     * \brief Display the list of the headers found
     */
    void toString();

protected:
    /*
     * \brief Constructor
     */
    HeaderManager();

    /*
     * \brief Reset properly the variable actualACQC
     */
    void resetAcqcHeader();

    /*
     * \brief Search for the header in the single ones
     * \param title: name of the header
     * \param number: The header has to be located beyond this position
     * \return Return the header with an empty title if the good header has not
     * been found
     */
    Header getSingleHeader(const std::string &title, int32_t number);

    /*
     * \brief Search for the header in the single ones
     * \param title: name of the header
     * \param number: The header has to be located beyond this position
     * \return Return the header with an empty title if the good header has not
     * been found
     */
    Header getEventHeader(const std::string &title, int32_t number);

    /*
     * \brief Search for the header in the single ones
     * \param title: name of the header
     * \param number: The header has to be located beyond this position
     * \return Return the header with an empty title if the good header has not
     * been found
     */
    Header getDataHeader(const std::string &title, int32_t number);

    static HeaderManager *instance_;  //!< HeaderManager instance
    std::list<Header> singleHeaders_; //!< List of the single headers
    std::map<int32_t, std::vector<Header>> eventsHeaders_; //!< List of the
                                                           //!< headers related
                                                           //!< to the event
    Header actualEVEH_; //!< EVEH header being read
    Header actualACQC_; //!< ACQC header being read
    int32_t nbEVEH_;    //!< ID of the actual event header being read
    int32_t nbACQC_;    //!< ID of the actual ACQC header being read
    std::map<int32_t, std::vector<Header>>::iterator
        evehIterator_;     //!< Keep the position of the last EVEH read
    int32_t acqcIterator_; //!< Keep the position of the last ACQC read
    // int32_t lastEventAdded_;                                             //!<
    // Keep the last event added in memory
};

} // namespace lib
} // namespace ntof

#endif
