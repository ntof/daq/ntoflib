/*
** Copyright (C) 2018 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-10-03T09:51:46+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef NOTIFICATION_H_
#define NOTIFICATION_H_

#include <stdint.h>

#include "NtofLibException.h"

namespace ntof {
namespace lib {

/**
 * @details mostly exist to create a namespace
 */
class Notification
{
public:
    enum State
    {
        FILE_FETCH,
        HEADER_PARSE,
        ACQC_INIT,
        ADDH_INIT,
        MODH_INIT
    };

    class Listener
    {
    public:
        virtual ~Listener() {};

        /**
         * @brief progress notification callback
         * @param[in] state    ntoflib current state
         * @param[in] progress progres to reach total
         * @param[in] total    maximum progress value
         * @return true on normal operation, false if current operation
         * should be aborted
         *
         * @details progrsess and total are both set to zero when no
         * estimations can be made by the ntoflib.
         */
        virtual bool onProgress(State state, size_t progress, size_t total) = 0;
    };

    /**
     * @brief handy function to send notification events
     * @details this function is mostly for ntoflib internal use.
     *
     * @TODO: do not export
     */
    static inline void notify(Listener *l,
                              State state,
                              size_t progress = 0,
                              size_t total = 0)
    {
        if (!l)
            return;
        else if (!l->onProgress(state, progress, total))
            throw NtofLibException();
    }
};

} // namespace lib
} // namespace ntof

#endif /* NOTIFICATION_H_ */
