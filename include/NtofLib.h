/*
 * NtofLib.h
 *
 *  Created on: Jan 22, 2015
 *      Author: agiraud
 */

#ifndef NTOFLIB_H_
#define NTOFLIB_H_

#include <memory>
#include <vector>

#include <stdint.h>

#include "HeaderManager.h"
#include "Notification.h"
#include "NtofLibConfig.h"
#include "NtofLibException.h"

namespace ntof {
namespace lib {

class Reader;
class ReaderStructRCTR;
class ReaderStructMODH;
class ReaderStructEVEH;
class ReaderStructACQC;
class ReaderStructBEAM;
class ReaderStructINDX;
class ReaderStructINFO;
class ReaderStructSLOW;
class ReaderStructADDH;

#ifdef CASTOR
class ReaderCastorCached;
#endif

class ReaderLocal;
class FileCache;

extern const std::string VERSION; //!< Version of the library

/* TYPEDEF */
typedef struct
{
    bool isValid;
    std::string message;
} CHECK_LIST;

class NtofLib
{
public:
    typedef std::vector<std::string> FileList;
    typedef std::vector<FILE_INFO> FileInfoList;
    /**
     * \brief Constructor.
     */
    NtofLib();
    NtofLib(const NtofLib &other) = delete;
    NtofLib &operator=(const NtofLib &other) = delete;
    NtofLib(NtofLib &&other);

    /**
     * \brief Constructor.
     * \param path Path of the file to read.
     */
    explicit NtofLib(const std::string &path);

    /**
     * \brief Constructor.
     * \param paths: Paths of the files to read
     */
    explicit NtofLib(const FileList &paths);

    /**
     * \brief Destructor.
     */
    virtual ~NtofLib();

    /**
     * \brief Initialize the reading of the file
     * \param paths: Paths of the files to read
     * \param closeOldFiles: Close the files already open by default true
     */
    void readFiles(const FileList &paths, bool closeOldFiles = true);

    /**
     * \brief Initialize the reading of the file
     * \param path Path of the file you want to read
     * \param closeOldFiles: Close the files already open by default true
     */
    void readFile(const std::string &pPath, bool closeOldFiles = true);

    /*
     * \brief Close all the files opened
     */
    void closeAllFiles();

    /*
     * \brief Get the details from the name of the file
     * \param path: Full path of the file
     * \return Return the details of the file
     */
    FILE_INFO getFileInfo(const std::string &path) const;

    /*
     * \brief Get the details from the name of the file
     * \param file: File handler
     * \return Return the details of the file
     */
    FILE_INFO getFileInfo(int32_t file) const;

    /**
     * \brief Allow to know if the file is an index file.
     * \return Return true if the file is an index file or false if it isn't.
     */
    bool isIDX();

    /**
     * \brief Initialize the reader of the previous event.
     * \return Return true if the event is ready to be read or false if there is
     * no more event in the file.
     */
    bool getPreviousEvent();

    /**
     * \brief Initialize the reader of the next event.
     * \return Return true if the event is ready to be read or false if there is
     * no more event in the file.
     */
    bool getNextEvent();

    /**
     * \brief Jump directly to a validated event
     * \param validatedEvent: id of the validated event to jump
     * \return Return true if the event is ready to be read or false if the
     * event doesn't exist in the file.
     */
    bool getValidatedEvent(int32_t validEvent);

    /**
     * \brief Jump directly to a particular event.
     * \param event id of the event to jump
     * \return Return true if the event is ready to be read or false if the
     * event doesn't exist in the file.
     */
    bool getEvent(int32_t event);

    /**
     * \brief Initialize the reader of the previous ACQC, according the actual
     * event that is being read. \return Return true if the ACQC is ready to be
     * read or false if there is no more event in the file.
     */
    bool getPreviousACQC();

    /**
     * \brief Initialize the reader of the next ACQC, according the actual event
     * that is being read. \return Return true if the ACQC is ready to be read
     * or false if there is no more event in the file.
     */
    bool getNextACQC();

    /**
     * \brief Initialize the reader of the next ACQC, according the actual event
     * that is being read. \param acqc id to jump on \return Return true if the
     * ACQC is ready to be read or false if there is no more event in the file.
     */
    bool getACQC(int32_t acqc);

    /**
     * \brief Initialize the reader of the next ACQC, according the actual event
     * that is being read. \param detectorType The type of the detector \param
     * detectorId The Id of the detector \return Return true if the ACQC is
     * ready to be read or false if there is no more event in the file.
     */
    bool getACQC(const std::string &detectorType, int32_t detectorId);

    /*
     * \brief Get the number of events found
     * \return Return the number of events found
     */
    int32_t getNumberOfEvents();

    /**
     * \brief Get the RCTR reader
     * \return Return the pointer of the reader or throw a
     * ntof::lib::NtofLibException if the pointer is null
     */
    ReaderStructRCTR *getRCTR();

    /**
     * \brief Get the MODH reader
     * \return Return the pointer of the reader or throw a
     * ntof::lib::NtofLibException if the pointer is null
     */
    ReaderStructMODH *getMODH();

    /**
     * \brief Get the EVEH reader
     * \return Return the pointer of the reader or throw a
     * ntof::lib::NtofLibException if the pointer is null
     */
    ReaderStructEVEH *getEVEH();

    /**
     * \brief Get the ACQC reader
     * \return Return the pointer of the reader or throw a
     * ntof::lib::NtofLibException if the pointer is null
     */
    ReaderStructACQC *getACQC();

    /**
     * \brief Get the BEAM reader
     * \return Return the pointer of the reader or throw a
     * ntof::lib::NtofLibException if the pointer is null
     */
    ReaderStructBEAM *getBEAM();

    /**
     * \brief Get the INDX reader
     * \return Return the pointer of the reader or throw a
     * ntof::lib::NtofLibException if the pointer is null
     */
    ReaderStructINDX *getINDX();

    /**
     * \brief Get the INFO reader
     * \return Return the pointer of the reader or throw a
     * ntof::lib::NtofLibException if the pointer is null
     */
    ReaderStructINFO *getINFO();

    /**
     * \brief Get the SLOW reader
     * \return Return the pointer of the reader or throw a
     * ntof::lib::NtofLibException if the pointer is null
     */
    ReaderStructSLOW *getSLOW();

    /**
     * \brief Get the ADDH reader
     * \return Return the pointer of the reader or throw a
     * ntof::lib::NtofLibException if the pointer is null
     */
    ReaderStructADDH *getADDH();

    /**
     * \brief Give the version of the lib as a string.
     * \return the version of the lib.
     */
    const std::string &getVersion();

    /**
     * \brief Get the reader
     * \param isOnCastor: Define the reader to return
     * \return Return the pointer of the reader or throw a
     * ntof::lib::NtofLibException if the pointer is null
     */
    ntof::lib::Reader *getReader(bool isOnCastor);

    /*
     * \brief Get the name of the file opened
     * \return Return the name of the file opened
     */
    std::string getFilename();

    /**
     * \brief All to know if the path is located on CASTOR
     * \param path The path to test
     * \return Return true if the file is on CASTOR or false if it isn't
     */
    bool isOnCastor(const std::string &path);

    /**
     * \brief All to know the ID of the actual EVEH
     * \return Return the ID of the EVEH in the file
     */
    int64_t getActualEvent();

    /*
     * \brief Allow to know if the header is defined
     * \return Return true if the header is defined or false if it's not
     */
    bool isDefinedRCTR();

    /*
     * \brief Allow to know if the header is defined
     * \return Return true if the header is defined or false if it's not
     */
    bool isDefinedMODH();

    /*
     * \brief Allow to know if the header is defined
     * \return Return true if the header is defined or false if it's not
     */
    bool isDefinedEVEH();

    /*
     * \brief Allow to know if the header is defined
     * \return Return true if the header is defined or false if it's not
     */
    bool isDefinedACQC();

    /*
     * \brief Allow to know if the header is defined
     * \return Return true if the header is defined or false if it's not
     */
    bool isDefinedBEAM();

    /*
     * \brief Allow to know if the header is defined
     * \return Return true if the header is defined or false if it's not
     */
    bool isDefinedINDX();

    /*
     * \brief Allow to know if the header is defined
     * \return Return true if the header is defined or false if it's not
     */
    bool isDefinedINFO();

    /*
     * \brief Allow to know if the header is defined
     * \return Return true if the header is defined or false if it's not
     */
    bool isDefinedSLOW();

    /*
     * \brief Allow to know if the header is defined
     * \return Return true if the header is defined or false if it's not
     */
    bool isDefinedADDH();

    /*
     * \brief Get the list of the files opened
     * \return Return a vector which contains the list of all the full paths
     * opened
     */
    std::vector<std::string> getFilenames();

    /*
     * \brief Get the list of the event number found and the count of ACQC
     * \return Return the list containing all the info loaded
     */
    std::vector<EVENT_INFO> getListOfEvents();

    /*
     * \brief Know if a previous event is available
     * \return Return true if a previous event is available
     */
    bool hasPreviousEvent();

    /*
     * \brief Get the information relative to the previous event
     * \return Return the data relative to the previous event
     */
    EVENT_INFO getPreviousEventInfo();

    /*
     * \brief Know if a next event is available
     * \return Return true if a next event is available
     */
    bool hasNextEvent();

    /*
     * \brief Get the information relative to the next event
     * \return Return the data relative to the next event
     */
    EVENT_INFO getNextEventInfo();

    /*
     * \brief Get the information relative to the actual event
     * \return Return the data relative to the actual event
     */
    EVENT_INFO getEventInfo();

    /*
     * \brief Check if the file can be opened according the ones already opened
     * \param path: Full path of the file to add
     * \return Return a structure with a boolean to know if the files can be
     * opened or not and if not an error message
     */
    CHECK_LIST checkFile(const std::string &path);

    /*
     * \brief Check if all the files can be opened according the ones already
     * opened \param paths: List of the full paths to add \return Return a
     * structure with a boolean to know if the files can be opened or not and if
     * not an error message
     */
    CHECK_LIST checkFiles(const FileList &paths);

    /*
     * \brief Check if all the files can be opened in once
     * \param paths: List of the full paths to add
     * \return Return a structure with a boolean to know if the files can be
     * opened or not and if not an error message
     */
    CHECK_LIST checkList(const FileList &paths);

    /*
     * \brief Get the information of an event number
     * \param key: event number used as key into the map
     * \return Return the information of the event
     */
    std::vector<ACQC_INFO> getACQCList(int32_t key);

    /*
     * \brief Get the first event number in a file
     * \param fullPath: Full path of the file
     * \return Return the event number or -1 if there is no one
     */
    int32_t getFirstEventOf(const std::string &fullPath);

    /*
     * \brief Display the list of the headers found
     */
    void toString();

#ifdef CASTOR
    /*
     * \brief Get the service class
     * \return Return the service class
     */
    const std::string &getServiceClass() const;

    /*
     * \brief Set the service class
     * \param serviceClass: The service class
     */
    void setServiceClass(const std::string &serviceClass);

    /*
     * \brief Get the hostname of the CASTOR machine
     * \return Return the hostname of the CASTOR machine
     */
    const std::string &getStageHost() const;

    /*
     * \brief Set the hostname of the CASTOR machine
     * \param stageHost: The hostname of the CASTOR machine
     */
    void setStageHost(const std::string &stageHost);
#endif

    /**
     * @brief set the notification callback
     * @param callback a callback meant to display progress
     *
     * @details set to 0 to remove a callback
     */
    void setNotificationListener(Notification::Listener *listener);

    inline Notification::Listener *notificationListener() { return notif_; }

    /**
     * @brief Get the FileCache instance
     * @details useful to modify the path or size of the cache
     */
    inline std::shared_ptr<FileCache> getFileCache() const { return cache_; }

protected:
    /**
     * \brief All to know if the path is an index file
     * \param path The path to test
     * \return Return true if the file is an index file or false if it isn't
     */
    bool isIDX(const std::string &path);

    /*
     * \brief Allow to initialize the header
     * \param header Details of the header
     */
    void initRCTR(const Header &header);

    /*
     * \brief Allow to initialize the header
     * \param header Details of the header
     */
    void initMODH(const Header &header);

    /*
     * \brief Allow to initialize the header
     * \param header Details of the header
     */
    void initEVEH(const Header &header);

    /*
     * \brief Allow to initialize the header
     * \param header Details of the header
     */
    void initACQC(const Header &header);

    /*
     * \brief Allow to initialize the header
     * \param header Details of the header
     */
    void initBEAM(const Header &header);

    /*
     * \brief Allow to initialize the header
     * \param header Details of the header
     */
    void initINDX(const Header &header);

    /*
     * \brief Allow to initialize the header
     * \param header Details of the header
     */
    void initINFO(const Header &header);

    /*
     * \brief Allow to initialize the header
     * \param header Details of the header
     */
    void initSLOW(const Header &header);

    /*
     * \brief Allow to initialize the header
     * \param header Details of the header
     */
    void initADDH(const Header &header);

    /*
     * \brief Allow to check if a file exists
     * \param name: full path of the file
     * \return Return true if the file exists of false in the other case
     */
    inline bool fileExists(const std::string &name);

    /*
     * \brief Split the filename to access the run and segment numbers quickly
     * \param filename: Name of the file to split
     * \return Return the filename split
     */
    FILE_INFO splitFilename(const std::string &filename);

    /*
     * \brief Initialize the RCTR and the MODH (+ INDX if possible)
     */
    void initHeaders();

    /*
     * \brief Add a list of files to the open files
     * \param paths: The list of full paths to open
     */
    void readMoreFiles(const FileList &paths);

    /*
     * \brief Add another file to the list of open files
     * \param path: Full path of the file to open
     */
    void readAnotherFile(const FILE_INFO &info);

    /**
     * @brief prepare several files for loading
     * @param[in] paths paths to add in fileInfo
     * @param[out] fileInfo output fileInfo list
     * @param[in] checkLoaded check already loaded files, or only input ones
     * @throws NtofLibException on inconsistent FILE_INFO (cache conflict ...)
     */
    void prepareFiles(const FileList &paths,
                      FileInfoList &fileInfo,
                      bool checkLoaded) const;

    ReaderStructRCTR *rctr_;           //!< Pointer to reader of the header
    ReaderStructMODH *modh_;           //!< Pointer to reader of the header
    ReaderStructEVEH *eveh_;           //!< Pointer to reader of the header
    ReaderStructACQC *acqc_;           //!< Pointer to reader of the header
    ReaderStructBEAM *beam_;           //!< Pointer to reader of the header
    ReaderStructINDX *indx_;           //!< Pointer to reader of the header
    ReaderStructINFO *info_;           //!< Pointer to reader of the header
    ReaderStructSLOW *slow_;           //!< Pointer to reader of the header
    ReaderStructADDH *addh_;           //!< Pointer to reader of the header
    std::vector<FILE_INFO> filenames_; //!< Contains all the files opened
    std::shared_ptr<FileCache> cache_;
#ifdef CASTOR
    std::unique_ptr<ReaderCastorCached> readerCastor_; //!< CASTOR reader
#endif
    std::unique_ptr<ReaderLocal> readerLocal_; //!< Local reader
    Notification::Listener *notif_;
};

} /* namespace lib */
} /* namespace ntof */

#include "ReaderStructACQC.h"
#include "ReaderStructADDH.h"
#include "ReaderStructBEAM.h"
#include "ReaderStructEVEH.h"
#include "ReaderStructINDX.h"
#include "ReaderStructINFO.h"
#include "ReaderStructMODH.h"
#include "ReaderStructRCTR.h"
#include "ReaderStructSLOW.h"

#endif /* NTOFLIB_H_ */
