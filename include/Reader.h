/*
 * Reader.h
 *
 *  Created on: Jan 8, 2015
 *      Author: agiraud
 */

#ifndef READER_H_
#define READER_H_

#include <stdint.h>

#include "HeaderManager.h"
#include "Notification.h"

const int32_t HEADER_SIZE = 16; //!< Size of the header bank (in bytes)

namespace ntof {
namespace lib {

/*
 * \brief Type of the data to read
 */
enum Type
{
    INT = 1,
    FLOAT,
    STRING,
    DOUBLESTRING
};

class Reader
{
public:
    /*
     * \brief Destructor of the class
     */
    virtual ~Reader() {}

    /*
     * \brief Get the file of the name opened
     * \return Return the name of the file
     */
    const std::string &getFilename() const;

    /*
     * \brief Get the header manager
     * \return Return the header manager
     */
    HeaderManager &getManager();

    /*
     * \brief Open a file on CASTOR
     * \param fileInfo: Contain all the data related to the file
     * \param notif: Notification callback to display progress
     * \return Return the details of the file opened
     */
    virtual FILE_INFO openFile(FILE_INFO fileInfo,
                               Notification::Listener *notif = 0) = 0;

    /*
     * \brief Initialize the headers list with the headers contains in the file
     * \param file: Handler to the file
     * \param fileSize: Size of the file
     */
    // virtual void readFile(int32_t file, __off_t fileSize) = 0;

    /*
     * \brief Initialize the headers list with the headers contains in the file
     * \param fileInfo: Contain all the data related to the file
     * \param notif: Notification callback to display progress
     */
    virtual void readFile(FILE_INFO fileInfo,
                          Notification::Listener *notif = 0) = 0;

    /*
     * \brief Allow to change the position of the cursor.
     * \param pos: Position you want to go
     * \param file: Handler to the file
     * \return Return the offset of the actual position according the SEEK_SET.
     */
    virtual __off_t setPosition(__off_t pos, int32_t file) = 0;

    /**
     * \brief Read some bytes
     * \param position Location in the file
     * \param buffer buffer to fill
     * \param size The size in bytes
     * \param file Handler to the file
     */
    virtual void read(__off_t position,
                      void *buffer,
                      size_t size,
                      int32_t file) = 0;

    /*
     * \brief Read the next bytes as an character
     * \param position: Location in the file
     * \param file: Handler to the file
     * \return the value that has been read
     */
    char readChar(__off_t position, int32_t file);

    /*
     * \brief Read the next bytes as a string
     * \param position: Location in the file
     * \param size The size in bytes
     * \param file: Handler to the file
     * \return the value that has been read
     */
    std::string readString(__off_t position, int32_t size, int32_t file);

    /*
     * \brief Read the next bytes as uint32_t
     * \param position: Location in the file
     * \param file: Handler to the file
     * \return the value that has been read
     */
    uint32_t readUint32(__off_t position, int32_t file);

    /*
     * \brief Read the next bytes as uint64_t
     * \param position: Location in the file
     * \param file: Handler to the file
     * \return the value that has been read
     */
    uint64_t readUint64(__off_t position, int32_t file);

    /*
     * \brief Read the next bytes as int8_t
     * \param position: Location in the file
     * \param file: Handler to the file
     * \return the value that has been read
     */
    int8_t readInt8(__off_t position, int32_t file);

    /*
     * \brief Read the next bytes as int16_t
     * \param position: Location in the file
     * \param file: Handler to the file
     * \return the value that has been read
     */
    int16_t readInt16(__off_t position, int32_t file);

    /*
     * \brief Read the next bytes as int32_t
     * \param position: Location in the file
     * \param file: Handler to the file
     * \return the value that has been read
     */
    int32_t readInt32(__off_t position, int32_t file);

    /*
     * \brief Read the next bytes as int64_t
     * \param position: Location in the file
     * \param file: Handler to the file
     * \return the value that has been read
     */
    int64_t readInt64(__off_t position, int32_t file);

    /*
     * \brief Read the next bytes as float
     * \param position: Location in the file
     * \param file: Handler to the file
     * \return the value that has been read
     */
    float readFloat(__off_t position, int32_t file);

    /*
     * \brief Read the next bytes as double
     * \param position: Location in the file
     * \param file: Handler to the file
     * \return the value that has been read
     */
    double readDouble(__off_t position, int32_t file);

    /*
     * \brief Read a vector of int8_t
     * \param position: Location in the file
     * \param length: number of element to read
     * \param vector: Vector to store the data
     * \param file: Handler to the file
     * \return the value that has been read
     */
    void readVectorInt8(__off_t position,
                        int64_t length,
                        std::vector<int8_t> &vector,
                        int32_t file);

    /*
     * \brief Read a vector of int16_t
     * \param position: Location in the file
     * \param length: number of element to read
     * \param vector: Vector to store the data
     * \param file: Handler to the file
     * \return the value that has been read
     */
    void readVectorInt16(__off_t position,
                         int64_t length,
                         std::vector<int16_t> &vector,
                         int32_t file);

    /*
     * \brief Read an array of char
     * \param position: Location in the file
     * \param length: number of element to read
     * \param array: Array to store the data
     * \param file: Handler to the file
     * \return the value that has been read
     */
    void readArrayChar(__off_t position,
                       int64_t length,
                       char *array,
                       int32_t file);

    /**
     * @brief Read a single value
     * @param[in] position Location in the file
     * @param[in] file Handler to the file
     * @return the value that has been read
     */
    template<typename t>
    t read(__off_t position, int32_t file);

    /**
     * @brief Read an array of values
     * @param[in] position Location in the file
     * @param[in] length Number of elements to read
     * @param[out] array Array to store the data
     * @param[in] file Handler to the file
     */
    template<typename t>
    void readArray(__off_t position, int64_t length, t *array, int32_t file);
};

} /* namespace lib */
} /* namespace ntof */

#include "Reader.hxx"

#endif /* READERCASTOR_H_ */
