/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-09-15T10:01:50+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef READER_HXX__
#define READER_HXX__

#include <type_traits>

#include "Reader.h"

namespace ntof {
namespace lib {

template<typename t>
t Reader::read(__off_t position, int32_t file)
{
    static_assert(
        std::is_same<t, int32_t>::value || std::is_same<t, int64_t>::value ||
            std::is_same<t, float>::value || std::is_same<t, double>::value,
        "Invalid type for function");

    t ret;
    read(position, &ret, sizeof(t), file);
    return ret;
}

template<typename t>
void Reader::readArray(__off_t position, int64_t length, t *array, int32_t file)
{
    static_assert(
        std::is_same<t, int32_t>::value || std::is_same<t, int64_t>::value ||
            std::is_same<t, float>::value || std::is_same<t, double>::value,
        "Invalid type for function");
    read(position, array, sizeof(t) * length, file);
}

} // namespace lib
} // namespace ntof

#endif
