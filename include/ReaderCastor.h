/*
 * ReaderCastor.h
 *
 *  Created on: Jan 8, 2015
 *      Author: agiraud
 */

#ifndef READERCASTOR_H_
#define READERCASTOR_H_

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <list>
#include <sstream>

#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#include "HeaderManager.h"
#include "NtofLibException.h"
#include "Reader.h"

namespace ntof {
namespace lib {

class ReaderCastorCached;

class ReaderCastor : public Reader
{
public:
    /*
     * \brief Constructor of the class
     */
    ReaderCastor();

    /*
     * \brief Destructor of the class
     */
    virtual ~ReaderCastor();

    /*
     * \brief Open a file on CASTOR
     * \param fileInfo: Contain all the data related to the file
     * \return Return the details of the file opened
     */
    FILE_INFO openFile(FILE_INFO fileInfo,
                       Notification::Listener *notif = 0) override;

    /*
     * \brief Initialize the headers list with the headers contains in the file
     * \param fileInfo: Contain all the data related to the file
     */
    void readFile(FILE_INFO fileInfo,
                  Notification::Listener *notif = 0) override;

    /*
     * \brief Allow to change the position of the cursor.
     * \param pos: Position you want to go
     * \param file: Handler to the file
     * \return Return the offset of the actual position according the SEEK_SET.
     */
    __off_t setPosition(__off_t pos, int32_t file) override;

    /**
     * \brief Read some bytes
     * \param position Location in the file
     * \param buffer buffer to fill
     * \param size The size in bytes
     * \param file Handler to the file
     */
    virtual void read(__off_t position,
                      void *buffer,
                      size_t size,
                      int32_t file) override;

    /*
     * \brief Close a file opened on CASTOR
     * \param file: File to close
     */
    void closeFile(int32_t file);

    /*
     * \brief Get the service class
     * \return Return the service class
     */
    const std::string &getServiceClass() const;

    /*
     * \brief Set the service class
     * \param serviceClass: The service class
     */
    void setServiceClass(const std::string &serviceClass);

    /*
     * \brief Get the hostname of the CASTOR machine
     * \return Return the hostname of the CASTOR machine
     */
    const std::string &getStageHost() const;

    /*
     * \brief Set the hostname of the CASTOR machine
     * \param stageHost: The hostname of the CASTOR machine
     */
    void setStageHost(const std::string &stageHost);

protected:
    friend class ReaderCastorCached; /* requires rawOpenFile */

    /**
     * @brief an open file that does only what it says.
     * @param[in,out] info opened file info.
     *
     * @details does not read the file.
     */
    void rawOpenFile(FILE_INFO &info);

    std::string stageHost_;    //!< Host machine used for CASTOR requests
    std::string serviceClass_; //!< Service class used for CASTOR requests
};
} /* namespace lib */
} /* namespace ntof */

#endif /* READERCASTOR_H_ */
