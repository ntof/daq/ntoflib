/*
** Copyright (C) 2018 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-10-11T23:02:14+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef READERCASTORCACHED_H_
#define READERCASTORCACHED_H_

#include <map>
#include <memory> // shared_ptr

#include "FileCache.h"
#include "HeaderManager.h"
#include "Notification.h"
#include "Reader.h"
#include "ReaderCastor.h"
#include "ReaderLocal.h"

namespace ntof {
namespace lib {

/**
 * @brief castor reader that caches files on disk
 *
 * @details if disk cache is unavailable this reader will act as a castor reader
 */
class ReaderCastorCached : public Reader
{
public:
    explicit ReaderCastorCached(const std::shared_ptr<FileCache> &cache);
    virtual ~ReaderCastorCached();

    FILE_INFO openFile(FILE_INFO fileInfo,
                       Notification::Listener *notif = 0) override;

    void readFile(FILE_INFO fileInfo,
                  Notification::Listener *notif = 0) override;

    __off_t setPosition(__off_t pos, int32_t file) override;

    void read(__off_t position, void *buffer, size_t size, int32_t file) override;

    void closeFile(int32_t file);

    inline const std::string &getServiceClass() const
    {
        return m_castor.getServiceClass();
    }

    inline void setServiceClass(const std::string &serviceClass)
    {
        m_castor.setServiceClass(serviceClass);
    }

    inline const std::string &getStageHost() const
    {
        return m_castor.getStageHost();
    }

    inline void setStageHost(const std::string &stageHost)
    {
        m_castor.setStageHost(stageHost);
    }

protected:
    Reader &readerFor(int32_t file);
    FILE_INFO fetchFile(FILE_INFO &fileInfo, Notification::Listener *notif);

    ReaderCastor m_castor;
    ReaderLocal m_local;
    std::shared_ptr<FileCache> m_cache;
    std::map<int32_t, FileCache::SharedLock> m_files;
};

} // namespace lib
} // namespace ntof

#endif
