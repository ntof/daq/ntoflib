/*
 * Reader.h
 *
 *  Created on: Jan 8, 2015
 *      Author: agiraud
 */
#ifndef READERLOCAL_H_
#define READERLOCAL_H_

#include <cstdlib>
#include <fstream>

#include <sys/stat.h>

#include "HeaderManager.h"
#include "NtofLibException.h"
#include "Reader.h"

namespace ntof {
namespace lib {

class ReaderLocal : public Reader
{
public:
    /*
     * \brief Constructor of the class
     */
    ReaderLocal();

    /*
     * \brief Destructor of the class
     */
    virtual ~ReaderLocal();

    /*
     * \brief Open a file on CASTOR
     * \param fileInfo: Contain all the data related to the file
     * \return Return the details of the file opened
     */
    FILE_INFO openFile(FILE_INFO fileInfo,
                       Notification::Listener *notif = 0) override;

    /*
     * \brief Initialize the headers list with the headers contains in the file
     * \param fileInfo: Contain all the data related to the file
     */
    void readFile(FILE_INFO fileInfo,
                  Notification::Listener *notif = 0) override;

    /*
     * \brief Allow to change the position of the cursor.
     * \param pos: Position you want to go
     * \param file: Handler to the file
     * \return Return the offset of the actual position according the SEEK_SET.
     */
    virtual __off_t setPosition(__off_t pos, int32_t file) override;

    /**
     * \brief Read some bytes
     * \param position Location in the file
     * \param buffer buffer to fill
     * \param size The size in bytes
     * \param file Handler to the file
     */
    virtual void read(__off_t position,
                      void *buffer,
                      size_t size,
                      int32_t file) override;

    /*
     * \brief Close a file opened on CASTOR
     * \param file: File to close
     */
    void closeFile(int32_t file);
};
} // namespace lib
} // namespace ntof

#endif
