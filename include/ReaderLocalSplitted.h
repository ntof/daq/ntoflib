///*
// * ReaderLocalSplitted.h
// *
// *  Created on: Jan 19, 2016
// *      Author: agiraud
// */
//
// #ifndef READERLOCALSPLITTED_H_
// #define READERLOCALSPLITTED_H_
//
// #include <cstdlib>
// #include <fstream>
// #include "HeaderManager.h"
// #include "Reader.h"
// #include "NtofLibException.h"
//
// namespace ntof {
//    namespace lib {
//
//        typedef struct {
//                std::string filename;
//                int32_t file;
//        } RAW_FILE;
//
//        class ReaderLocalSplitted : public Reader {
//            public:
//                /*
//                 * \brief Constructor
//                 * \param filename Location of the file you want to read
//                 */
//                ReaderLocalSplitted(std::string filename);
//
//                /*
//                 * \brief Destructor
//                 */
//                ~ReaderLocalSplitted();
//
//                /*
//                 * \brief Initialize the headers list with the headers
//                 contains in the file
//                 * \param filename: The name of the file to open
//                 * \param file: Handler to the file
//                 * \param fileSize: Size of the file
//                 */
//                void readFile(std::string filename, int32_t file, __off_t
//                fileSize);
//
//                /*
//                 * \brief Allow to change the position of the cursor.
//                 * \param pos: Position you want to go
//                 * \param file: Handler to the file
//                 * \return Return the offset of the actual position according
//                 the SEEK_SET.
//                 */
//                __off_t setPosition(__off_t pos, int32_t file);
//
//                /*
//                 * \brief Read the next bytes as an character
//                 * \param position: Location in the file
//                 * \param file: Handler to the file
//                 * \return the value that has been read
//                 */
//                char readChar(__off_t position, int32_t file);
//
//                /*
//                 * \brief Read the next bytes as 4 characters
//                 * \param position: Location in the file
//                 * \param file: Handler to the file
//                 * \return the value that has been read
//                 */
//                char* readChar4(__off_t position, int32_t file);
//
//                /*
//                 * \brief Read the next bytes as 8 characters
//                 * \param position: Location in the file
//                 * \param file: Handler to the file
//                 * \return the value that has been read
//                 */
//                char* readChar8(__off_t position, int32_t file);
//
//                /*
//                 * \brief Read the next bytes as a string
//                 * \param position: Location in the file
//                 * \param size The size in bytes
//                 * \param file: Handler to the file
//                 * \return the value that has been read
//                 */
//                std::string readString(__off_t position, int32_t size, int32_t
//                file);
//
//                /*
//                 * \brief Read the next bytes as uint32_t
//                 * \param position: Location in the file
//                 * \param file: Handler to the file
//                 * \return the value that has been read
//                 */
//                uint32_t readUint32(__off_t position, int32_t file);
//
//                /*
//                 * \brief Read the next bytes as uint64_t
//                 * \param position: Location in the file
//                 * \param file: Handler to the file
//                 * \return the value that has been read
//                 */
//                uint64_t readUint64(__off_t position, int32_t file);
//
//                /*
//                 * \brief Read the next bytes as int8_t
//                 * \param position: Location in the file
//                 * \param file: Handler to the file
//                 * \return the value that has been read
//                 */
//                int8_t readInt8(__off_t position, int32_t file);
//
//                /*
//                 * \brief Read the next bytes as int16_t
//                 * \param position: Location in the file
//                 * \param file: Handler to the file
//                 * \return the value that has been read
//                 */
//                int16_t readInt16(__off_t position, int32_t file);
//
//                /*
//                 * \brief Read the next bytes as int32_t
//                 * \param position: Location in the file
//                 * \param file: Handler to the file
//                 * \return the value that has been read
//                 */
//                int32_t readInt32(__off_t position, int32_t file);
//
//                /*
//                 * \brief Read the next bytes as int64_t
//                 * \param position: Location in the file
//                 * \param file: Handler to the file
//                 * \return the value that has been read
//                 */
//                int64_t readInt64(__off_t position, int32_t file);
//
//                /*
//                 * \brief Read the next bytes as float
//                 * \param position: Location in the file
//                 * \param file: Handler to the file
//                 * \return the value that has been read
//                 */
//                float readFloat(__off_t position, int32_t file);
//
//                /*
//                 * \brief Read the next bytes as double
//                 * \param position: Location in the file
//                 * \param file: Handler to the file
//                 * \return the value that has been read
//                 */
//                double readDouble(__off_t position, int32_t file);
//
//                /*
//                 * \brief Read a vector of int8_t
//                 * \param position: Location in the file
//                 * \param length: number of element to read
//                 * \param vector: Vector to store the data
//                 * \param file: Handler to the file
//                 * \return the value that has been read
//                 */
//                void readVectorInt8(__off_t position, int64_t length,
//                std::vector<int8_t>& vector, int32_t file);
//
//                /*
//                 * \brief Read a vector of int16_t
//                 * \param position: Location in the file
//                 * \param length: number of element to read
//                 * \param vector: Vector to store the data
//                 * \param file: Handler to the file
//                 * \return the value that has been read
//                 */
//                void readVectorInt16(__off_t position, int64_t length,
//                std::vector<int16_t>& vector, int32_t file);
//
//                /*
//                 * \brief Add a run file to the header manager
//                 * \param filename: Name of the file to add
//                 */
//                void addRunFile(std::string filename);
//
//                /*
//                 * \brief Add a event file to the header manager
//                 * \param filename: Name of the file to add
//                 */
//                void addEventFile(std::string filename);
//
//                /*
//                 * \brief Add a raw file to the header manager
//                 * \param filename: Name of the file to add
//                 */
//                void addRawFile(std::string filename);
//
//                /*
//                 * \brief Load the data of the splitted files in the header
//                 manager
//                 */
//                void readFiles();
//
//            protected:
//                /*
//                 * \brief Try to open all the files already added
//                 */
//                void openFiles();
//
//                int32_t myRunFile_;                     //!< Handler of the
//                run file int32_t myEventFile_;                   //!< Handler
//                of the event file std::list<RAW_FILE> myRawFiles_;        //!<
//                Handler of the raw file std::string myRunFilename_; //!< Name
//                of the run file std::string myEventFilename_;           //!<
//                Name of the event file std::list<std::string> myRawFilenames_;
//                //!< Name of the raw file
//        };
//    }
//}
//
// #endif /* READERLOCALSPLITTED_H_ */
