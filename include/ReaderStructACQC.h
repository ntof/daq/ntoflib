/*
 * ReaderStructACQC.h
 *
 *  Created on: Jan 12, 2015
 *      Author: agiraud
 */

#ifndef READERSTRUCTACQC_H_
#define READERSTRUCTACQC_H_

#include <map>
#include <vector>

#include <stdint.h>

#include "HeaderManager.h"

// const int32_t RESERVE_COEFFICIENT = 1.2;
static const int32_t RESERVE_COEFFICIENT = 1;

namespace ntof {
namespace lib {

class NtofLib;

/*
 * \brief Store the data related to the pulse
 */
typedef struct
{
    __off_t pulsePosition;
    __off_t pulseLength;
} PulseInfo;

/*
 * \brief Store the data of the pulse in order to apply the algorithm of
 * correction
 */
typedef struct
{
    std::vector<int16_t> data;
    __off_t timeStamp;
    __off_t startIndex;
    __off_t position;
} PULSE_DATA_t;

class ReaderStructACQC
{
public:
    /*
     * \brief Constructor
     * \param header Details of the header
     * \param ntoflib give access to the reader
     */
    ReaderStructACQC(const Header &header, NtofLib &ntoflib);

    /*
     * \brief Destructor
     */
    virtual ~ReaderStructACQC();

    /*
     * \brief Allow to initialized the map of the pulse parameters
     */
    void initPulses();

    /*
     * \brief Allow to initialized the reading parameters to get data from the
     * file \return Return true if the next pulse has been found or false if
     * there is no more pulse.
     */
    bool readNextPulse();

    /*
     * \brief Allow to initialized the reading parameters to get data from the
     * map of the pulse \return Return true if the next pulse exist or false if
     * there is no more pulse.
     */
    bool getNextPulse();

    /*
     * \brief Get the title
     * \return Return the title.
     */
    std::string getTitle();

    /*
     * \brief Get the revision number
     * \return Return the revision number.
     */
    int32_t getRevisionNumber();

    /*
     * \brief Get the reserved
     * \return Return the reserved.
     */
    uint32_t getReserved();

    /*
     * \brief Get the length
     * \return Return the length
     */
    uint32_t getLength();

    /*
     * \brief Get the detector type
     * \return Return the detector type
     */
    std::string getDetectorType();

    /*
     * \brief Get the detector id
     * \return Return the detector id
     */
    uint32_t getDetectorId();

    /*
     * \brief Get the channel id
     * \return Return the channel id
     */
    int32_t getChannel();

    /*
     * \brief Get the module id
     * \return Return the module id
     */
    int32_t getModule();

    /*
     * \brief Get the chassis id
     * \return Return the chassis id.
     */
    int32_t getChassis();

    /*
     * \brief Get the stream id
     * \return Return the stream id.
     */
    int32_t getStream();

    /*
     * \brief Get the timestamp of the pulse
     * \return Return the timestamp of the pulse.
     */
    int64_t getPulseTimestamp();

    /*
     * \brief Get the length of the pulse
     * \return Return the length of the pulse.
     */
    int64_t getPulseLength();

    /*
     * \brief Get the data
     * \param id Position of the data
     * \return Return the data.
     */
    int16_t &getData(int64_t id);

    /*
     * \brief Set the channel configuration
     * \return Return true if the configuration has been set correctly
     */
    bool setChannelConfiguration();

    /*
     * \brief Operator [] allows to access to a data index
     * \param id Position of the data
     * \return Return the data.
     */
    int16_t &operator[](int64_t index);

    /*
     * \brief Get the position of the ACQC in the file
     * \return Return the position of the ACQC in the file
     */
    uint64_t getCurrentSeek();

    /*
     * \brief Jump to the pulse at the position given
     * \param pulsePosition: position of the pulse
     */
    void setPulsePosition(int64_t pulsePosition);

    /*
     * \brief Get the id of the actual pulse
     * \return Return the id of the pulse
     */
    int32_t getPulseId() const;

    /*
     * \brief Set the id of the pulse
     * \param pulseId: id to set
     * \return Return true if the id has been set or false in the other case
     */
    bool setPulseId(int32_t pulseId);

    /*
     * \brief Start to read the pulses
     * \return Return false in case of problem
     */
    bool initPulseInfo();

    /*
     * \brief Allow to know the size of the data in the file
     * \return Return the size of one data.
     */
    int32_t getDataSize();

    /*
     * \brief Allow to know the amount of data for an event
     * \return Return the size the event.
     */
    int64_t getPulseSize();

    /*
     * \brief Get the details of the actual header
     * \return Return the details of the header
     */
    const Header &getHeader() const;

    /*
     * \brief Set the details of the header
     * \param header: new header used to replace the old one
     */
    void setHeader(Header &header);

protected:
    /*
     * \brief Swap to big endian to little endian and vise versa
     * \param x data to convert
     * \return Return the converted data.
     */
    uint32_t swap(uint32_t x);

    /*
     * \brief Allow to load all the data in the vector
     */
    void readData();

    /*
     * \brief Allow to load all the data in the vector for revision number 0
     */
    void readData8();

    /*
     * \brief Allow to load all the data in the vector for revision number 1
     */
    void readData16();

    /*
     * \brief Check if the file has been collapsed or not
     * \return Return true if the data need to be corrected
     */
    bool zeroSupressionCheck();

    /*
     * \brief Algorithm to fixed the data collapsed
     */
    void correctZeroSuppressionData();

    NtofLib &ntoflib_; //!< Allow to access to the handler of the reading file
    int64_t startPosition_;  //!< Position of the header in the file
    int32_t revisionNumber_; //!< Allow to save the value to not have to read in
                             //!< the file each time
    int32_t ACQCLength_; //!< Allow to save the value to not have to read in the
                         //!< file each time
    int32_t pulseId_;    //!< The id of the pulse in the ACQC
    std::map<int32_t, PulseInfo> pulses_; //!< Container of the pulses details
    int64_t pulsePosition_; //!< Allow to save the value to not have to read in
                            //!< the file each time
    int64_t pulseLength_;   //!< Allow to save the value to not have to read in
                            //!< the file each time
    int16_t data_; //!< Allow to save the value to not have to read in the file
                   //!< each time
    std::vector<int16_t> pulse16_; //!< Save the data of the pulse in memory
                                   //!< (data on 16 bits)
    std::vector<int8_t> pulse8_; //!< Save the data of the pulse in memory (data
                                 //!< on 8 bits)
    std::vector<PULSE_DATA_t> correctPulseCollection_; //!< Save the data of the
                                                       //!< ACQC if they are
                                                       //!< collapsed
    bool isZeroSuppressionCorrect_; //!< Allow to know if data are collapsed
    bool isZeroSuppressionInit_;    //!< Allow to know if all the data have been
                                    //!< recovered from the file
    Header header_;                 //!< Details of the header being read
};

} /* namespace lib */
} /* namespace ntof */

#endif /* READERSTRUCTACQC_H_ */
