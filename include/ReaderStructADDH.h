/*
 * ReaderStructACQC.h
 *
 *  Created on: Jan 12, 2015
 *      Author: agiraud
 */

#ifndef READERSTRUCTADDH_H_
#define READERSTRUCTADDH_H_

#include <string>
#include <type_traits>
#include <vector>

#include <stdint.h>

#include "HeaderManager.h"

const int64_t SIZE_OF_TITLE = (sizeof(char) * 64); //!< Size of the header
const int64_t SIZE_OF_TYPE = sizeof(int32_t);      //!< Size of the data type
const int64_t SIZE_OF_REPEAT = sizeof(
    int32_t); //!< Size of the repetition factor

namespace ntof {
namespace lib {
class NtofLib;

enum ADDHDATATYPE
{
    TypeString = 0, //!< Type string
    TypeInt,        //!< Type int32_t
    TypeLong,       //!< Type int64_t
    TypeFloat,      //!< Type float
    TypeDouble      //!< Type double
};
std::string typeToStr(ADDHDATATYPE type);

class ReaderStructADDH
{
public:
    /*
     * \brief Constructor
     * \param header Details of the header
     * \param ntoflib give access to the reader
     */
    ReaderStructADDH(const Header &header, NtofLib &ntoflib);

    /*
     * \brief Destructor
     */
    virtual ~ReaderStructADDH();

    /*
     * \brief Get the title
     * \return Return the title.
     */
    std::string getTitle();

    /*
     * \brief Get the revision number
     * \return Return the revision number.
     */
    int32_t getRevisionNumber();

    /*
     * \brief Get the reserved
     * \return Return the reserved.
     */
    uint32_t getReserved();

    /*
     * \brief Get the length
     * \return Return the length
     */
    uint32_t getLength();

    /*
     * \brief index all the ADDH fields
     */
    void initADDH();

    /*
     * \brief Jump to the next ADDH field
     * \return Return true if everything is ok and false if an error occurred
     */
    bool getNextField();

    /*
     * \brief Jump to the previous ADDH field
     * \return Return true if everything is ok and false if an error occurred
     */
    bool getPreviousField();

    /*
     * \brief Jump to the ADDH field with the given name
     * \param name: The name of the ADDH field to find
     * \return Return true if everything is ok and false if an error occurred
     */
    bool getField(const std::string &name);

    /*
     * \brief Get the name of the ADDH field
     * \return The name of the ADDH field
     */
    std::string getName();

    /*
     * \brief Get the type of the ADDH field
     * \return Return the type of the ADDH field as an enum
     */
    ADDHDATATYPE getType();

    /*
     * \brief Get the length of the array
     * \return Return the value of the repetition factor
     */
    uint32_t getRepetitionFactor();

    /*
     * \brief Get the value of the field as a string
     * \return Return the value of a string
     * \deprecated use getData<std::string>() instead
     */
    std::string getDataAsString();

    /*
     * \brief Get the value of the field as an int32_t
     * \return Return the value of an int32_t
     * \deprecated use getData<int32_t>() instead
     */
    int32_t getDataAsInt32();

    /*
     * \brief Get the value of the field as an int64_t
     * \return Return the value of an int64_t
     * \deprecated use getData<int64_t>() instead
     */
    int64_t getDataAsInt64();

    /*
     * \brief Get the value of the field as a float
     * \return Return the value of a float
     * \deprecated use getData<float>() instead
     */
    float getDataAsFloat();

    /*
     * \brief Get the value of the field as a double
     * \return Return the value of a double
     * \deprecated use getData<double>() instead
     */
    double getDataAsDouble();

    /**
     * @brief retrieve the value of current field
     * @return the value of current field
     */
    template<typename T>
    T getData();

    /**
     * @brief retrieve an array of data
     * @param[out] it output iterator
     * @return the output iterator
     */
    template<typename insert_iterator>
    insert_iterator getDataIterator(insert_iterator it);

    /**
     * @brief retrieve an array of data in a container
     * @param[out] out output container
     */
    template<typename container>
    void getDataArray(container &out);

    template<typename value>
    void getDataArray(std::vector<value> &out);

    /*
     * \brief Get the details of the actual header
     * \return Return the details of the header
     */
    const Header &getHeader() const;

    /*
     * \brief Set the details of the header
     * \param header: new header used to replace the old one
     */
    void setHeader(Header &header);

    /**
     * @brief internal struct to statically convert C++ types to ADDHDATATYPE
     */
    template<typename t>
    struct dataType :
        std::integral_constant<ADDHDATATYPE, static_cast<ADDHDATATYPE>(-1)>
    {};

protected:
    /*
     * \brief index all the ADDH fields
     * \param type: type of the data
     * \return Return the size of the data type given
     */
    int32_t getSizeOfData(int32_t type);

    NtofLib &ntoflib_; //!< Allow to access to the handler of the reading file
    int64_t startPosition_; //!< Position of the header in the file
    int32_t ADDHLength_; //!< Allow to save the value to not have to read in the
                         //!< file each time
    int32_t revisionNumber_; //!< Allow to save the value to not have to read in
                             //!< the file each time
    Header header_;          //!< Details of the header being read
    std::vector<int64_t> addhFieldPositions_; //!< Index of all the ADDH fields
    int32_t actualFieldId_; //!< The ID of the field being read
};

} /* namespace lib */
} /* namespace ntof */

#include "ReaderStructADDH.hxx"

#endif /* READERSTRUCTADDH_H_ */
