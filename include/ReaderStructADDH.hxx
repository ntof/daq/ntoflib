/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-09-15T09:23:40+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef READERSTRUCTADDH_HXX__
#define READERSTRUCTADDH_HXX__

#include <iterator>
#include <type_traits>

#include "HeaderManager.h"
#include "NtofLib.h"
#include "NtofLibException.h"
#include "Reader.h"
#include "ReaderStructADDH.h"

namespace ntof {
namespace lib {

template<>
struct ReaderStructADDH::dataType<std::string> :
    std::integral_constant<ADDHDATATYPE, TypeString>
{};
template<>
struct ReaderStructADDH::dataType<int32_t> :
    std::integral_constant<ADDHDATATYPE, TypeInt>
{};
template<>
struct ReaderStructADDH::dataType<std::int64_t> :
    std::integral_constant<ADDHDATATYPE, TypeLong>
{};
template<>
struct ReaderStructADDH::dataType<float> :
    std::integral_constant<ADDHDATATYPE, TypeFloat>
{};
template<>
struct ReaderStructADDH::dataType<double> :
    std::integral_constant<ADDHDATATYPE, TypeDouble>
{};

template<typename container>
void ReaderStructADDH::getDataArray(container &out)
{
    getDataIterator(std::back_inserter(out));
}

template<typename value>
void ReaderStructADDH::getDataArray(std::vector<value> &out)
{
    if (actualFieldId_ == -1)
    {
        throw NtofLibException("The id of the field is undefined", __FILE__,
                               __LINE__);
    }

    if (getType() != dataType<value>::value)
    {
        throw NtofLibException(
            "The data type is incompatible " +
                typeToStr(dataType<value>::value) +
                " != " + typeToStr(getType()),
            __FILE__, __LINE__);
    }

    {
        const int64_t position = addhFieldPositions_[actualFieldId_] +
            SIZE_OF_TITLE + SIZE_OF_TYPE + SIZE_OF_REPEAT;
        const uint32_t repeat = getRepetitionFactor();
        out.resize(repeat);
        ntoflib_.getReader(header_.isOnCastor)
            ->readArray(position, repeat, out.data(), header_.file);
    }
}

template<typename iterator>
iterator ReaderStructADDH::getDataIterator(iterator it)
{
    if (actualFieldId_ == -1)
    {
        throw NtofLibException("The id of the field is undefined", __FILE__,
                               __LINE__);
    }

    if (getType() !=
        dataType<typename iterator::container_type::value_type>::value)
    {
        throw NtofLibException(
            "The data type is incompatible " +
                typeToStr(
                    dataType<typename iterator::container_type::value_type>::value) +
                " != " + typeToStr(getType()),
            __FILE__, __LINE__);
    }

    {
        const int64_t position = addhFieldPositions_[actualFieldId_] +
            SIZE_OF_TITLE + SIZE_OF_TYPE + SIZE_OF_REPEAT;
        const uint32_t repeat = getRepetitionFactor();
        std::vector<typename iterator::container_type::value_type> ret;
        ret.reserve(repeat);
        ntoflib_.getReader(header_.isOnCastor)
            ->readArray(position, repeat, ret.data(), header_.file);
        std::copy(ret.data(), ret.data() + repeat, it);
    }
    return it;
}

template<>
std::string ReaderStructADDH::getData<std::string>();

template<typename t>
t ReaderStructADDH::getData()
{
    if (actualFieldId_ == -1)
    {
        throw NtofLibException("The id of the field is undefined", __FILE__,
                               __LINE__);
    }

    if (getType() != dataType<t>::value)
    {
        throw NtofLibException("The data type is incompatible " +
                                   typeToStr(dataType<t>::value) +
                                   " != " + typeToStr(getType()),
                               __FILE__, __LINE__);
    }

    const int64_t position = addhFieldPositions_[actualFieldId_] +
        SIZE_OF_TITLE + SIZE_OF_TYPE + SIZE_OF_REPEAT;
    const uint32_t repeat = getRepetitionFactor();
    return ntoflib_.getReader(header_.isOnCastor)
        ->read<t>(position, header_.file);
}

} // namespace lib
} // namespace ntof

#endif
