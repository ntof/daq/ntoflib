/*
 * ReaderStructEVEH.h
 *
 *  Created on: Jan 12, 2015
 *      Author: agiraud
 */

#ifndef READERSTRUCTEVEH_H_
#define READERSTRUCTEVEH_H_

#include <stdint.h>

#include "HeaderManager.h"

namespace ntof {
namespace lib {
class NtofLib;

class ReaderStructEVEH
{
public:
    /*
     * \brief Constructor
     * \param header Details of the header
     * \param ntoflib give access to the reader
     */
    ReaderStructEVEH(const Header &header, NtofLib &ntoflib);

    /*
     * \brief Destructor
     */
    virtual ~ReaderStructEVEH();

    /*
     * \brief Get the title
     * \return Return the title.
     */
    std::string getTitle();

    /*
     * \brief Get the revision number
     * \return Return the revision number.
     */
    uint32_t getRevisionNumber();

    /*
     * \brief Get the reserved
     * \return Return the reserved.
     */
    uint32_t getReserved();

    /*
     * \brief Get the length
     * \return Return the length.
     */
    uint32_t getLength();

    /*
     * \brief Get the size of the event
     * \return Return the size of the event.
     */
    uint32_t getSizeOfEvent();

    /*
     * \brief Get the event number
     * \return Return the event number.
     */
    uint32_t getEventNumber();

    /*
     * \brief Get the run number
     * \return Return the run number.
     */
    uint32_t getRunNumber();

    /*
     * \brief Get the time of the event
     * \return Return the time of the event.
     */
    uint32_t getTime();

    /*
     * \brief Get the date of the event
     * \return Return the date of the event.
     */
    uint32_t getDate();

    /*
     * \brief Get the hardware timestamp
     * \return Return the timestamp
     */
    double getComputerTimestamp();

    /*
     * \brief Get the BCT timestamp
     * \return Return the timestamp
     */
    double getBCTTimestamp();

    /*
     * \brief Get the beam intensity
     * \return Return the beam intensity
     */
    float getBeamIntensity();

    /*
     * \brief Get the beam type
     * \return Return the type of the beam as an enum.
     */
    uint32_t getBeamType();

    /*
     * \brief Get the beam type
     * \return Return the beam type as a string
     */
    std::string getBeamTypeName();

    /*
     * \brief Convert the type of beam from int to string
     * \return Return the type of the beam as an enum.
     */
    int32_t typeToInt(std::string type);

    /*
     * \brief Get the position of the event in the file
     * \return Return the position of the event in the file
     */
    uint64_t getCurrentSeek();

    /*
     * \brief Get the details of the actual header
     * \return Return the details of the header
     */
    const Header &getHeader() const;

    /*
     * \brief Set the details of the header
     * \param header: new header used to replace the old one
     */
    void setHeader(Header &header);

protected:
    NtofLib &ntoflib_; //!< Allow to access to the handle of the reading file
    int64_t startPosition_;  //!< Position of the header in the file
    int32_t revisionNumber_; //!< Allow to save the value to not have to read in
                             //!< the file each time
    uint32_t beamType_; //!< Allow to save the value to not have to read in the
                        //!< file each time
    Header header_;     //!< Details of the header being read
};

} /* namespace lib */
} /* namespace ntof */

#endif /* READERSTRUCTEVEH_H_ */
