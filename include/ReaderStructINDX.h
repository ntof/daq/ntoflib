/*
 * ReaderStructINDX.h
 *
 *  Created on: Jan 12, 2015
 *      Author: agiraud
 */

#ifndef READERSTRUCTINDX_H_
#define READERSTRUCTINDX_H_

#include <stdint.h>

#include "HeaderManager.h"

namespace ntof {
namespace lib {
class NtofLib;

/* TYPEDEF */
typedef struct
{
    uint32_t streamNumber;
    uint32_t segmentNumber;
    uint32_t validatedNumber;
    uint32_t triggerNumber;
    uint64_t offset;
} INDEX_FIELD;

class ReaderStructINDX
{
public:
    /*
     * \brief Constructor
     * \param header Details of the header
     * \param ntoflib give access to the reader
     */
    ReaderStructINDX(const Header &header, NtofLib &ntoflib);

    /*
     * \brief Destructor
     */
    virtual ~ReaderStructINDX();

    /*
     * \brief Get the title
     * \return Return the title.
     */
    std::string getTitle();

    /*
     * \brief Get the revision number
     * \return Return the revision number.
     */
    uint32_t getRevisionNumber();

    /*
     * \brief Get the reserved
     * \return Return the reserved.
     */
    uint32_t getReserved();

    /*
     * \brief Get the length
     * \return Return the length.
     */
    uint32_t getLength();

    /*
     * \brief Get the list of the position of the ACQC in the raw file
     * \return Return the list of the position of the ACQC in the raw file.
     */
    std::list<int32_t> getIndexList();

    /*
     * \brief Get the details of the actual header
     * \return Return the details of the header
     */
    const Header &getHeader() const;

    /*
     * \brief Set the details of the header
     * \param header: new header used to replace the old one
     */
    void setHeader(Header &header);

    /*
     * \brief Get the list of the positions of the EVEH in the raw files
     * \return Return the list of the positions of the EVEH in the raw files.
     */
    std::vector<INDEX_FIELD> getIndexFields();

protected:
    /*
     * \brief Initialize the list of the positions of the EVEH in the raw files
     */
    void setIndexFields();

    NtofLib &ntoflib_; //!< Allow to access to the handle of the reading file
    int64_t startPosition_;  //!< Position of the header in the file
    int32_t revisionNumber_; //!< Allow to save the value to not have to read in
                             //!< the file each time
    Header header_;          //!< Details of the header being read
    std::vector<INDEX_FIELD> index_; //!< All the fields of the index
};

} /* namespace lib */
} /* namespace ntof */

#endif /* READERSTRUCTINDX_H_ */
