/*
 * ReaderStructINFO.h
 *
 *  Created on: Jan 12, 2015
 *      Author: agiraud
 */

#ifndef READERSTRUCTINFO_H_
#define READERSTRUCTINFO_H_

#include <stdint.h>

#include "HeaderManager.h"

namespace ntof {
namespace lib {
class NtofLib;

class ReaderStructINFO
{
public:
    /*
     * \brief Constructor
     * \param header Details of the header
     * \param ntoflib give access to the reader
     */
    ReaderStructINFO(const Header &header, NtofLib &ntoflib);

    /*
     * \brief Destructor
     */
    virtual ~ReaderStructINFO();

    /*
     * \brief Get the title
     * \return Return the title.
     */
    std::string getTitle();

    /*
     * \brief Get the revision number
     * \return Return the revision number.
     */
    uint32_t getRevisionNumber();

    /*
     * \brief Get the reserved
     * \return Return the reserved.
     */
    uint32_t getReserved();

    /*
     * \brief Get the length
     * \return Return the length.
     */
    uint32_t getLength();

    /*
     * \brief Get the description
     * \return Return the description.
     */
    std::string getData();

    /*
     * \brief Get the details of the actual header
     * \return Return the details of the header
     */
    const Header &getHeader() const;

    /*
     * \brief Set the details of the header
     * \param header: new header used to replace the old one
     */
    void setHeader(Header &header);

protected:
    NtofLib &ntoflib_; //!< Allow to access to the handle of the reading file
    int64_t startPosition_;  //!< Position of the header in the file
    int32_t revisionNumber_; //!< Allow to save the value to not have to read in
                             //!< the file each time
    Header header_;          //!< Details of the header being read
};

} /* namespace lib */
} /* namespace ntof */

#endif /* READERSTRUCTINFO_H_ */
