/*
 * ReaderStructMODH.h
 *
 *  Created on: Jan 12, 2015
 *      Author: agiraud
 */

#ifndef READERSTRUCTMODH_H_
#define READERSTRUCTMODH_H_

#include <list>
#include <string>

#include <stdint.h>

#include "HeaderManager.h"

namespace ntof {
namespace lib {

typedef struct
{
    std::string detectorType; //!< Detector type (4*char, for example 'SILI' or
                              //!< 'C6D6')
    uint32_t detectorId;      //!< Physical detector channel
    std::string moduleType;
    uint32_t channel;
    uint32_t module;
    uint32_t chassis;
    uint32_t stream;
    float sampleRate; //!< Setting for the digitizer. The sample rate is the
                      //!< sampling frequency (in MHz) of the digitizer and can
                      //!< be 250, 400, 500, 1000 for Acqiris modules. MS/s
    float sampleSize; //!< Setting for the digitizer. The sample size is the
                      //!< amount of data (in kB) acquired before zero
                      //!< suppression. MB
    float fullScale;  //!< Setting for the digitizer. 50mV, 100mV, 200mV, 500mV,
                      //!< 1V, 2V and 5V
    int32_t delayTime; //!< Setting for the digitizer. Delay time is the time
                       //!< shift between trigger and start of the acquisition:
                       //!< The acquiris digitizer are able to start data taking
                       //!< at a given time after or before (!) the trigger. The
                       //!< number is given in samples.
    int32_t thresholdADC; //!< Setting for the digitizer. Threshold is used for
                          //!< the zero suppression and is given in ADC
    float thresholdMV; //!< Setting for the digitizer. Threshold is used for the
                       //!< zero suppression and is given in mV
    int32_t thresholdSign; //!< Setting for the digitizer. Threshold Sign
                           //!< specifies whether the part of the pulse below or
                           //!< above the threshold is wanted. If set to 0, the
                           //!< part below is wanted, as needed for negative
                           //!< pulses.
    float offset;          //!< Setting for the digitizer. Offset voltage in mV.
    uint32_t preSample;    //!< Setting for the digitizer. It is the number of
                        //!< samples before a threshold crossing which are saved
                        //!< to disk. Allowed values are 0,16,32,48, ... 2032.
    uint32_t postSample; //!< Setting for the digitizer. is the number of
                         //!< samples which are saved to disk after the pulse is
                         //!< below the threshold again. Allowed values are
                         //!< 0,16,32,48, ... 4080.
    std::string clockState; //!< INTC/EXTC internal/external clock (Acqiris only)
    uint32_t zeroSuppressionStart;  //!< Used to delay the zeroSuppression
    std::string masterDetectorType; //!< Detector name of the master in the
                                    //!< second level zero suppression algorithm
    uint32_t masterDetectorId;      //!< Detector ID of the master in the second
                                    //!< level zero suppression algorithm
} CHAN_CONFIG;

class NtofLib;

class ReaderStructMODH
{
public:
    /*
     * \brief Constructor
     * \param header Details of the header
     * \param ntoflib give access to the reader
     */
    ReaderStructMODH(const Header &header, NtofLib &ntoflib);

    /*
     * \brief Destructor
     */
    virtual ~ReaderStructMODH();

    /*
     * \brief Get the title
     * \return Return the title.
     */
    std::string getTitle();

    /*
     * \brief Get the revision number
     * \return Return the revision number.
     */
    int32_t getRevisionNumber();

    /*
     * \brief Get the reserved
     * \return Return the reserved.
     */
    uint32_t getReserved();

    /*
     * \brief Get the length
     * \return Return the length.
     */
    uint32_t getLength();

    /*
     * \brief Get the number of channels in the run
     * \return Return the number of channels in the run.
     */
    uint32_t getNbChannels();

    /*
     * \brief Initialize the next channel configuration
     * \return Return true if the next channel is ready to be read or false if
     * there is no more channel configuration.
     */
    bool getNextChannel();

    /*
     * \brief Get the detector type
     * \return Return the value of the detector type
     */
    std::string getDetectorType();

    /*
     * \brief Get the detector id
     * \return Return the value of the detector id
     */
    uint32_t getDetectorId();

    /*
     * \brief Get the module type
     * \return Return the value of the module type
     */
    std::string getModuleType();

    /*
     * \brief Get the channel id
     * \return Return the value of the channel id
     */
    uint32_t getChannel();

    /*
     * \brief Get the module id
     * \return Return the value of the module id
     */
    uint32_t getModule();

    /*
     * \brief Get the chassis id
     * \return Return the value of the chassis id
     */
    uint32_t getChassis();

    /*
     * \brief Get the stream id
     * \return Return the value of the stream id
     */
    uint32_t getStream();

    /*
     * \brief Get the sample rate
     * \return Return the value of the sample rate
     */
    float getSampleRate();

    /*
     * \brief Get the sample size
     * \return Return the value of the sample size
     */
    float getSampleSize();

    /*
     * \brief Get the full scale
     * \return Return the value of the full scale
     */
    float getFullScale();

    /*
     * \brief Get the full scale in millivolts
     * \param FullScale to convert
     * \return Return the value of the full scale in millivolts
     */
    float getFullScaleInMilliVolts(float fullScale = -1);

    /*
     * \brief Get the delay time
     * \return Return the value of the delay time
     */
    int32_t getDelayTime();

    /*
     * \brief Get the threshold in ADC
     * \return Return the value of the threshold in ADC
     */
    int32_t getThresholdADC();

    /*
     * \brief Get the threshold in millivolts
     * \return Return the value of the threshold in millivolts
     */
    float getThresholdMilliVolts();

    /*
     * \brief Get the threshold sign
     * \return Return the value of the threshold sign
     */
    int32_t getThresholdSign();

    /*
     * \brief Get the offset
     * \return Return the value of the offset
     */
    float getOffset();

    /*
     * \brief Get the presample
     * \return Return the value of the presample
     */
    uint32_t getPreSample();

    /*
     * \brief Get the postsample
     * \return Return the value of the postsample
     */
    uint32_t getPostSample();

    /*
     * \brief Get the clock state
     * \return Return the value of the clock state
     */
    std::string getClockState();

    /*
     * \brief Get the zero suppression start
     * \return Return the value of the zero suppression start
     */
    uint32_t getZeroSuppressionStart();

    /*
     * \brief Get the master detector type
     * \return Return the master detector type
     */
    std::string getMasterDetectorType();

    /*
     * \brief Get the master detector ID
     * \return Return the master detector ID
     */
    uint32_t getMasterDetectorId();

    /*
     * \brief Get all the channel configuration as a list
     * \return Return the value of the get the channel configuration as a list
     */
    std::list<CHAN_CONFIG> getChannelsConfig();

    /*
     * \brief Initialize the actual channel according the id of the DAQ elements
     * \param channelId The id of the channel
     * \param moduleId The id of the card
     * \param streamId The id of the stream
     * \param chassisId The id of the chassis
     * \return Return true if the channel configuration has been found or false
     * in the other case.
     */
    bool setIndex(uint32_t channelId,
                  uint32_t moduleId,
                  uint32_t streamId,
                  uint32_t chassisId);

    /*
     * \brief Get the maximal value that the data can have
     * \param moduleType the type of card
     * \return Return the maximal value for a card type
     */
    int32_t getMaxDataValue(std::string moduleType = "");

    /*
     * \brief Get the minimal value that the data can have
     * \param moduleType the type of card
     * \return Return the minimal value for a card type
     */
    int32_t getMinDataValue(std::string moduleType = "");

    /*
     * \brief Get number of bits for a card type
     * \param moduleType the type of card
     * \return Return the number of bits for a card type
     */
    int32_t getNbBits(std::string moduleType = "");

    /*
     * \brief Get number of bits for a card type
     * \param moduleType the type of card
     * \return Return the ADC range for a card type
     */
    int32_t getADCRange(std::string moduleType = "");

    /*
     * \brief Allow to know the number of ACQC by EVEH expected
     * \return Return the number of ACQC by EVEH
     */
    int32_t getNumberOfAcqcByEvent();

    /*
     * \brief Get the index of the channel configuration
     * \return Return the index value
     */
    uint32_t getIndex() const;

    /*
     * \brief Get the index of the channel configuration
     * \param detectorType The type of detector on 4 digits
     * \param detectorId The id of detector
     * \return Return the index value
     */
    uint32_t getIndex(std::string detectorType, uint32_t detectorId);

    /*
     * \brief Set the index of the channel configuration
     * \param detectorType The type of detector on 4 digits
     * \param detectorId The id of detector
     * \return Return true if the channel has been found or false if it's not.
     */
    bool setIndex(std::string detectorType, uint32_t detectorId);

    /*
     * \brief Set the id of the channel to access to the configuration data
     * \param id channel's index
     */
    void setIndex(uint32_t id);

    /*
     * \brief Get the details of the actual header
     * \return Return the details of the header
     */
    const Header &getHeader() const;

    /*
     * \brief Set the details of the header
     * \param header: new header used to replace the old one
     */
    void setHeader(Header &header);

protected:
    NtofLib &ntoflib_; //!< Allow to access to the handle of the reading file
    int64_t startPosition_;  //!< Position of the header in the file
    int32_t revisionNumber_; //!< Allow to save the value to not have to read in
                             //!< the file each time
    int32_t acqcExpected_;   //!< Allow to save the value to not have to
                             //!< recalculate each time
    uint32_t numberOfChannel_; //!< Allow to save the value to not have to read
                               //!< in the file each time
    int32_t channelId_; //!< if equals to -1 no channel load, else the index of
                        //!< the channel in the list "listConfig_"
    std::list<CHAN_CONFIG> listConfig_; //!< Allow to save the configuration to
                                        //!< not have to read in the file each
                                        //!< time
    Header header_;                     //!< Details of the header being read
};

} /* namespace lib */
} /* namespace ntof */

#endif /* READERSTRUCTMODH_H_ */
