/*
 * ReaderStructRCTR.h
 *
 *  Created on: Jan 12, 2015
 *      Author: agiraud
 */

#ifndef READERSTRUCTRCTR_H_
#define READERSTRUCTRCTR_H_

#include <list>

#include <stdint.h>

#include "HeaderManager.h"

namespace ntof {
namespace lib {
class NtofLib;

class ReaderStructRCTR
{
public:
    /*
     * \brief Constructor
     * \param header Details of the header
     * \param ntoflib give access to the reader
     */
    ReaderStructRCTR(const Header &header, NtofLib &ntoflib);

    /*
     * \brief Destructor
     */
    virtual ~ReaderStructRCTR();

    /*
     * \brief Get the title
     * \return Return the title.
     */
    std::string getTitle();

    /*
     * \brief Get the revision number
     * \return Return the revision number.
     */
    int32_t getRevisionNumber();

    /*
     * \brief Get the reserved
     * \return Return the reserved.
     */
    uint32_t getReserved();

    /*
     * \brief Get the length
     * \return Return the length.
     */
    uint32_t getLength();

    /*
     * \brief Get the run number
     * \return Return the run number.
     */
    int32_t getRunNumber();

    /*
     * \brief Get the event number
     * \return Return the event number.
     */
    int32_t getExtensionNumber();

    /*
     * \brief Get the stream number
     * \return Return the stream number.
     */
    uint32_t getStreamNumber();

    /*
     * \brief Get the experiment
     * \return Return the experiment.
     */
    std::string getExperiment();

    /*
     * \brief Get the date of start
     * \return Return the date of start.
     */
    uint32_t getDate();

    /*
     * \brief Get the time of start
     * \return Return the time of start.
     */
    uint32_t getTime();

    /*
     * \brief Get the number of modules
     * \return Return the number of modules.
     */
    uint32_t getNumberOfModules();

    /*
     * \brief Get the number of channels
     * \return Return the number of channels.
     */
    uint32_t getNumberOfChannels();

    /*
     * \brief Get the numbers of streams
     * \return Return the numbers of streams.
     */
    uint32_t getTotalNumberOfStreams();

    /*
     * \brief Get the number of chassis
     * \return Return the number of chassis.
     */
    uint32_t getTotalNumberOfChassis();

    /*
     * \brief Get the number of modules
     * \return Return the number of modules.
     */
    uint32_t getTotalNumberOfModules();

    /*
     * \brief Get the list of the channel used by module
     * \return Return the list of the channel used by module.
     */
    std::list<uint32_t> getNumberOfUsedChannelByModule();

    /*
     * \brief Get the details of the actual header
     * \return Return the details of the header
     */
    const Header &getHeader() const;

    /*
     * \brief Set the details of the header
     * \param header: new header used to replace the old one
     */
    void setHeader(Header &header);

protected:
    NtofLib &ntoflib_; //!< Allow to access to the handle of the reading file
    int64_t startPosition_;  //!< Position of the header in the file
    int32_t revisionNumber_; //!< Allow to save the value to not have to read in
                             //!< the file each time
    int32_t runNumber_; //!< Allow to save the value to not have to read in the
                        //!< file each time
    int32_t extensionNumber_; //!< Allow to save the value to not have to read
                              //!< in the file each time
    Header header_;           //!< Details of the header being read
};

} /* namespace lib */
} /* namespace ntof */

#endif /* READERSTRUCTRCTR_H_ */
