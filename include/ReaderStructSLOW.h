/*
 * ReaderStructSLOW.h
 *
 *  Created on: Jan 12, 2015
 *      Author: agiraud
 */

#ifndef READERSTRUCTSLOW_H_
#define READERSTRUCTSLOW_H_

#include <stdint.h>

#include "HeaderManager.h"

namespace ntof {
namespace lib {
class NtofLib;

class ReaderStructSLOW
{
public:
    /*
     * \brief Constructor
     * \param header Details of the header
     * \param ntoflib give access to the reader
     */
    ReaderStructSLOW(const Header &header, NtofLib &ntoflib);

    /*
     * \brief Destructor
     */
    virtual ~ReaderStructSLOW();

    /*
     * \brief Get the title
     * \return Return the title.
     */
    std::string getTitle();

    /*
     * \brief Get the revision number
     * \return Return the revision number.
     */
    uint32_t getRevisionNumber();

    /*
     * \brief Get the reserved
     * \return Return the reserved.
     */
    uint32_t getReserved();

    /*
     * \brief Get the length
     * \return Return the length.
     */
    uint32_t getLength();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getCT1();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getCT2();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getCT3();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getCT4();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getCT5();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getFT1();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getFT2();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getO2T1();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getO2T2();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPT1();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPT2();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPT3();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPT4();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPT5();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPT6();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPT7();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPH1();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPH2();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPH3();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPH4();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getTT2();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getTT3();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getTT4();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getTT5();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getTT6();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getTT7();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getTT8();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getTT9();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getTT10();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPAXTOF01();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPAXTOF02();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPAXTOF03();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPAXTOF04();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPAXTOF05();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPMIBL01();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPMIBL02();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPMITOF01();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPMITOF02();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPMITOF03();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPMITOF04();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getPMITOF05();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getMagnetIntensity();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    int32_t getMagnetStatus();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    // Vacuum definition for EAR1
    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF_VGR_1_PR();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF_VGR_1_Stat();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF_VGR_1A_PR();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF_VGR_1A_Stat();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF_VGR_2_PR();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF_VGR_2_Stat();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF_VGR_2A_PR();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF_VGR_2A_Stat();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF_VGR_3_PR();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF_VGR_3_Stat();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF_VGR_3A_PR();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF_VGR_3A_Stat();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF_VGR_4_PR();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF_VGR_4_Stat();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF_VGR_4A_PR();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF_VGR_4A_Stat();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF_EXP_PR();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF_EXP_Stat();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF_EXP_A_PR();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF_EXP_A_Stat();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF_EXP_B_PR();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF_EXP_B_Stat();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOFVVS01_Stat();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOFVVS02_Stat();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOFVVS01();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOFVVS02();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF2VVS01();
    // Vacuum definition for EAR2

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF2_VGR_1_PR();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF2_VGR_2_PR();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF2_VGR_3_PR();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getVAC_TOF2_VGR_4_PR();
    // Exp area temperature:

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getExpTemp_EAR1_IN();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getExpTemp_EAR1_OUT();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getExpTemp_EAR2_IN();

    /*
     * \brief Get the slow parameter
     * \return Return the slow parameter.
     */
    float getExpTemp_EAR2_OUT();

    /*
     * \brief Get the details of the actual header
     * \return Return the details of the header
     */
    const Header &getHeader() const;

    /*
     * \brief Set the details of the header
     * \param header: new header used to replace the old one
     */
    void setHeader(Header &header);

protected:
    NtofLib &ntoflib_; //!< Allow to access to the handle of the reading file
    int64_t startPosition_;  //!< Position of the header in the file
    int32_t revisionNumber_; //!< Allow to save the value to not have to read in
                             //!< the file each time
    Header header_;          //!< Details of the header being read
};

} /* namespace lib */
} /* namespace ntof */

#endif /* READERSTRUCTSLOW_H_ */
