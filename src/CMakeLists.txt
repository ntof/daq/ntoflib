
include_directories(
    ${CMAKE_SOURCE_DIR}/include
    ${CMAKE_BINARY_DIR}/include)
include_directories(SYSTEM
    ${XROOTD_INCLUDE_DIRS}
    ${Boost_INCLUDE_DIRS})

set(NTOFLIB_SRC
    HeaderManager.cpp
    NtofLib.cpp
    NtofLibException.cpp
    Reader.cpp
    ReaderLocal.cpp
    ReaderStructACQC.cpp
    ReaderStructADDH.cpp
    ReaderStructBEAM.cpp
    ReaderStructEVEH.cpp
    ReaderStructINDX.cpp
    ReaderStructINFO.cpp
    ReaderStructMODH.cpp
    ReaderStructRCTR.cpp
    ReaderStructSLOW.cpp
    FileCache.cpp
    )

if(CASTOR)
    list(APPEND NTOFLIB_SRC
        ReaderCastorCached.cpp
        ReaderCastor.cpp)
endif(CASTOR)

add_library(ntoflib ${NTOFLIB_SRC})
target_link_libraries(ntoflib ${XROOTD_LIBRARIES} ${Boost_LIBRARIES})

install(TARGETS ntoflib
    LIBRARY DESTINATION "${CMAKE_INSTALL_LIBDIR}" COMPONENT Runtime
    ARCHIVE DESTINATION "${CMAKE_INSTALL_LIBDIR}" COMPONENT Devel)
