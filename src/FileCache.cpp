/*
** Copyright (C) 2018 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-10-15T11:27:39+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "FileCache.h"

#include <iostream>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

#include <errno.h>
#include <sys/stat.h>

namespace bfs = boost::filesystem;

namespace ntof {
namespace lib {

FileCache::FileCache(size_t maxSize, const bfs::path &baseDir) :
    m_maxSize(maxSize), m_baseDir(baseDir)
{}

void FileCache::setBaseDir(const bfs::path &baseDir)
{
    m_baseDir = baseDir;
}

void FileCache::setMaxSize(size_t size)
{
    m_maxSize = size;
}

FileCache::SharedLock FileCache::readLock(const std::string &name, int &err)
{
    LockCtnr::const_iterator it = m_locks.find(name);
    SharedLock lock;
    err = 0;

    if (it != m_locks.end())
    {
        lock = it->second.lock();
        if (lock->state() == Lock::SHARED)
            return lock;
        else
        {
            err = -EAGAIN;
            return SharedLock();
        }
    }

    lock.reset(new Lock(*this));
    int ret = lock->init(name, false);
    if (ret < 0)
    {
        err = ret;
        return SharedLock();
    }
    m_locks[name] = lock;
    return lock;
}

FileCache::SharedLock FileCache::create(const std::string &name,
                                        size_t size,
                                        int &err)
{
    boost::system::error_code ec;
    SharedLock lock;
    err = 0;

    if (m_baseDir.empty())
    {
        err = -ENOENT;
        return SharedLock();
    }

    boost::filesystem::create_directories(m_baseDir, ec);
    cleanup(size);

    if (m_locks.find(name) != m_locks.end())
    {
        err = -EAGAIN;
        return lock;
    }
    lock.reset(new Lock(*this));
    int ret = lock->init(name, true);
    if (ret < 0)
    {
        err = ret;
        return SharedLock();
    }
    bfs::resize_file(lock->fullpath(), size, ec);
    if (ec)
    {
        err = -EACCES;
        return SharedLock();
    }
    m_locks[name] = lock;
    return lock;
}

FileCache::Lock::Lock(FileCache &cache) : m_state(UNLOCKED), m_cache(cache) {}

FileCache::Lock::~Lock()
{
    if (m_state == EXCLUSIVE)
        m_lock.unlock();
    else if (m_state == SHARED)
        m_lock.unlock_sharable();
    m_cache.m_locks.erase(m_filename);
}

int FileCache::Lock::init(const std::string &filename, bool exclusive)
{
    m_fullpath = m_cache.baseDir();
    m_fullpath /= filename;
    m_filename = filename;

    if (exclusive && !bfs::exists(m_fullpath))
    {
        bfs::ofstream file(m_fullpath,
                           std::ios_base::out | std::ios_base::trunc);
        if (!file.is_open())
            return -EACCES;
    }

    try
    {
        ;
        m_lock = boost::interprocess::file_lock(m_fullpath.c_str());
    }
    catch (boost::interprocess::interprocess_exception &e)
    {
        if (exclusive) /* should not happen for exclusive locks */
            std::cerr << "Failed to create file_lock: " << e.what()
                      << std::endl;
        return -ENOENT;
    }
    if (exclusive)
    {
        if (!m_lock.try_lock())
            return -EAGAIN;
        m_state = EXCLUSIVE;
    }
    else
    {
        if (!m_lock.try_lock_sharable())
            return -EAGAIN;
        m_state = SHARED;
    }
    return 0;
}

struct StatCompare
{
    bool operator()(const struct stat &s1, const struct stat &s2) const
    {
        if (s1.st_atime != s2.st_atime)
            return s1.st_atime < s2.st_atime;
        else if (s1.st_mtime != s2.st_mtime)
            return s1.st_mtime < s2.st_mtime;
        else if (s1.st_ctime != s2.st_ctime)
            return s1.st_ctime < s2.st_ctime;
        else
            return s1.st_ino < s2.st_ino; /* required for strict ordering */
    }
};

void FileCache::cleanup(size_t requestedSize)
{
    if (!bfs::is_directory(m_baseDir))
    {
        std::cerr << "BaseDir is not a directory " << m_baseDir << std::endl;
        return;
    }

    size_t total = 0;
    boost::filesystem::directory_iterator end_iter;
    std::map<struct stat, bfs::path, StatCompare> files;
    for (bfs::directory_iterator dir(m_baseDir); dir != end_iter; ++dir)
    {
        if (!bfs::is_regular_file(dir->status()))
            continue;

        bfs::path path = dir->path();
        struct stat st;
        if (::stat(path.c_str(), &st) < 0)
        {
            std::cerr << "Failed to stat " << path << std::endl;
            continue;
        }
        total += st.st_size;
        files[st] = path;
    }

    for (std::map<struct stat, bfs::path, StatCompare>::iterator it =
             files.begin();
         (total + requestedSize > m_maxSize) && (it != files.end()); ++it)
    {
        std::string filename = it->second.filename().string();
        Lock lock(*this);
        if ((m_locks.find(filename) == m_locks.end()) &&
            (lock.init(filename, true) == 0))
        {
            std::cout << "Removing (old) file: " << filename << std::endl;
            ::unlink(it->second.c_str());
            total -= it->first.st_size;
        }
    }
}

} // namespace lib
} // namespace ntof
