/*
 * HeaderManager.cpp
 *
 *  Created on: Aug 11, 2015
 *      Author: agiraud
 */

#include "HeaderManager.h"

#include <iostream>
#include <sstream>

#include "NtofLibException.h"

namespace ntof {
namespace lib {

HeaderManager *HeaderManager::instance_ = NULL;

void FILE_INFO::setFlag(FILE_INFO::Flag flag, bool value)
{
    if (flag == ON_CASTOR)
        isOnCastor = value;
    else if (flag == IS_FINISHED)
        isFinished = value;

    if (value)
    {
        flags |= flag;
    }
    else
    {
        flags &= ~flag;
    }
}

/*!
 *  \brief Get the instance of the EACS object and initialize the instance if
 * the pointer is null \return A pointer on the EACS object
 */
HeaderManager *HeaderManager::getInstance()
{
    if (instance_ == NULL)
    {
        instance_ = new HeaderManager();
    }
    return instance_;
}

/*
 * \brief Constructor
 */
HeaderManager::HeaderManager() :
    nbEVEH_(0),
    nbACQC_(0),
    evehIterator_(eventsHeaders_.begin()),
    acqcIterator_(-1) /*,
     lastEventAdded_(-1)*/
{
    actualEVEH_.title = "";
    actualACQC_.title = "";
}

/*
 * \brief Destructor
 */
HeaderManager::~HeaderManager() {}

/* ********************************************** */
/* HEADERS LIST                                   */
/* ********************************************** */
/**
 * \brief Add a new header to the list of the headers.
 * \param header the header to add
 */
void HeaderManager::addHeader(Header header)
{
    //            std::cerr << "*** HEADER:" << std::endl;
    //            std::cerr << "   title: " << header.title << std::endl;
    //            std::cerr << "   detectorId: " << header.detectorId <<
    //            std::endl; std::cerr << "   detectorType: " <<
    //            header.detectorType << std::endl; std::cerr << " eventNumber:
    //            " << header.eventNumber << std::endl; std::cerr << "   id: "
    //            << header.id << std::endl; std::cerr << "   isOnCastor: " <<
    //            header.isOnCastor << std::endl; std::cerr << "   pos: " <<
    //            header.pos << std::endl; std::cerr << std::endl;

    // Add it to the right place
    if (header.title == "RCTR" || header.title == "MODH" ||
        (header.title == "INDX" && header.revisionNumber == 1))
    {
        singleHeaders_.push_back(header);
    }
    else
    {
        // If the key doesn't exist
        if (header.title == "EVEH")
        {
            // If the event doesn't exist in the map, it's added
            if (eventsHeaders_.find(header.eventNumber) == eventsHeaders_.end())
            {
                eventsHeaders_[header.eventNumber];
            }
            // lastEventAdded_ = header.eventNumber;
        }
        else if (header.title == "ACQC" &&
                 eventsHeaders_.find(header.eventNumber) == eventsHeaders_.end())
        {
            throw NtofLibException("An header can't be related to an event",
                                   __FILE__, __LINE__);
        }

        // Check if the header doesn't exist
        bool skip = false;
        for (std::vector<Header>::iterator headerIt =
                 eventsHeaders_[header.eventNumber].begin();
             headerIt != eventsHeaders_[header.eventNumber].end(); ++headerIt)
        {
            if ((*headerIt).title == header.title &&
                (*headerIt).eventNumber == header.eventNumber &&
                (*headerIt).detectorType == header.detectorType &&
                (*headerIt).detectorId == header.detectorId &&
                (*headerIt).revisionNumber == header.revisionNumber)
            {
                if (header.title == "ACQC" || header.title == "EVEH" ||
                    header.title == "ADDH" ||
                    (header.detectorType == "NONE" && header.detectorId == 1))
                {
                    // Skip
                    skip = true;
                    break;
                }
                else
                {
                    throw NtofLibException(
                        "Identical headers found [" + header.title + "]",
                        __FILE__, __LINE__);
                }
            }
        }

        if (!skip)
        {
            // Give an id to the Headers
            if (header.title == "EVEH")
            {
                header.id = nbEVEH_;
                ++nbEVEH_;
            }
            else if (header.title == "ACQC")
            {
                header.id = nbACQC_;
                ++nbACQC_;
            }

            // Error if no event related
            /*if (lastEventAdded_ == -1){
                throw NtofLibException("An header can't be related to an event",
            __FILE__, __LINE__);
            }*/

            // Add the header
            eventsHeaders_[header.eventNumber].push_back(header);
        }
    }
}

/**
 * \brief Clear the list of the headers.
 */
void HeaderManager::clear()
{
    for (std::map<int32_t, std::vector<Header>>::iterator event =
             eventsHeaders_.begin();
         event != eventsHeaders_.end(); ++event)
    {
        (*event).second.clear();
    }

    nbEVEH_ = 0;
    nbACQC_ = 0;
    acqcIterator_ = -1;
    //            lastEventAdded_=-1;
    actualEVEH_.title = "";
    actualACQC_.title = "";

    singleHeaders_.clear();
    eventsHeaders_.clear();
}

/* ********************************************** */
/* VALID HEADER TITLE                             */
/* ********************************************** */
/*
 * \brief Allow to know if the data we are reading is an Header title
 * \param memblock: string to test
 * \return Return true if it's a header title or false if it's not
 */
bool HeaderManager::isHeaderTitle(char *memblock)
{
    if (strcmp(memblock, "RCTR") == 0 || strcmp(memblock, "MODH") == 0 ||
        strcmp(memblock, "EVEH") == 0 || strcmp(memblock, "ACQC") == 0 ||
        strcmp(memblock, "INFO") == 0 || strcmp(memblock, "INDX") == 0 ||
        strcmp(memblock, "BEAM") == 0 || strcmp(memblock, "SLOW") == 0 ||
        strcmp(memblock, "HIVO") == 0 || strcmp(memblock, "ADDH") == 0 ||
        strcmp(memblock, "SKIP") == 0)
    {
        return true;
    }
    return false;
}

/* ********************************************** */
/* GETTERS                                        */
/* ********************************************** */
/*
 * \brief Get the number of event header found in the list
 * \return Return the number of event header found in the list
 */
int32_t HeaderManager::getNumberOfEvents()
{
    return eventsHeaders_.size();
}

/*
 * \brief Get the ID of the actual ACQC header being read
 * \return Return the ID of the actual ACQC header being read
 */
int32_t HeaderManager::getIdAcqc() const
{
    int32_t res = -1;
    if (actualACQC_.title != "")
    {
        res = actualACQC_.id;
    }
    return res;
}

/*
 * \brief Get the ID of the actual EVEH header being read
 * \return Return the ID of the actual EVEH header being read
 */
int32_t HeaderManager::getIdEveh() const
{
    int32_t res = -1;
    if (actualEVEH_.title != "")
    {
        res = actualEVEH_.id;
    }
    return res;
}

/* ********************************************** */
/* HELPERS                                        */
/* ********************************************** */
/*
 * \brief Get the details relative to the first occurrence of an header
 * \param title: Title of the header
 * \return Return the detail of the next header or an empty header if it can't
 * be found
 */
Header HeaderManager::getHeader(std::string title)
{
    //            std::cerr << "Number of single headers: " <<
    //            singleHeaders_.size() << std::endl; std::cerr << "Number of
    //            events headers: " << eventsHeaders_.size() << std::endl;

    //            if (title == "RCTR"){
    //                for (std::map<int32_t, std::vector<Header> >::iterator
    //                eventIt = eventsHeaders_.begin(); eventIt !=
    //                eventsHeaders_.end(); ++eventIt){
    //                    std::cerr << "Key " << (*eventIt).first << std::endl;
    //                    for (std::vector<Header>::iterator headerIt =
    //                    (*eventIt).second.begin(); headerIt !=
    //                    (*eventIt).second.end(); ++headerIt){
    //                        if ((*headerIt).title == "ACQC"){
    //                            std::cerr << "     - " << (*headerIt).title <<
    //                            " [" << (*headerIt).pos << "] : " <<
    //                            (*headerIt).detectorType << " " <<
    //                            (*headerIt).detectorId << std::endl;
    //                        } else{
    //                            std::cerr << "     - " << (*headerIt).title <<
    //                            " [" << (*headerIt).pos << "]" << std::endl;
    //                        }
    //                    }
    //                }
    //            }

    return getHeader(title, 0);
}

/*
 * \brief Search for the header in the single ones
 * \param title: name of the header
 * \param number: The header has to be located beyond this position
 * \return Return the header with an empty title if the good header has not been
 * found
 */
Header HeaderManager::getSingleHeader(const std::string &title, int32_t number)
{
    Header headerTmp;
    headerTmp.title = "";
    int32_t id = 0;
    for (std::list<Header>::iterator single = singleHeaders_.begin();
         single != singleHeaders_.end(); ++single)
    {
        if ((*single).title == title)
        {
            if (id == number)
            {
                headerTmp = (*single);
            }
            else
            {
                ++id;
            }
        }
    }
    return headerTmp;
}

/*
 * \brief Search for the header with the validated event number
 * \param validEvent: The header has to be located beyond this position
 * \return Return the header with an empty title if the good header has not been
 * found
 */
Header HeaderManager::getValidatedEventHeader(int32_t validEvent)
{
    Header headerTmp;
    headerTmp.title = "";

    std::map<int32_t, std::vector<Header>>::iterator eventIt =
        eventsHeaders_.find(validEvent);
    if (eventIt != eventsHeaders_.end())
    {
        evehIterator_ = eventIt;
        headerTmp = getEventHeader("EVEH", -1);
    }

    return headerTmp;
}

/*
 * \brief Search for the header in the single ones
 * \param title: name of the header
 * \param number: The header has to be located beyond this position
 * \return Return the header with an empty title if the good header has not been
 * found
 */
Header HeaderManager::getEventHeader(const std::string &title, int32_t number)
{
    Header headerTmp;
    headerTmp.title = "";

    if (number != -1)
    {
        evehIterator_ = eventsHeaders_.begin();

        int32_t id = 0;
        while (id != number && evehIterator_ != eventsHeaders_.end())
        {
            ++id;
            ++evehIterator_;
        }
    }

    if (evehIterator_ != eventsHeaders_.end())
    {
        for (std::vector<Header>::iterator header =
                 (*evehIterator_).second.begin();
             header != (*evehIterator_).second.end(); ++header)
        {
            if ((*header).title == title)
            {
                actualEVEH_ = (*header);
                acqcIterator_ = -1;
                headerTmp = (*header);
                break;
            }
        }
    }

    // If the header can't be found everything must be reset
    if (headerTmp.title == "")
    {
        actualEVEH_ = headerTmp;
        evehIterator_ = eventsHeaders_.end();
        actualACQC_ = headerTmp;
        acqcIterator_ = -1;
    }

    return headerTmp;
}

/*
 * \brief Search for the header in the single ones
 * \param title: name of the header
 * \param number: The header has to be located beyond this position
 * \return Return the header with an empty title if the good header has not been
 * found
 */
Header HeaderManager::getDataHeader(const std::string &title, int32_t number)
{
    Header headerTmp;
    headerTmp.title = "";

    if (actualEVEH_.title != "")
    {
        int32_t id = 0;
        // If the header is related to an event
        for (std::vector<Header>::iterator header =
                 eventsHeaders_[actualEVEH_.eventNumber].begin();
             header != eventsHeaders_[actualEVEH_.eventNumber].end(); ++header)
        {
            if ((*header).title == title)
            {
                if (id == number)
                {
                    if (title == "ACQC")
                    {
                        acqcIterator_ = id;
                        actualACQC_ = (*header);
                    }
                    headerTmp = (*header);
                    break;
                }
                else
                {
                    ++id;
                }
            }
        }
    }

    // If the header can't be found everything must be reset
    if (title == "ACQC" && headerTmp.title == "")
    {
        actualACQC_ = headerTmp;
        acqcIterator_ = -1;
    }

    return headerTmp;
}

/*
 * \brief Get the details relative to the next header beyond a define position
 * \param title: Title of the header
 * \param number: The header has to be located beyond this position
 * \return Return the detail of the next header or an empty header if it can't
 * be found
 */
Header HeaderManager::getHeader(std::string title, int32_t number)
{
    Header headerTmp;
    headerTmp.title = "";

    // If the header is a single one
    if (title == "RCTR" || title == "MODH" ||
        (title == "INDX" &&
         (actualEVEH_.title == "" || actualEVEH_.revisionNumber == 1)))
    {
        headerTmp = getSingleHeader(title, number);
    }
    else if (title == "EVEH")
    {
        evehIterator_ = eventsHeaders_.begin();
        headerTmp = getEventHeader(title, number);
    }
    else if (actualEVEH_.title != "" &&
             (title == "ACQC" || title == "BEAM" || title == "INDX" ||
              title == "INFO" || title == "SLOW" || title == "ADDH"))
    {
        headerTmp = getDataHeader(title, number);
    }

    return headerTmp;
}

/*
 * \brief Get the details relative to the next header in the list
 * \param title: Title of the header
 * \return Return the detail of the next header or an empty header if it can't
 * be found
 */
Header HeaderManager::getNextHeader(std::string title)
{
    Header headerTmp;
    headerTmp.title = "";

    // If the header is a single one
    if (title == "RCTR" || title == "MODH" ||
        (title == "INDX" &&
         (actualEVEH_.title == "" || actualEVEH_.revisionNumber == 1)))
    {
        // Nothing to do because they should be unique
    }
    else if (title == "EVEH")
    {
        if (actualEVEH_.title == "")
        {
            evehIterator_ = eventsHeaders_.begin();
        }
        else
        {
            ++evehIterator_;
        }

        if (evehIterator_ != eventsHeaders_.end())
        {
            headerTmp = getEventHeader(title, -1);
        }
        else
        {
            actualEVEH_ = headerTmp;
            actualACQC_ = headerTmp;
            acqcIterator_ = -1;
        }
    }
    else if (actualEVEH_.title != "" &&
             (title == "ACQC" || title == "BEAM" || title == "INDX" ||
              title == "INFO" || title == "SLOW" || title == "ADDH"))
    {
        if (title == "ACQC")
        {
            ++acqcIterator_;
            headerTmp = getDataHeader(title, acqcIterator_);
        }
        else
        {
            headerTmp = getDataHeader(title, 0);
        }
    }

    return headerTmp;
}

/*
 * \brief Get the details relative to the previous header in the list
 * \param title: Title of the header
 * \return Return the detail of the previous header or an empty header if it
 * can't be found
 */
Header HeaderManager::getPreviousHeader(std::string title)
{
    Header headerTmp;
    headerTmp.title = "";

    // If the header is a single one
    if (title == "RCTR" || title == "MODH" ||
        (title == "INDX" &&
         (actualEVEH_.title == "" || actualEVEH_.revisionNumber == 1)))
    {
        // Nothing to do because they should be unique
    }
    else if (title == "EVEH")
    {
        if (evehIterator_ != eventsHeaders_.begin())
        {
            --evehIterator_;
            headerTmp = getEventHeader(title, -1);
        }
        else
        {
            actualEVEH_ = headerTmp;
            actualACQC_ = headerTmp;
            acqcIterator_ = -1;
        }
    }
    else if (actualEVEH_.title != "" &&
             (title == "ACQC" || title == "BEAM" || title == "INDX" ||
              title == "INFO" || title == "SLOW" || title == "ADDH"))
    {
        if (title == "ACQC")
        {
            --acqcIterator_;
            headerTmp = getDataHeader(title, acqcIterator_);
        }
        else
        {
            headerTmp = getDataHeader(title, 0);
        }
    }

    return headerTmp;
}

/*
 * \brief Get the details relative to the previous EVEH header
 * \return Return the detail of the previous EVEH header or an empty header if
 * it can't be found
 */
Header HeaderManager::getPreviousEventHeader()
{
    return getPreviousHeader("EVEH");
}

/*
 * \brief Get the details relative to the actual EVEH header
 * \param reset: Reset the ACQC
 * \return Return the detail of the actual EVEH header or an empty header if it
 * can't be found
 */
Header HeaderManager::getActualEventHeader(bool reset)
{
    if (reset)
    {
        resetAcqcHeader();
    }
    return actualEVEH_;
}

/*
 * \brief Get the details relative to the next EVEH header
 * \return Return the detail of the next EVEH header or an empty header if it
 * can't be found
 */
Header HeaderManager::getNextEventHeader()
{
    return getNextHeader("EVEH");
}

/*
 * \brief Get the details relative to the previous ACQC header (without reaching
 * another EVEH) \return Return the detail of the previous ACQC header or an
 * empty header if it can't be found
 */
Header HeaderManager::getPreviousACQCHeader()
{
    return getPreviousHeader("ACQC");
}

/*
 * \brief Get the details relative to the actual ACQC header (without reaching
 * another EVEH) \return Return the detail of the actual ACQC header or an empty
 * header if it can't be found
 */
Header HeaderManager::getActualACQCHeader()
{
    return actualACQC_;
}

/*
 * \brief Get the details relative to the next ACQC header (without reaching
 * another EVEH) \return Return the detail of the next ACQC header or an empty
 * header if it can't be found
 */
Header HeaderManager::getNextACQCHeader()
{
    return getNextHeader("ACQC");
}

/*
 * \brief Get the details relative to the ACQC header which match with detector
 * type and the detector if given in parameter (without reaching another EVEH)
 * \param detectorType: Name of the detector
 * \param detectorId: ID of the detector
 * \return Return the detail of the ACQC header or an empty header if it can't
 * be found
 */
Header HeaderManager::getHeaderACQC(std::string detectorType, int32_t detectorId)
{
    Header headerTmp;
    headerTmp.title = "";

    if (actualEVEH_.title != "")
    {
        int32_t id = 0;
        // If the header is related to an event
        for (std::vector<Header>::iterator header =
                 eventsHeaders_[actualEVEH_.eventNumber].begin();
             header != eventsHeaders_[actualEVEH_.eventNumber].end(); ++header)
        {
            if ((*header).title == "ACQC" &&
                (*header).detectorType == detectorType &&
                (*header).detectorId == detectorId)
            {
                acqcIterator_ = id;
                actualACQC_ = (*header);
                headerTmp = (*header);
                break;
            }
            ++id;
        }
    }

    // If the header can't be found everything must be reset
    if (headerTmp.title == "")
    {
        actualACQC_ = headerTmp;
        acqcIterator_ = -1;
    }

    return headerTmp;
}

/*
 * \brief Reset properly the variable actualACQC_
 */
void HeaderManager::resetAcqcHeader()
{
    Header header;
    header.title = "";
    actualACQC_ = header;
    acqcIterator_ = -1;
}

/*
 * \brief Get the list of the event number found and the count of ACQC
 * \return Return the list containing all the info loaded
 */
std::vector<EVENT_INFO> HeaderManager::getListOfEvents()
{
    std::vector<EVENT_INFO> res;

    // Fill the vector with the event in memory
    for (std::map<int32_t, std::vector<Header>>::iterator eventIt =
             eventsHeaders_.begin();
         eventIt != eventsHeaders_.end(); ++eventIt)
    {
        EVENT_INFO eventInfo = getEventInfo((*eventIt).first);
        res.push_back(eventInfo);
    }
    return res;
}

/*
 * \brief Get the information of an event number
 * \param key: key used into the map
 * \return Return the information of the event
 */
EVENT_INFO HeaderManager::getEventInfo(int32_t key)
{
    EVENT_INFO eventInfo = {key, 0, 0};

    // Count the amount of ACQC header related to this event number
    for (std::vector<Header>::iterator headerIt = eventsHeaders_[key].begin();
         headerIt != eventsHeaders_[key].end(); ++headerIt)
    {
        if ((*headerIt).title == "ACQC")
        {
            ++eventInfo.acqcCount;
        }
        else if ((*headerIt).title == "EVEH")
        {
            eventInfo.file = (*headerIt).file;
        }
    }
    return eventInfo;
}

/*
 * \brief Get the information of an event number
 * \param key: event number used as key into the map
 * \return Return the information of the event
 */
std::vector<ACQC_INFO> HeaderManager::getACQCList(int32_t key)
{
    std::vector<ACQC_INFO> res;

    // Count the amount of ACQC header related to this event number
    for (std::vector<Header>::iterator headerIt = eventsHeaders_[key].begin();
         headerIt != eventsHeaders_[key].end(); ++headerIt)
    {
        if ((*headerIt).title == "ACQC")
        {
            ACQC_INFO acqc_info;
            acqc_info.detectorType = (*headerIt).detectorType;
            acqc_info.detectorId = (*headerIt).detectorId;
            res.push_back(acqc_info);
        }
    }
    return res;
}

/*
 * \brief Get the next event key
 * \return Return the next key or -1 if there is no more
 */
int32_t HeaderManager::getNextEventNumber()
{
    int32_t res = -1;
    std::map<int32_t, std::vector<Header>>::iterator eventIt;
    if (actualEVEH_.title == "")
    {
        eventIt = eventsHeaders_.begin();
        res = (*eventIt).first;
    }
    else
    {
        eventIt = evehIterator_;
        ++eventIt;
        if (eventIt != eventsHeaders_.end())
        {
            res = (*eventIt).first;
        }
    }

    return res;
}

/*
 * \brief Get the previous event key
 * \return Return the previous key or -1 if there is no more
 */
int32_t HeaderManager::getPreviousEventNumber()
{
    int32_t res = -1;
    std::map<int32_t, std::vector<Header>>::iterator eventIt;
    if (!eventsHeaders_.empty())
    {
        if (actualEVEH_.title == "")
        {
            eventIt = eventsHeaders_.end();
            --eventIt;
            res = (*eventIt).first;
        }
        else
        {
            if (evehIterator_ != eventsHeaders_.begin())
            {
                eventIt = evehIterator_;
                --eventIt;
                res = (*eventIt).first;
            }
        }
    }

    return res;
}

/*
 * \brief Get the first event number in a file
 * \param file: Handler of the file related to the search
 * \return Return the event number or -1 if there is no one
 */
int32_t HeaderManager::getFirstEventOf(int32_t file)
{
    for (std::map<int32_t, std::vector<Header>>::iterator mapIter =
             eventsHeaders_.begin();
         mapIter != eventsHeaders_.end(); ++mapIter)
    {
        std::vector<Header> value = mapIter->second;
        for (std::vector<Header>::iterator vectorIter = value.begin();
             vectorIter != value.end(); ++vectorIter)
        {
            if (vectorIter->file == file && vectorIter->title == "EVEH")
            {
                return vectorIter->eventNumber;
            }
        }
    }
    return -1;
}

/*
 * \brief Display the list of the headers found
 */
void HeaderManager::toString()
{
    std::cout << "List of the headers found" << std::endl;
    for (std::list<Header>::iterator single = singleHeaders_.begin();
         single != singleHeaders_.end(); ++single)
    {
        std::cout << "    - " << (*single).title << " [" << (*single).file
                  << " at " << (*single).pos << "]" << std::endl;
    }

    for (std::map<int32_t, std::vector<Header>>::iterator events =
             eventsHeaders_.begin();
         events != eventsHeaders_.end(); ++events)
    {
        std::cout << "    - " << (*events).first << ":" << std::endl;
        for (std::vector<Header>::iterator event = (*events).second.begin();
             event != (*events).second.end(); ++event)
        {
            std::cout << "        - " << (*event).title << " [" << (*event).file
                      << " at " << (*event).pos << "]" << std::endl;
        }
    }
}
} // namespace lib
} // namespace ntof
