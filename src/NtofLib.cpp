/*
 * NtofLib.cpp
 *
 *  Created on: Jan 22, 2015
 *      Author: agiraud
 */

#include "NtofLib.h"

#include <iostream>
#include <sstream>

#include "FileCache.h"
#include "HeaderManager.h"
#include "NtofLibConfig.h"
#include "Reader.h"
#include "ReaderLocal.h"

#ifdef CASTOR
#include "ReaderCastorCached.h"
#endif

namespace ntof {
namespace lib {

const std::string VERSION = NTOFLIB_VERSION;
/**
 * \brief Constructor.
 */
NtofLib::NtofLib() :
    rctr_(NULL),
    modh_(NULL),
    eveh_(NULL),
    acqc_(NULL),
    beam_(NULL),
    indx_(NULL),
    info_(NULL),
    slow_(NULL),
    addh_(NULL),
    cache_(new FileCache(0, boost::filesystem::path())),
#ifdef CASTOR
    readerCastor_(new ReaderCastorCached(cache_)),
#endif
    readerLocal_(new ReaderLocal()),
    notif_(NULL)
{}

/**
 * \brief Constructor.
 * \param path Path of the file you want to read.
 */
NtofLib::NtofLib(const std::string &path) :
    rctr_(NULL),
    modh_(NULL),
    eveh_(NULL),
    acqc_(NULL),
    beam_(NULL),
    indx_(NULL),
    info_(NULL),
    slow_(NULL),
    addh_(NULL),
    cache_(new FileCache(0, boost::filesystem::path())),
#ifdef CASTOR
    readerCastor_(new ReaderCastorCached(cache_)),
#endif
    readerLocal_(new ReaderLocal()),
    notif_(NULL)
{
    readFile(path);
}

/**
 * \brief Constructor.
 * \param paths: List of path of the files to read.
 */
NtofLib::NtofLib(const FileList &paths) :
    rctr_(NULL),
    modh_(NULL),
    eveh_(NULL),
    acqc_(NULL),
    beam_(NULL),
    indx_(NULL),
    info_(NULL),
    slow_(NULL),
    addh_(NULL),
    cache_(new FileCache(0, boost::filesystem::path())),
#ifdef CASTOR
    readerCastor_(new ReaderCastorCached(cache_)),
#endif
    readerLocal_(new ReaderLocal()),
    notif_(NULL)
{
    readFiles(paths);
}

NtofLib::NtofLib(NtofLib &&other) :
    rctr_(other.rctr_),
    modh_(other.modh_),
    eveh_(other.eveh_),
    acqc_(other.acqc_),
    beam_(other.beam_),
    indx_(other.indx_),
    info_(other.info_),
    slow_(other.slow_),
    addh_(other.addh_),
    cache_(other.cache_),
#ifdef CASTOR
    readerCastor_(std::move(other.readerCastor_)),
#endif
    readerLocal_(std::move(other.readerLocal_)),
    notif_(notif_)
{
    other.rctr_ = NULL;
    other.modh_ = NULL;
    other.eveh_ = NULL;
    other.acqc_ = NULL;
    other.beam_ = NULL;
    other.indx_ = NULL;
    other.info_ = NULL;
    other.slow_ = NULL;
    other.addh_ = NULL;
    other.cache_ = NULL;
    other.notif_ = NULL;
}

/**
 * \brief Destructor.
 */
NtofLib::~NtofLib()
{
    closeAllFiles();
}

/*
 * \brief Initialize the RCTR and the MODH (+ INDX if possible)
 */
void NtofLib::initHeaders()
{
    if (!isDefinedRCTR() || !isDefinedMODH())
    {
        initRCTR(HeaderManager::getInstance()->getHeader("RCTR"));
        initMODH(HeaderManager::getInstance()->getHeader("MODH"));
    }

    Header index = HeaderManager::getInstance()->getHeader("INDX");
    if (!isDefinedINDX() && index.revisionNumber == 1)
    {
        initINDX(index);
    }
}

/*
 * \brief Add a list of files to the open files
 * \param paths: The list of full paths to open
 */
void NtofLib::readMoreFiles(const FileList &paths)
{
    FileInfoList infoList;
    prepareFiles(paths, infoList, true);
    for (const FILE_INFO &info : infoList)
        readAnotherFile(info);
    initHeaders();
}

/**
 * \brief Initialize the reading of the file.
 * \param paths: List of path of the files to read.
 * \param closeOldFiles: Close the files already open by default true
 */
void NtofLib::readFiles(const FileList &paths, bool closeOldFiles)
{
    if (closeOldFiles)
    {
        closeAllFiles();
    }

    // TODO order correctly the files

    readMoreFiles(paths);
}

/*
 * \brief Get the details from the name of the file
 * \param path: Full path of the file
 * \return Return the details of the file
 */
FILE_INFO NtofLib::getFileInfo(const std::string &path) const
{
    FILE_INFO fileInfo;

    uint64_t lastSlashPos = path.find_last_of('/');
    std::string name = path.substr(lastSlashPos + 1, path.size() + 1);

    // remove params when using url based file (ex: xroot://xxx?svcClass=default)
    size_t paramsPos = name.find_last_of('?');
    if (paramsPos != std::string::npos)
        name = name.substr(0, paramsPos);

    size_t firstUnderscorePos = name.find_first_of('_');
    size_t secondUnderscorePos = name.find_first_of('_', firstUnderscorePos + 1);
    size_t firstDotPos = name.find_first_of('.');
    size_t lastDotPos = name.find_last_of('.');
    size_t sizeOfRun = sizeof("run") - 1;

    // Get the run number
    std::string runNumber = "-1";
    if (firstUnderscorePos != std::string::npos)
    {
        runNumber = name.substr(sizeOfRun, firstUnderscorePos - sizeOfRun);
    }
    else
    {
        if (firstDotPos != std::string::npos)
        {
            runNumber = name.substr(sizeOfRun, firstDotPos - sizeOfRun);
        }
        else
        {
            throw NtofLibException("The position of the first '.' is undefined",
                                   __FILE__, __LINE__);
        }
    }

    // Get the segment number
    std::string eventNumber = "-1";
    if (firstUnderscorePos != std::string::npos)
    {
        if (secondUnderscorePos != std::string::npos)
        {
            eventNumber = name.substr(
                firstUnderscorePos + 1,
                secondUnderscorePos - firstUnderscorePos - 1);
        }
        else
        {
            eventNumber = name.substr(firstUnderscorePos + 1,
                                      firstDotPos - firstUnderscorePos - 1);
        }
    }

    // Define if the file is finished or not
    if (lastDotPos != std::string::npos)
    {
        std::string extension = name.substr(lastDotPos + 1);
        fileInfo.setFlag(FILE_INFO::IS_FINISHED, extension == "finished");
        fileInfo.extension = extension;
    }

    //            std::cerr << " *** FILE *** " << std::endl;
    //            std::cerr << "_: " << firstUnderscorePos << " ?= " <<
    //            std::string::npos << std::endl; std::cerr << "_: " <<
    //            secondUnderscorePos << " ?= " << std::string::npos <<
    //            std::endl; std::cerr << "Run number : " << runNumber <<
    //            std::endl; std::cerr << "Event number : " << eventNumber <<
    //            std::endl;

    fileInfo.filename = name;
    fileInfo.fullpath = path;

    // Convert the run number
    std::istringstream runNumberStr(runNumber);
    int32_t runNumberValue;
    runNumberStr >> runNumberValue;
    fileInfo.runNumber = runNumberValue;

    // Convert the segment number
    std::istringstream eventNumberStr(eventNumber);
    int32_t eventNumberValue;
    eventNumberStr >> eventNumberValue;
    fileInfo.segmentNumber = eventNumberValue;

    return fileInfo;
}

/*
 * \brief Add another file to the list of open files
 * \param path: Full path of the file to open
 */
void NtofLib::readAnotherFile(const FILE_INFO &fileInfo)
{
    FILE_INFO info(fileInfo);
    FILE_INFO fileInfoTmp;
    if (isOnCastor(fileInfo.fullpath))
    {
#ifdef CASTOR
        fileInfoTmp = readerCastor_->openFile(fileInfo, notif_);
#else
        throw NtofLibException("This library doesn't support the CASTOR paths",
                               __FILE__, __LINE__);
#endif /* IFDEF CASTOR */
    }
    else
    {
        fileInfoTmp = readerLocal_->openFile(fileInfo, notif_);
    }

    info.file = fileInfoTmp.file;
    info.size = fileInfoTmp.size;
    info.setFlag(FILE_INFO::ON_CASTOR,
                 fileInfoTmp.hasFlag(FILE_INFO::ON_CASTOR));

    filenames_.push_back(std::move(info));
}

void NtofLib::prepareFiles(const FileList &paths,
                           FileInfoList &fileInfoList,
                           bool checkLoaded) const
{
    for (const std::string &file : paths)
    {
        FILE_INFO fileInfo = getFileInfo(file);

        if (checkLoaded)
        {
            for (const FILE_INFO &info : filenames_)
            {
                if (info.filename == fileInfo.filename)
                {
                    if (info.hasFlag(FILE_INFO::NO_CACHE))
                        fileInfo.setFlag(FILE_INFO::NO_CACHE);
                    else
                    {
                        throw NtofLibException(
                            "The same file has been found twice", __FILE__,
                            __LINE__);
                    }
                }

                if (info.fullpath == fileInfo.fullpath)
                {
                    throw NtofLibException("The same file has been found twice",
                                           __FILE__, __LINE__);
                }
                else if (info.runNumber != fileInfo.runNumber)
                {
                    throw NtofLibException(
                        "The files are not from the same run", __FILE__,
                        __LINE__);
                }
            }
        }

        for (FILE_INFO &info : fileInfoList)
        {
            if (info.filename == fileInfo.filename)
            {
                info.setFlag(FILE_INFO::NO_CACHE);
                fileInfo.setFlag(FILE_INFO::NO_CACHE);
            }

            if (info.fullpath == fileInfo.fullpath)
            {
                throw NtofLibException("The same file has been found twice",
                                       __FILE__, __LINE__);
            }
            else if (info.runNumber != fileInfo.runNumber)
            {
                throw NtofLibException("The files are not from the same run",
                                       __FILE__, __LINE__);
            }
        }

        fileInfoList.push_back(std::move(fileInfo));
    }
}

/*
 * \brief Get the details from the name of the file
 * \param file: File handler
 * \return Return the details of the file
 */
FILE_INFO NtofLib::getFileInfo(int32_t file) const
{
    for (const FILE_INFO &info : filenames_)
    {
        if (info.file == file)
            return info;
    }

    return FILE_INFO();
}

/**
 * \brief Initialize the reading of the file.
 * \param path Path of the file you want to read.
 * \param closeOldFiles: Close the files already open by default true
 */
void NtofLib::readFile(const std::string &path, bool closeOldFiles)
{
    if (closeOldFiles)
    {
        closeAllFiles();
    }
    FileInfoList info;
    prepareFiles(FileList(1, path), info, true);
    readAnotherFile(info.front());
    initHeaders();
}

/*
 * \brief Close all the files opened
 */
void NtofLib::closeAllFiles()
{
    for (std::vector<FILE_INFO>::iterator it = filenames_.begin();
         it != filenames_.end(); ++it)
    {
        if ((*it).isOnCastor)
        {
#ifdef CASTOR
            readerCastor_->closeFile((*it).file);
#else
            throw NtofLibException(
                "This library doesn't support the CASTOR paths.", __FILE__,
                __LINE__);
#endif /* IFDEF CASTOR */
        }
        else
        {
            readerLocal_->closeFile((*it).file);
        }
    }

    // Clear headers
    if (rctr_ != NULL)
    {
        delete rctr_;
        rctr_ = NULL;
    }
    if (modh_ != NULL)
    {
        delete modh_;
        modh_ = NULL;
    }
    if (eveh_ != NULL)
    {
        delete eveh_;
        eveh_ = NULL;
    }
    if (acqc_ != NULL)
    {
        delete acqc_;
        acqc_ = NULL;
    }
    if (beam_ != NULL)
    {
        delete beam_;
        beam_ = NULL;
    }
    if (indx_ != NULL)
    {
        delete indx_;
        indx_ = NULL;
    }
    if (info_ != NULL)
    {
        delete info_;
        info_ = NULL;
    }
    if (slow_ != NULL)
    {
        delete slow_;
        slow_ = NULL;
    }
    if (addh_ != NULL)
    {
        delete addh_;
        addh_ = NULL;
    }

    filenames_.clear();
    HeaderManager::getInstance()->clear();
}

/* **************************************************** */
/* Check the properties of files                        */
/* **************************************************** */
/**
 * \brief All to know if the path is located on CASTOR
 * \param path The path to test
 * \return Return true if the file is on CASTOR or false if it isn't
 */
bool NtofLib::isOnCastor(const std::string &path)
{
    if (path.find("/castor/cern.ch/") == 0 ||
        path.compare(0, 8, "xroot://") == 0)
    {
#ifdef CASTOR
        return true;
#else
        throw NtofLibException("This library doesn't support the CASTOR paths.",
                               __FILE__, __LINE__);
#endif /* IFDEF CASTOR */
    }
    return false;
}

/**
 * \brief All to know if the path is an index file
 * \param path The path to test
 * \return Return true if the file is an index file or false if it isn't
 */
bool NtofLib::isIDX(const std::string &path)
{
    if (path.find(".idx") != std::string::npos)
    {
        return true;
    }
    return false;
}

/**
 * \brief Allow to know if the file is an index file.
 * \return Return true if the file is an index file or false if it isn't.
 */
bool NtofLib::isIDX()
{
    if (filenames_.empty())
    {
        throw NtofLibException("No file detected.", __FILE__, __LINE__);
    }
    return isIDX(filenames_[0].fullpath);
}

/*
 * \brief Allow to check if a file exists
 * \param name: full path of the file
 * \return Return true if the file exists of false in the other case
 */
inline bool NtofLib::fileExists(const std::string &name)
{
    struct stat buffer;
    return (stat((char *) name.c_str(), &buffer) == 0);
}
/* **************************************************** */
/* Navigate in the file                                 */
/* **************************************************** */
/**
 * \brief Initialize the reader of the previous event.
 * \return Return true if the event is ready to be read or false if there is no
 * more event in the file.
 */
bool NtofLib::getPreviousEvent()
{
    Header eventHeader = HeaderManager::getInstance()->getPreviousEventHeader();
    if (eventHeader.title == "")
    {
        return false;
    }
    else
    {
        initEVEH(eventHeader);
        initBEAM(HeaderManager::getInstance()->getNextHeader("BEAM"));
        initINDX(HeaderManager::getInstance()->getNextHeader("INDX"));
        initINFO(HeaderManager::getInstance()->getNextHeader("INFO"));
        initSLOW(HeaderManager::getInstance()->getNextHeader("SLOW"));
        initADDH(HeaderManager::getInstance()->getNextHeader("ADDH"));
        /*
         if (isIDX()){
         switch (eventHeader.revisionNumber) {
         case 0:
         initBEAM(HeaderManager::getInstance()->getNextHeader("BEAM"));
         initINDX(HeaderManager::getInstance()->getNextHeader("INDX"));
         initINFO(HeaderManager::getInstance()->getNextHeader("INFO"));
         initSLOW(HeaderManager::getInstance()->getNextHeader("SLOW"));
         break;
         case 1:
         initADDH(HeaderManager::getInstance()->getNextHeader("ADDH"));
         break;
         default:
         throw NtofLibException("Revision number of the event header unknown",
         __FILE__, __LINE__);
         }
         } else{
         switch (eventHeader.revisionNumber) {
         case 0:
         //Do nothing more
         break;
         case 1:
         initADDH(HeaderManager::getInstance()->getNextHeader("ADDH"));
         break;
         default:
         throw NtofLibException("Revision number of the event header unknown",
         __FILE__, __LINE__);
         }
         }*/
        return true;
    }
}

/**
 * \brief Initialize the reader of the next event.
 * \return Return true if the event is ready to be read or false if there is no
 * more event in the file.
 */
bool NtofLib::getNextEvent()
{
    Header eventHeader = HeaderManager::getInstance()->getNextEventHeader();
    if (eventHeader.title == "")
    {
        return false;
    }
    else
    {
        initEVEH(eventHeader);
        initBEAM(HeaderManager::getInstance()->getNextHeader("BEAM"));
        initINDX(HeaderManager::getInstance()->getNextHeader("INDX"));
        initINFO(HeaderManager::getInstance()->getNextHeader("INFO"));
        initSLOW(HeaderManager::getInstance()->getNextHeader("SLOW"));
        initADDH(HeaderManager::getInstance()->getNextHeader("ADDH"));
        return true;
    }
}

/**
 * \brief Jump directly to a validated event
 * \param validatedEvent: id of the validated event to jump
 * \return Return true if the event is ready to be read or false if the event
 * doesn't exist in the file.
 */
bool NtofLib::getValidatedEvent(int32_t validEvent)
{
    Header eventHeader = HeaderManager::getInstance()->getValidatedEventHeader(
        validEvent);
    if (eventHeader.title == "")
    {
        return false;
    }
    else
    {
        initEVEH(eventHeader);
        initBEAM(HeaderManager::getInstance()->getNextHeader("BEAM"));
        initINDX(HeaderManager::getInstance()->getNextHeader("INDX"));
        initINFO(HeaderManager::getInstance()->getNextHeader("INFO"));
        initSLOW(HeaderManager::getInstance()->getNextHeader("SLOW"));
        initADDH(HeaderManager::getInstance()->getNextHeader("ADDH"));
        return true;
    }
}
/**
 * \brief Jump directly to a particular event.
 * \param event id of the event to jump
 * \return Return true if the event is ready to be read or false if the event
 * doesn't exist in the file.
 */
bool NtofLib::getEvent(int32_t event)
{
    Header eventHeader = HeaderManager::getInstance()->getHeader("EVEH", event);
    if (eventHeader.title == "")
    {
        return false;
    }
    else
    {
        initEVEH(eventHeader);
        initBEAM(HeaderManager::getInstance()->getNextHeader("BEAM"));
        initINDX(HeaderManager::getInstance()->getNextHeader("INDX"));
        initINFO(HeaderManager::getInstance()->getNextHeader("INFO"));
        initSLOW(HeaderManager::getInstance()->getNextHeader("SLOW"));
        initADDH(HeaderManager::getInstance()->getNextHeader("ADDH"));
        return true;
    }
}

/**
 * \brief Initialize the reader of the previous ACQC, according the actual event
 * that is being read. \return Return true if the ACQC is ready to be read or
 * false if there is no more event in the file.
 */
bool NtofLib::getPreviousACQC()
{
    Header acqcHeader = HeaderManager::getInstance()->getPreviousACQCHeader();
    if (acqcHeader.title == "")
    {
        HeaderManager::getInstance()->getActualEventHeader();
        return false;
    }
    else
    {
        initACQC(acqcHeader);
        return true;
    }
}

/**
 * \brief Initialize the reader of the next ACQC, according the actual event
 * that is being read. \return Return true if the ACQC is ready to be read or
 * false if there is no more event in the file.
 */
bool NtofLib::getNextACQC()
{
    Header acqcHeader = HeaderManager::getInstance()->getNextACQCHeader();
    if (acqcHeader.title == "")
    {
        // HeaderManager::getInstance()->getActualEventHeader();
        return false;
    }
    else
    {
        initACQC(acqcHeader);
        return true;
    }
}

/**
 * \brief Initialize the reader of the next ACQC, according the actual event
 * that is being read. \param acqc id to jump on \return Return true if the ACQC
 * is ready to be read or false if there is no more event in the file.
 */
bool NtofLib::getACQC(int32_t number)
{
    Header acqcHeader = HeaderManager::getInstance()->getHeader("ACQC", number);
    if (acqcHeader.title == "")
    {
        // HeaderManager::getInstance()->getActualEventHeader();
        return false;
    }
    else
    {
        initACQC(acqcHeader);
        return true;
    }
}

/**
 * \brief Initialize the reader of the next ACQC, according the actual event
 * that is being read. \param detectorType The type of the detector \param
 * detectorId The Id of the detector \return Return true if the ACQC is ready to
 * be read or false if there is no more event in the file.
 */
bool NtofLib::getACQC(const std::string &detectorType, int32_t detectorId)
{
    Header acqcHeader = HeaderManager::getInstance()->getHeaderACQC(
        detectorType, detectorId);
    if (acqcHeader.title == "")
    {
        // HeaderManager::getInstance()->getActualEventHeader();
        return false;
    }
    else
    {
        initACQC(acqcHeader);
        return true;
    }
}

/* ************************************************ */
/* GETTERS                                          */
/* ************************************************ */
/*
 * \brief Get the number of events found
 * \return Return the number of events found
 */
int32_t NtofLib::getNumberOfEvents()
{
    return HeaderManager::getInstance()->getNumberOfEvents();
}

/**
 * \brief All to know the ID of the actual EVEH
 * \return Return the ID of the EVEH in the file
 */
int64_t NtofLib::getActualEvent()
{
    return HeaderManager::getInstance()->getIdEveh();
}

/**
 * \brief Get the RCTR reader
 * \return Return the pointer of the reader or throw a
 * ntof::lib::NtofLibException if the pointer is null
 */
ReaderStructRCTR *NtofLib::getRCTR()
{
    if (rctr_ == NULL)
    {
        throw NtofLibException("RCTR isn't initialized.", __FILE__, __LINE__);
    }
    return rctr_;
}

/**
 * \brief Get the MODH reader
 * \return Return the pointer of the reader or throw a
 * ntof::lib::NtofLibException if the pointer is null
 */
ReaderStructMODH *NtofLib::getMODH()
{
    if (modh_ == NULL)
    {
        throw NtofLibException("MODH isn't initialized.", __FILE__, __LINE__);
    }
    return modh_;
}

/**
 * \brief Get the EVEH reader
 * \return Return the pointer of the reader or throw a
 * ntof::lib::NtofLibException if the pointer is null
 */
ReaderStructEVEH *NtofLib::getEVEH()
{
    if (eveh_ == NULL)
    {
        throw NtofLibException("EVEH isn't initialized.", __FILE__, __LINE__);
    }
    return eveh_;
}

/**
 * \brief Get the ACQC reader
 * \return Return the pointer of the reader or throw a
 * ntof::lib::NtofLibException if the pointer is null
 */
ReaderStructACQC *NtofLib::getACQC()
{
    if (acqc_ == NULL)
    {
        throw NtofLibException("ACQC isn't initialized.", __FILE__, __LINE__);
    }
    return acqc_;
}

/**
 * \brief Get the BEAM reader
 * \return Return the pointer of the reader or throw a
 * ntof::lib::NtofLibException if the pointer is null
 */
ReaderStructBEAM *NtofLib::getBEAM()
{
    if (beam_ == NULL)
    {
        throw NtofLibException("BEAM isn't initialized.", __FILE__, __LINE__);
    }
    return beam_;
}

/**
 * \brief Get the INFO reader
 * \return Return the pointer of the reader or throw a
 * ntof::lib::NtofLibException if the pointer is null
 */
ReaderStructINFO *NtofLib::getINFO()
{
    if (info_ == NULL)
    {
        throw NtofLibException("INFO isn't initialized.", __FILE__, __LINE__);
    }
    return info_;
}

/**
 * \brief Get the INDX reader
 * \return Return the pointer of the reader or throw a
 * ntof::lib::NtofLibException if the pointer is null
 */
ReaderStructINDX *NtofLib::getINDX()
{
    if (indx_ == NULL)
    {
        throw NtofLibException("INDX isn't initialized.", __FILE__, __LINE__);
    }
    return indx_;
}

/**
 * \brief Get the SLOW reader
 * \return Return the pointer of the reader or throw a
 * ntof::lib::NtofLibException if the pointer is null
 */
ReaderStructSLOW *NtofLib::getSLOW()
{
    if (slow_ == NULL)
    {
        throw NtofLibException("SLOW isn't initialized.", __FILE__, __LINE__);
    }
    return slow_;
}

/**
 * \brief Get the ADDH reader
 * \return Return the pointer of the reader or throw a
 * ntof::lib::NtofLibException if the pointer is null
 */
ReaderStructADDH *NtofLib::getADDH()
{
    if (addh_ == NULL)
    {
        throw NtofLibException("ADDH isn't initialized.", __FILE__, __LINE__);
    }
    return addh_;
}

/**
 * \brief Give the version of the lib as a string.
 * \return the version of the lib.
 */
const std::string &NtofLib::getVersion()
{
    return VERSION;
}
/* ************************************************ */
/* IS DEFINED                                       */
/* ************************************************ */
/*
 * \brief Allow to know if the header is defined
 * \return Return true if the header is defined or false if it's not
 */
bool NtofLib::isDefinedRCTR()
{
    return rctr_ == NULL ? false : true;
}

/*
 * \brief Allow to know if the header is defined
 * \return Return true if the header is defined or false if it's not
 */
bool NtofLib::isDefinedMODH()
{
    return modh_ == NULL ? false : true;
}

/*
 * \brief Allow to know if the header is defined
 * \return Return true if the header is defined or false if it's not
 */
bool NtofLib::isDefinedEVEH()
{
    return eveh_ == NULL ? false : true;
}

/*
 * \brief Allow to know if the header is defined
 * \return Return true if the header is defined or false if it's not
 */
bool NtofLib::isDefinedACQC()
{
    return acqc_ == NULL ? false : true;
}

/*
 * \brief Allow to know if the header is defined
 * \return Return true if the header is defined or false if it's not
 */
bool NtofLib::isDefinedBEAM()
{
    return beam_ == NULL ? false : true;
}

/*
 * \brief Allow to know if the header is defined
 * \return Return true if the header is defined or false if it's not
 */
bool NtofLib::isDefinedINDX()
{
    return indx_ == NULL ? false : true;
}

/*
 * \brief Allow to know if the header is defined
 * \return Return true if the header is defined or false if it's not
 */
bool NtofLib::isDefinedINFO()
{
    return info_ == NULL ? false : true;
}

/*
 * \brief Allow to know if the header is defined
 * \return Return true if the header is defined or false if it's not
 */
bool NtofLib::isDefinedSLOW()
{
    return slow_ == NULL ? false : true;
}

/*
 * \brief Allow to know if the header is defined
 * \return Return true if the header is defined or false if it's not
 */
bool NtofLib::isDefinedADDH()
{
    return addh_ == NULL ? false : true;
}
/* ************************************************ */
/* INITIALISATION                                   */
/* ************************************************ */
/*
 * \brief Allow to initialize the header
 * \param header Details of the header
 */
void NtofLib::initRCTR(const Header &header)
{
    if (rctr_ != NULL)
    {
        delete rctr_;
        rctr_ = NULL;
    }
    if (header.title != "")
    {
        rctr_ = new ReaderStructRCTR(header, (*this));
    }
}

/*
 * \brief Allow to initialize the header
 * \param header Details of the header
 */
void NtofLib::initMODH(const Header &header)
{
    if (modh_ != NULL)
    {
        delete modh_;
        modh_ = NULL;
    }
    if (header.title != "")
    {
        modh_ = new ReaderStructMODH(header, (*this));
    }
}

/*
 * \brief Allow to initialize the header
 * \param header Details of the header
 */
void NtofLib::initEVEH(const Header &header)
{
    if (eveh_ != NULL)
    {
        delete eveh_;
        eveh_ = NULL;
    }
    if (header.title != "")
    {
        eveh_ = new ReaderStructEVEH(header, (*this));
    }
}

/*
 * \brief Allow to initialize the header
 * \param header Details of the header
 */
void NtofLib::initACQC(const Header &header)
{
    if (acqc_ != NULL)
    {
        delete acqc_;
        acqc_ = NULL;
    }
    if (header.title != "")
    {
        acqc_ = new ReaderStructACQC(header, (*this));
    }
}

/*
 * \brief Allow to initialize the header
 * \param header Details of the header
 */
void NtofLib::initBEAM(const Header &header)
{
    if (beam_ != NULL)
    {
        delete beam_;
        beam_ = NULL;
    }
    if (header.title != "")
    {
        beam_ = new ReaderStructBEAM(header, (*this));
    }
}

/*
 * \brief Allow to initialize the header
 * \param header Details of the header
 */
void NtofLib::initINDX(const Header &header)
{
    if (indx_ != NULL)
    {
        delete indx_;
        indx_ = NULL;
    }
    if (header.title != "")
    {
        indx_ = new ReaderStructINDX(header, (*this));
    }
}

/*
 * \brief Allow to initialize the header
 * \param header Details of the header
 */
void NtofLib::initINFO(const Header &header)
{
    if (info_ != NULL)
    {
        delete info_;
        info_ = NULL;
    }
    if (header.title != "")
    {
        info_ = new ReaderStructINFO(header, (*this));
    }
}

/*
 * \brief Allow to initialize the header
 * \param header Details of the header
 */
void NtofLib::initSLOW(const Header &header)
{
    if (slow_ != NULL)
    {
        delete slow_;
        slow_ = NULL;
    }
    if (header.title != "")
    {
        slow_ = new ReaderStructSLOW(header, (*this));
    }
}

/*
 * \brief Allow to initialize the header
 * \param header Details of the header
 */
void NtofLib::initADDH(const Header &header)
{
    if (addh_ != NULL)
    {
        delete addh_;
        addh_ = NULL;
    }
    if (header.title != "")
    {
        addh_ = new ReaderStructADDH(header, (*this));
    }
}

/**
 * \brief Get the reader
 * \param isOnCastor: Define the reader to return
 * \return Return the pointer of the reader or throw a
 * ntof::lib::NtofLibException if the pointer is null
 */
Reader *NtofLib::getReader(bool isOnCastor)
{
    if (isOnCastor)
    {
#ifdef CASTOR
        if (readerCastor_ == NULL)
        {
            throw NtofLibException("The reader is not initialized.", __FILE__,
                                   __LINE__);
        }
        return readerCastor_.get();
#else
        throw NtofLibException("This library doesn't support the CASTOR paths.",
                               __FILE__, __LINE__);
#endif /* IFDEF CASTOR */
    }
    else
    {
        if (readerLocal_ == NULL)
        {
            throw NtofLibException("The reader is not initialized.", __FILE__,
                                   __LINE__);
        }
        // std::cerr << readerLocal_ << std::endl; //TODO
        return readerLocal_.get();
    }
}

/*
 * \brief Get the name of the file opened
 * \return Return the name of the file opened
 */
std::string NtofLib::getFilename()
{
    std::string res = "";
    if (!filenames_.empty())
    {
        res = filenames_[0].fullpath;
    }
    return res;
}

/*
 * \brief Get the list of the files opened
 * \return Return a vector which contains the list of all the full paths opened
 */
std::vector<std::string> NtofLib::getFilenames()
{
    std::vector<std::string> res;
    for (std::vector<FILE_INFO>::iterator it = filenames_.begin();
         it != filenames_.end(); ++it)
    {
        res.push_back((*it).fullpath);
    }
    return res;
}

/*
 * \brief Get the list of the event number found and the count of ACQC
 * \return Return the list containing all the info loaded
 */
std::vector<EVENT_INFO> NtofLib::getListOfEvents()
{
    return HeaderManager::getInstance()->getListOfEvents();
}

/*
 * \brief Know if a previous event is available
 * \return Return true if a previous event is available
 */
bool NtofLib::hasPreviousEvent()
{
    return (HeaderManager::getInstance()->getPreviousEventNumber() != -1);
}

/*
 * \brief Get the information relative to the previous event
 * \return Return the data relative to the previous event
 */
EVENT_INFO NtofLib::getPreviousEventInfo()
{
    int32_t previousEvent =
        HeaderManager::getInstance()->getPreviousEventNumber();
    EVENT_INFO res = {0, 0};

    if (previousEvent != -1)
    {
        res = HeaderManager::getInstance()->getEventInfo(previousEvent);
    }

    return res;
}

/*
 * \brief Know if a next event is available
 * \return Return true if a next event is available
 */
bool NtofLib::hasNextEvent()
{
    return (HeaderManager::getInstance()->getNextEventNumber() != -1);
}

/*
 * \brief Get the information relative to the next event
 * \return Return the data relative to the next event
 */
EVENT_INFO NtofLib::getNextEventInfo()
{
    int32_t nextEvent = HeaderManager::getInstance()->getNextEventNumber();
    EVENT_INFO res = {0, 0};

    if (nextEvent != -1)
    {
        res = HeaderManager::getInstance()->getEventInfo(nextEvent);
    }

    return res;
}

/*
 * \brief Get the information relative to the actual event
 * \return Return the data relative to the actual event
 */
EVENT_INFO NtofLib::getEventInfo()
{
    Header actualEvent = HeaderManager::getInstance()->getActualEventHeader(
        false);
    EVENT_INFO res = {0, 0};

    if (actualEvent.title != "")
    {
        res = HeaderManager::getInstance()->getEventInfo(
            actualEvent.eventNumber);
    }

    return res;
}

/*
 * \brief Check if the file can be opened according the ones already opened
 * \param path: Full path of the file to add
 * \return Return a structure with a boolean to know if the files can be opened
 * or not and if not an error message
 */
CHECK_LIST NtofLib::checkFile(const std::string &path)
{
    CHECK_LIST res = {true, ""};

    try
    {
        FileInfoList info;
        prepareFiles(FileList(1, path), info, true);
    }
    catch (NtofLibException &ex)
    {
        res.isValid = false;
        res.message = ex.getMessage();
    }

    return res;
}

/*
 * \brief Check if all the files can be opened according the ones already opened
 * \param paths: List of the full paths to add
 * \return Return a structure with a boolean to know if the files can be opened
 * or not and if not an error message
 */
CHECK_LIST NtofLib::checkFiles(const FileList &paths)
{
    CHECK_LIST res = {true, ""};

    try
    {
        FileInfoList info;
        prepareFiles(paths, info, true);
    }
    catch (NtofLibException &ex)
    {
        res.isValid = false;
        res.message = ex.getMessage();
    }

    return res;
}
/*
 * \brief Check if all the files can be opened in once
 * \param paths: List of the full paths to add
 * \return Return a structure with a boolean to know if the files can be opened
 * or not and if not an error message
 */
CHECK_LIST NtofLib::checkList(const FileList &paths)
{
    CHECK_LIST res = {true, ""};

    try
    {
        FileInfoList info;
        prepareFiles(paths, info, false);
    }
    catch (NtofLibException &ex)
    {
        res.isValid = false;
        res.message = ex.getMessage();
    }

    return res;
}

/*
 * \brief Get the information of an event number
 * \param key: event number used as key into the map
 * \return Return the information of the event
 */
std::vector<ACQC_INFO> NtofLib::getACQCList(int32_t key)
{
    return HeaderManager::getInstance()->getACQCList(key);
}

/*
 * \brief Get the first event number in a file
 * \param fullPath: Full path of the file
 * \return Return the event number or -1 if there is no one
 */
int32_t NtofLib::getFirstEventOf(const std::string &fullPath)
{
    for (std::vector<FILE_INFO>::iterator fileInfoIter = filenames_.begin();
         fileInfoIter != filenames_.end(); ++fileInfoIter)
    {
        if (fileInfoIter->fullpath == fullPath)
        {
            return HeaderManager::getInstance()->getFirstEventOf(
                fileInfoIter->file);
        }
    }
    return -1;
}

/*
 * \brief Display the list of the headers found
 */
void NtofLib::toString()
{
    HeaderManager::getInstance()->toString();
}

#ifdef CASTOR
const std::string &NtofLib::getServiceClass() const
{
    return readerCastor_->getServiceClass();
}

void NtofLib::setServiceClass(const std::string &serviceClass)
{
    readerCastor_->setServiceClass(serviceClass);
}

const std::string &NtofLib::getStageHost() const
{
    return readerCastor_->getStageHost();
}

void NtofLib::setStageHost(const std::string &stageHost)
{
    readerCastor_->setStageHost(stageHost);
}
#endif

void NtofLib::setNotificationListener(Notification::Listener *cb)
{
    notif_ = cb;
}

} /* namespace lib */
} /* namespace ntof */
