/*
 * NtofLibException.cpp
 *
 *  Created on: Oct 13, 2014
 *      Author: mdonze
 */

#include "NtofLibException.h"

#include <sstream>

namespace ntof {
namespace lib {

/**
 * \brief Constructs this
 * \param Msg Error message
 * \param file Name of the file where the exception is thrown (__FILE__)
 * \param Line Line of code doing the error (__LINE__)
 */
NtofLibException::NtofLibException(const std::string &Msg,
                                   const std::string &file,
                                   int Line) :
    msg(Msg), file_(file), line(Line), errorCode(0)
{}

/**
 * \brief Construct this
 * \param Msg Error message
 * \param file Name of the file where the exception is thrown (__FILE__)
 * \param Line Line of code doing the error (__LINE__)
 */
NtofLibException::NtofLibException(const char *Msg,
                                   const std::string &file,
                                   int Line) :
    msg(Msg), file_(file), line(Line), errorCode(0)
{}

/**
 * \brief Construct this
 * \param Msg Error message
 * \param file Name of the file where the exception is thrown (__FILE__)
 * \param Line Line of code doing the error (__LINE__)
 * \param error Error code
 */
NtofLibException::NtofLibException(const char *Msg,
                                   const std::string &file,
                                   int Line,
                                   int error) :
    msg(Msg), file_(file), line(Line), errorCode(error)
{}

/**
 * \brief Empty constructor of the class
 */
NtofLibException::NtofLibException() : msg(""), file_(""), line(0), errorCode(0)
{}

/**
 * \brief Destructor of the class
 */
NtofLibException::~NtofLibException() throw() {}

/**
 * \brief Overload of std::exception
 * \return Return the error message
 */
const char *NtofLibException::what() const throw()
{
    std::ostringstream oss;
    oss << "Ntof Lib exception in source file " << file_ << " at line " << line
        << " : " << msg;
    return oss.str().c_str();
}

/**
 * \brief Gets the error message
 * \return Return the error message
 */
const char *NtofLibException::getMessage() const throw()
{
    return this->msg.c_str();
}

/**
 * \brief Gets the error code
 * \return
 */
int NtofLibException::getErrorCode() const throw()
{
    return errorCode;
}

/**
 * \brief Gets the source file
 * \return Source file name
 */
const std::string NtofLibException::getFile() const throw()
{
    return file_;
}

/**
 * \brief Gets the line in source file
 * \return Return the line in source file
 */
int NtofLibException::getLine() const throw()
{
    return line;
}
} // namespace lib
} // namespace ntof
