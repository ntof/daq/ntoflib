/*
 * Reader.cpp
 *
 *  Created on: Aug 11, 2015
 *      Author: agiraud
 */

#include "Reader.h"

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <vector>

#include "HeaderManager.h"
#include "NtofLibException.h"

namespace ntof {
namespace lib {

char Reader::readChar(__off_t position, int32_t file)
{
    char out;
    read(position, &out, 1, file);
    return out;
}

std::string Reader::readString(__off_t position, int32_t size, int32_t file)
{
    std::vector<char> out(size + 1, '\0');
    read(position, out.data(), size, file);
    return std::string(out.data());
}

uint32_t Reader::readUint32(__off_t position, int32_t file)
{
    uint32_t res;
    read(position, &res, sizeof(uint32_t), file);
    return res;
}

uint64_t Reader::readUint64(__off_t position, int32_t file)
{
    uint64_t res;
    read(position, &res, sizeof(uint64_t), file);
    return res;
}

int8_t Reader::readInt8(__off_t position, int32_t file)
{
    int8_t res;
    read(position, &res, sizeof(int8_t), file);
    return res;
}

int16_t Reader::readInt16(__off_t position, int32_t file)
{
    int16_t res;
    read(position, &res, sizeof(int16_t), file);
    return res;
}

int32_t Reader::readInt32(__off_t position, int32_t file)
{
    int32_t res;
    read(position, &res, sizeof(int32_t), file);
    return res;
}

int64_t Reader::readInt64(__off_t position, int32_t file)
{
    int64_t res;
    read(position, &res, sizeof(int64_t), file);
    return res;
}

float Reader::readFloat(__off_t position, int32_t file)
{
    float res;
    read(position, &res, sizeof(float), file);
    return res;
}

double Reader::readDouble(__off_t position, int32_t file)
{
    double res;
    read(position, &res, sizeof(double), file);
    return res;
}

void Reader::readVectorInt8(__off_t position,
                            int64_t length,
                            std::vector<int8_t> &vector,
                            int32_t file)
{
    vector.resize(length * sizeof(int8_t));
    read(position, vector.data(), length * sizeof(int8_t), file);
}

void Reader::readVectorInt16(__off_t position,
                             int64_t length,
                             std::vector<int16_t> &vector,
                             int32_t file)
{
    vector.resize(length * sizeof(int16_t));
    read(position, vector.data(), length * sizeof(int16_t), file);
}

void Reader::readArrayChar(__off_t position,
                           int64_t length,
                           char *array,
                           int32_t file)
{
    read(position, array, length, file);
}

} // namespace lib
} // namespace ntof
