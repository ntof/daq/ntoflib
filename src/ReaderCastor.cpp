/*
 * cpp
 *
 *  Created on: Jan 8, 2015
 *      Author: agiraud
 */

#include "ReaderCastor.h"

#include <iostream>

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

#include <XrdPosix/XrdPosixXrootd.hh>
XrdPosixXrootd posixsingleton; //!< Needed by the xrootdlib (no idea why
                               //!< precisely?)

namespace ntof {
namespace lib {

/*
 * \brief Constructor of the class
 */
ReaderCastor::ReaderCastor() :
    stageHost_("castorpublic"), serviceClass_("default")
{}

/*
 * \brief Destructor of the class
 */
ReaderCastor::~ReaderCastor() {}

FILE_INFO ReaderCastor::openFile(FILE_INFO fileInfo,
                                 Notification::Listener *notif)
{
    rawOpenFile(fileInfo);
    readFile(fileInfo, notif);

    return fileInfo;
}

void ReaderCastor::rawOpenFile(FILE_INFO &fileInfo)
{
    const std::string path = fileInfo.fullpath.compare(0, 8, "xroot://") ?
        ("xroot://" + stageHost_ + ".cern.ch/" + fileInfo.fullpath +
         "?svcClass=" + serviceClass_) :
        fileInfo.fullpath;
    // Open the file
    fileInfo.file = XrdPosixXrootd::Open(const_cast<char *>(path.c_str()),
                                         O_RDONLY);
    if (fileInfo.file < 0)
    {
        std::ostringstream message;
        message << "Error to open the file " << fileInfo.fullpath
                << " on CASTOR [" << errno << "]";
        throw NtofLibException(message.str().c_str(), __FILE__, __LINE__);
    }

    // Get the file size
    struct stat statFile;
    int result = XrdPosixXrootd::Stat(path.c_str(), &statFile);
    if (result != 0)
    {
        throw NtofLibException(
            "Error to get the stats of the file " + fileInfo.fullpath, __FILE__,
            __LINE__);
    }

    fileInfo.size = statFile.st_size;
    fileInfo.isOnCastor = true;
}

/*
 * \brief Close a file opened on CASTOR
 * \param file: File to close
 */
void ReaderCastor::closeFile(int32_t file)
{
    if (file > 0)
    {
        int result = XrdPosixXrootd::Close(file);
        if (result == -1)
        {
            throw NtofLibException("Error to close the file.", __FILE__,
                                   __LINE__);
        }
    }
}

void ReaderCastor::readFile(FILE_INFO fileInfo, Notification::Listener *notif)
{
    int32_t eventNumber = -1;
    char memblock[5];
    memblock[4] = '\0';
    int32_t revisionNumber;
    int32_t reserved;
    int32_t length; // Number of word following in words (1 word = 32 bits)
    __off_t size;

    if (fileInfo.file != -1)
    {
        size = fileInfo.size;
        setPosition(0, fileInfo.file);
        __off_t loc = 0;

        while (loc < size)
        {
            Notification::notify(notif, Notification::HEADER_PARSE, loc, size);

            __off_t startHeader = loc;

            loc += XrdPosixXrootd::Read(fileInfo.file, &(memblock),
                                        sizeof(char) * 4);
            if (HeaderManager::getInstance()->isHeaderTitle(memblock))
            {
                loc += XrdPosixXrootd::Read(fileInfo.file, &revisionNumber,
                                            sizeof(revisionNumber));
                loc += XrdPosixXrootd::Read(fileInfo.file, &reserved,
                                            sizeof(reserved));
                loc += XrdPosixXrootd::Read(fileInfo.file, &length,
                                            sizeof(length));

                char detectorType[5];
                detectorType[0] = '\0';
                int32_t detectorId = 0;
                if (strcmp(memblock, "ACQC") == 0)
                {
                    XrdPosixXrootd::Read(fileInfo.file, &(detectorType),
                                         sizeof(char) * 4);
                    detectorType[4] = '\0';
                    XrdPosixXrootd::Read(fileInfo.file, &detectorId,
                                         sizeof(detectorId));
                }
                else if (strcmp(memblock, "EVEH") == 0)
                {
                    setPosition(loc + sizeof(int32_t), fileInfo.file);
                    XrdPosixXrootd::Read(fileInfo.file, &eventNumber,
                                         sizeof(eventNumber));
                }
                std::string str(detectorType);
                Header head = {
                    memblock,    revisionNumber, str, detectorId, eventNumber,
                    startHeader, fileInfo.file,  -1,  true};

                HeaderManager::getInstance()->addHeader(head);
                loc = setPosition(loc + (length * 4), fileInfo.file);

                // Security removed to avoid problems with ADDH corruption
                /*} else{
                    //Bug fix to ignore the empty lines between two headers
                    int32_t val = std::atoi(memblock);
                    if (val != 0){
                        std::ostringstream oss;
                        oss << "Error during the reading of the data at [" <<
                   loc << "] : " << memblock; throw NtofLibException(oss.str(),
                   __FILE__, __LINE__); break;
                    }*/
            }
        }
        Notification::notify(notif, Notification::HEADER_PARSE, size, size);
    }
    else
    {
        throw NtofLibException("Error : The file is not opened", __FILE__,
                               __LINE__);
    }
}

/*
 * \brief Allow to change the position of the cursor.
 * \param pos: Position you want to go
 * \param file: Handler to the file
 * \return Return the offset of the actual position according the SEEK_SET.
 */
__off_t ReaderCastor::setPosition(__off_t pos, int32_t file)
{
    // rfio_lseek(file, pos, SEEK_SET); // BUG FIXED added by AG [01/10/2015]
    __off_t offset = XrdPosixXrootd::Lseek(file, pos, SEEK_SET);
    if (offset == -1)
    {
        std::ostringstream oss;
        oss << "Error in file : no position " << pos << " in the file";
        throw NtofLibException(oss.str(), __FILE__, __LINE__);
    }
    return offset;
}

void ReaderCastor::read(__off_t position, void *buffer, size_t size, int32_t file)
{
    setPosition(position, file);
    XrdPosixXrootd::Read(file, static_cast<char *>(buffer), size);
}

/*
 * \brief Get the service class
 * \return Return the service class
 */
const std::string &ReaderCastor::getServiceClass() const
{
    return serviceClass_;
}

/*
 * \brief Set the service class
 * \param serviceClass: The service class
 */
void ReaderCastor::setServiceClass(const std::string &serviceClass)
{
    serviceClass_ = serviceClass;
}

/*
 * \brief Get the hostname of the CASTOR machine
 * \return Return the hostname of the CASTOR machine
 */
const std::string &ReaderCastor::getStageHost() const
{
    return stageHost_;
}

/*
 * \brief Set the hostname of the CASTOR machine
 * \param stageHost: The hostname of the CASTOR machine
 */
void ReaderCastor::setStageHost(const std::string &stageHost)
{
    stageHost_ = stageHost;
}
} /* namespace lib */
} /* namespace ntof */
