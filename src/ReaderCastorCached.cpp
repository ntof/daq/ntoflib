/*
** Copyright (C) 2018 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-10-15T08:37:19+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "ReaderCastorCached.h"

#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "Notification.h"

#include <XrdPosix/XrdPosixXrootd.hh>

#define RCC_BUFFER_SIZE (4 * 1024 * 1024)

namespace ntof {
namespace lib {

ReaderCastorCached::ReaderCastorCached(const std::shared_ptr<FileCache> &cache) :
    m_cache(cache)
{}

ReaderCastorCached::~ReaderCastorCached() {}

FILE_INFO ReaderCastorCached::openFile(FILE_INFO fileInfo,
                                       Notification::Listener *notif)
{
    if (!m_cache || m_cache->baseDir().empty() ||
        fileInfo.hasFlag(FILE_INFO::NO_CACHE))
    {
        std::cerr << "Not using cache for " << fileInfo.filename << std::endl;
        return m_castor.openFile(fileInfo, notif);
    }
    else
    {
        int err;
        FileCache::SharedLock lock = m_cache->readLock(fileInfo.filename, err);
        if (!lock || (err != 0))
        {
            readFile(fileInfo, notif);
            lock = m_cache->readLock(fileInfo.filename, err);
            if (!lock || (err != 0)) /* should not happen since we just created
                                        the file */
                throw NtofLibException("Failed to lock the file", __FILE__,
                                       __LINE__);
        }
        fileInfo.fullpath = lock->fullpath().string();
        fileInfo = m_local.openFile(fileInfo, notif);
        m_files[fileInfo.file] = lock;
        fileInfo.setFlag(FILE_INFO::ON_CASTOR, true);
        return fileInfo;
    }
}

void ReaderCastorCached::readFile(FILE_INFO fileInfo,
                                  Notification::Listener *notif)
{
    int err;
    Notification::notify(notif, Notification::FILE_FETCH, 0, 0);

    m_castor.rawOpenFile(fileInfo); /* open file on castor */
    FileCache::SharedLock lock = m_cache->create(fileInfo.filename + ".dl",
                                                 fileInfo.size, err);
    switch (err)
    {
    case 0: break;
    case -EAGAIN:
        m_castor.closeFile(fileInfo.file);
        throw NtofLibException("File is already being fetched", __FILE__,
                               __LINE__, err);
    case -EACCES:
        m_castor.closeFile(fileInfo.file);
        throw NtofLibException("Failed to open cache file", __FILE__, __LINE__,
                               err);
    default:
        m_castor.closeFile(fileInfo.file);
        std::cerr << "Unknown FileCache error " << err << std::endl;
        throw NtofLibException("Unknown error", __FILE__, __LINE__, err);
    }

    int32_t wrFile = ::open(lock->fullpath().c_str(), O_WRONLY | O_TRUNC);
    try
    {
        if (wrFile < 0)
            throw NtofLibException("Failed to open file", __FILE__, __LINE__);

        std::vector<int8_t> buffer(RCC_BUFFER_SIZE);
        for (ssize_t sz = 0; sz < fileInfo.size;)
        {
            /* reduce sz to KB, may overflow otherwise */
            Notification::notify(notif, Notification::FILE_FETCH, sz >> 10,
                                 fileInfo.size >> 10);
            ssize_t toRead = fileInfo.size - sz;
            if (toRead > RCC_BUFFER_SIZE)
                toRead = RCC_BUFFER_SIZE;
            toRead = XrdPosixXrootd::Read(fileInfo.file, buffer.data(), toRead);
            if (toRead <= 0)
                throw NtofLibException("Failed to read file on castor",
                                       __FILE__, __LINE__, err);
            ::write(wrFile, buffer.data(), toRead);

            sz += toRead;
        }
        Notification::notify(notif, Notification::FILE_FETCH, fileInfo.size,
                             fileInfo.size);
    }
    catch (NtofLibException &e)
    {
        if (wrFile >= 0)
            ::close(wrFile);
        m_castor.closeFile(fileInfo.file);
        throw;
    }
    m_castor.closeFile(fileInfo.file);
    ::close(wrFile);

    if (::rename(lock->fullpath().c_str(),
                 (m_cache->baseDir() / fileInfo.filename).c_str()) < 0)
    {
        throw NtofLibException("Failed to rename downloaded file", __FILE__,
                               __LINE__);
    }
}

__off_t ReaderCastorCached::setPosition(__off_t pos, int32_t file)
{
    return readerFor(file).setPosition(pos, file);
}

void ReaderCastorCached::read(__off_t position,
                              void *buffer,
                              size_t size,
                              int32_t file)
{
    readerFor(file).read(position, buffer, size, file);
}

Reader &ReaderCastorCached::readerFor(int32_t file)
{
    return (m_files.find(file) != m_files.end()) ?
        static_cast<Reader &>(m_local) :
        static_cast<Reader &>(m_castor);
}

void ReaderCastorCached::closeFile(int32_t file)
{
    if (m_files.erase(file))
        m_local.closeFile(file);
    else
        m_castor.closeFile(file);
}

} // namespace lib
} // namespace ntof
