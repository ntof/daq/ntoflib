/*
 * Reader.cpp
 *
 *  Created on: Jan 8, 2015
 *      Author: agiraud
 */

#include <iostream>
#include <sstream> // std::ostringstream
#include <vector>

#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h> // read open close
// #ifndef CASTOR
//     #include <unistd.h>
//     #include <sys/stat.h>
// #endif
#include "ReaderLocal.h"

namespace ntof {
namespace lib {

/*
 * \brief Constructor of the class
 */
ReaderLocal::ReaderLocal() {}

/*
 * \brief Destructor of the class
 */
ReaderLocal::~ReaderLocal() {}

FILE_INFO ReaderLocal::openFile(FILE_INFO fileInfo,
                                Notification::Listener *notif)
{
    // Open the file
    fileInfo.file = open((char *) (fileInfo.fullpath).c_str(), O_RDONLY);
    if (fileInfo.file < 0)
    {
        throw NtofLibException(
            "Error to open the file " + fileInfo.fullpath + " on LOCAL",
            __FILE__, __LINE__);
    }

    // Get the file size
    struct stat statFile;
    int result = stat((fileInfo.fullpath).c_str(), &statFile);
    if (result != 0)
    {
        throw NtofLibException(
            "Error to get the stats of the file " + fileInfo.fullpath, __FILE__,
            __LINE__);
    }

    fileInfo.size = statFile.st_size;
    fileInfo.setFlag(FILE_INFO::ON_CASTOR, false);

    readFile(fileInfo, notif);

    return fileInfo;
}

/*
 * \brief Close a file opened on CASTOR
 * \param file: File to close
 */
void ReaderLocal::closeFile(int32_t file)
{
    if (file >= 0)
    {
        int result = close(file);
        if (result == -1)
        {
            throw NtofLibException("Error to close the file.", __FILE__,
                                   __LINE__);
        }
    }
}

/*
 * \brief Allow to change the position of the cursor.
 * \param pos: Position you want to go
 * \param file: Handler to the file
 * \return Return the offset of the actual position according the SEEK_SET.
 */
__off_t ReaderLocal::setPosition(__off_t pos, int32_t file)
{
    // rfio_lseek(file, pos, SEEK_SET); // BUG FIXED added by AG [01/10/2015]
    __off_t offset = lseek(file, pos, SEEK_SET);
    if (offset == (off_t) -1)
    {
        std::ostringstream oss;
        oss << "Error in file : no position " << (__off_t) pos
            << " in the file";
        throw NtofLibException(oss.str(), __FILE__, __LINE__);
    }
    return offset;
}

void ReaderLocal::readFile(FILE_INFO fileInfo, Notification::Listener *notif)
{
    int32_t eventNumber = -1;
    char memblock[5];
    memblock[4] = '\0';
    int32_t revisionNumber;
    int32_t reserved;
    int32_t length; // Number of word following in words (1 word = 32 bits)
    __off_t size;

    if (fileInfo.file != -1)
    {
        size = fileInfo.size;
        setPosition(0, fileInfo.file);
        __off_t loc = 0;

        while (loc < size)
        {
            Notification::notify(notif, Notification::HEADER_PARSE, loc, size);

            __off_t startHeader = loc;

            loc += ::read(fileInfo.file, &(memblock), sizeof(char) * 4);
            if (HeaderManager::getInstance()->isHeaderTitle(memblock))
            {
                loc += ::read(fileInfo.file, &revisionNumber,
                              sizeof(revisionNumber));
                loc += ::read(fileInfo.file, &reserved, sizeof(reserved));
                loc += ::read(fileInfo.file, &length, sizeof(length));

                char detectorType[5];
                detectorType[0] = '\0';
                int32_t detectorId = 0;
                if (strcmp(memblock, "ACQC") == 0)
                {
                    ::read(fileInfo.file, &(detectorType), sizeof(char) * 4);
                    detectorType[4] = '\0';
                    ::read(fileInfo.file, &detectorId, sizeof(detectorId));

                    if (!fileInfo.hasFlag(FILE_INFO::IS_FINISHED))
                    {
                        eventNumber = fileInfo.segmentNumber;
                    }
                }
                else if (strcmp(memblock, "EVEH") == 0)
                {
                    setPosition(loc + sizeof(int32_t), fileInfo.file);
                    ::read(fileInfo.file, &eventNumber, sizeof(eventNumber));
                }
                std::string str(detectorType);
                Header head = {
                    memblock,    revisionNumber, str, detectorId, eventNumber,
                    startHeader, fileInfo.file,  -1,  false};

                if (strcmp(memblock, "SKIP") != 0)
                {
                    HeaderManager::getInstance()->addHeader(head);
                }
                loc = setPosition(loc + (length * 4), fileInfo.file);

                // Security removed to avoid problems with local files too long
                /*} else{
                    //Bug fix to ignore the empty lines between two headers
                    int32_t val = std::atoi(memblock);
                    if (val != 0){
                        std::ostringstream oss;
                        oss << "Error during the reading of the data at [" <<
                   loc << "] : " << memblock; throw NtofLibException(oss.str(),
                   __FILE__, __LINE__); break;
                    }*/
            }
        }
        Notification::notify(notif, Notification::HEADER_PARSE, size, size);
    }
    else
    {
        throw NtofLibException("Error : The file is not opened", __FILE__,
                               __LINE__);
    }
}

void ReaderLocal::read(__off_t position, void *buffer, size_t size, int32_t file)
{
    setPosition(position, file);
    ::read(file, static_cast<char *>(buffer), size);
}

} /* namespace lib */
} /* namespace ntof */
