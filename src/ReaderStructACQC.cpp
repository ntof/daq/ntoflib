/*
 * ReaderStructACQC.cpp
 *
 *  Created on: Jan 12, 2015
 *      Author: agiraud
 */

#include "ReaderStructACQC.h"

#include "HeaderManager.h"
#include "NtofLib.h"
#include "Reader.h"

namespace ntof {
namespace lib {

/*
 * \brief Constructor
 * \param header Details of the header
 * \param ntoflib give access to the reader
 */
ReaderStructACQC::ReaderStructACQC(const Header &header, NtofLib &ntoflib) :
    ntoflib_(ntoflib),
    startPosition_(header.pos),
    revisionNumber_(-1),
    ACQCLength_(-1),
    pulseId_(-1),
    pulsePosition_(-1),
    pulseLength_(-1),
    data_(0),
    isZeroSuppressionCorrect_(false),
    isZeroSuppressionInit_(false),
    header_(header)
{
    Notification::notify(ntoflib_.notificationListener(),
                         Notification::ACQC_INIT);
    getTitle();
    getRevisionNumber();

    initPulses();
    pulsePosition_ = -1;
    pulseLength_ = -1;

    // Zero suppression fix (data collapsed)
    // isZeroSuppressionCorrect_ = false;
    isZeroSuppressionCorrect_ = zeroSupressionCheck();
    if (isZeroSuppressionCorrect_)
    {
        correctZeroSuppressionData();
        isZeroSuppressionInit_ = true;
    }
    pulseId_ = -1;
}

/*
 * \brief Destructor
 */
ReaderStructACQC::~ReaderStructACQC() {}

bool ReaderStructACQC::zeroSupressionCheck()
{
    // if the run number is in the range of the files that can contains
    // collapsed data
    uint32_t runNumber = ntoflib_.getRCTR()->getRunNumber();
    if (getReserved() == 0 &&
        ((runNumber >= 101353 && runNumber <= 101590) ||
         (runNumber >= 201325 && runNumber <= 202064)))
    {
        // if the parameter zeroSuppressionStart is not null
        // Save the actual MODH position to reset it at the end of the check
        uint32_t channelId = ntoflib_.getMODH()->getIndex();
        try
        {
            if (setChannelConfiguration())
            {
                return true;
            }
        }
        catch (NtofLibException &ex)
        {
            // Need to do nothing because the parameter can't be found
        }
        ntoflib_.getMODH()->setIndex(channelId);
    }
    return false;
}

/*
 * \brief Algorithm to fixed the data collapsed
 */
void ReaderStructACQC::correctZeroSuppressionData()
{
    correctPulseCollection_.clear();
    std::vector<PULSE_DATA_t> pulseCollection;
    // Save the actual MODH position to reset it at the end of the check
    uint32_t channelId = ntoflib_.getMODH()->getIndex();
    try
    {
        if (setChannelConfiguration())
        {
            uint32_t preSamples = ntoflib_.getMODH()->getPreSample();

            // Full the vector of pulse with all the pulses and recalculation of
            // the startIndex
            while (getNextPulse())
            {
                PULSE_DATA_t pulse;
                pulse.data.reserve(getPulseLength());

                for (int64_t i = 0; i < getPulseLength(); ++i)
                {
                    pulse.data.push_back(getData(i));
                }

                pulse.timeStamp = getPulseTimestamp();
                pulse.position = pulsePosition_;
                pulse.startIndex = pulse.timeStamp - preSamples;
                if (pulse.startIndex < 0)
                {
                    pulse.startIndex = 0;
                }

                pulseCollection.push_back(pulse);
            }

            for (std::vector<PULSE_DATA_t>::iterator it =
                     pulseCollection.begin();
                 it != pulseCollection.end(); ++it)
            {
                PULSE_DATA_t pulse = (*it);
                PULSE_DATA_t correctedPulse;
                if (correctPulseCollection_.empty())
                {
                    correctPulseCollection_.push_back(pulse);
                    // correctPulseCollection.push_back(pulse);
                    continue;
                }

                // check overlap with previous pulse
                if (pulse.startIndex <=
                    (correctPulseCollection_.back().startIndex +
                     static_cast<int64_t>(
                         correctPulseCollection_.back().data.size())))
                {
                    correctedPulse.data.reserve(
                        pulse.startIndex -
                        correctPulseCollection_.back().startIndex +
                        pulse.data.size());

                    for (int64_t i = 0;
                         i < (pulse.startIndex -
                              correctPulseCollection_.back().startIndex);
                         ++i)
                    {
                        correctedPulse.data.push_back(
                            correctPulseCollection_.back().data[i]);
                    }

                    for (uint64_t i = 0; i < pulse.data.size(); ++i)
                    {
                        correctedPulse.data.push_back(pulse.data[i]);
                    }

                    // update present pulse parameters with previous's ones
                    correctedPulse.startIndex =
                        correctPulseCollection_.back().startIndex;
                    correctedPulse.timeStamp =
                        correctPulseCollection_.back().timeStamp;
                    correctedPulse.position =
                        correctPulseCollection_.back().position;

                    // previous pulse is deleted
                    correctPulseCollection_.pop_back();
                    correctPulseCollection_.push_back(correctedPulse);
                }
                else
                {
                    correctPulseCollection_.push_back(pulse);
                }
            }

            uint32_t newACQCLength = 3; // Detector type, detector id and ids
            for (std::vector<PULSE_DATA_t>::iterator it =
                     correctPulseCollection_.begin();
                 it != correctPulseCollection_.end(); ++it)
            {
                while ((*it).data.size() % 4 != 0)
                {
                    (*it).data.push_back(0);
                }

                newACQCLength += 4 + ((*it).data.size() / 2);
            }
            ACQCLength_ = newACQCLength;
        }
    }
    catch (NtofLibException &ex)
    {
        // Need to do nothing because the parameter can't be found
    }
    ntoflib_.getMODH()->setIndex(channelId);
}

/*
 * \brief Get the id of the actual pulse
 * \return Return the id of the pulse
 */
int32_t ReaderStructACQC::getPulseId() const
{
    return pulseId_;
}

/*
 * \brief Set the id of the pulse
 * \param pulseId: id to set
 * \return Return true if the id has been set or false in the other case
 */
bool ReaderStructACQC::setPulseId(int32_t pulseId)
{
    pulseId_ = pulseId;
    if (!initPulseInfo())
    {
        pulseId_ = -1;
        pulsePosition_ = -1;
        pulseLength_ = -1;
        return false;
    }
    return true;
}

/*
 * \brief Allow to initialized the map of the pulse parameters
 */
void ReaderStructACQC::initPulses()
{
    int32_t id = 0;

    while (readNextPulse())
    {
        PulseInfo pulseInfo = {pulsePosition_, pulseLength_};
        pulses_.insert(std::pair<int32_t, PulseInfo>(id, pulseInfo));
        ++id;
    }
}

/*
 * \brief Start to read the pulses
 * \return Return false in case of problem
 */
bool ReaderStructACQC::initPulseInfo()
{
    if (isZeroSuppressionCorrect_ && isZeroSuppressionInit_)
    {
        if (pulseId_ == -1)
        {
            throw NtofLibException("No pulse selected", __FILE__, __LINE__);
        }
        if (pulseId_ >= (int32_t) correctPulseCollection_.size())
        {
            //                    std::cerr << "Data out of range ZS [" <<
            //                    pulseId_ << "/" <<
            //                    (int32_t)correctPulseCollection_.size() << "]"
            //                    << std::endl;
            return false;
        }
        pulseLength_ = correctPulseCollection_[pulseId_].data.size();
        pulsePosition_ = correctPulseCollection_[pulseId_].position;
        readData();
    }
    else
    {
        if (pulseId_ >= (int32_t) pulses_.size())
        {
            //                    std::cerr << "Data out of range [" << pulseId_
            //                    << "/" << (int32_t)pulses_.size() << "]" <<
            //                    std::endl;
            return false;
        }

        pulseLength_ = pulses_[pulseId_].pulseLength;
        pulsePosition_ = pulses_[pulseId_].pulsePosition;
        readData();
    }
    return true;
}

/*
 * \brief Allow to initialized the reading parameters to get data from the map
 * of the pulse \return Return true if the next pulse exist or false if there is
 * no more pulse.
 */
bool ReaderStructACQC::getNextPulse()
{
    ++pulseId_;
    return initPulseInfo();
}

/*
 * \brief Allow to initialized the reading parameters to get data from the file
 * \return Return true if the next pulse has been found or false if there is no
 * more pulse.
 */
bool ReaderStructACQC::readNextPulse()
{
    if (pulsePosition_ == -1)
    {
        pulsePosition_ = startPosition_ + (sizeof(int32_t) * 7);
    }
    else
    {
        switch (getRevisionNumber())
        {
        case 0:
            if (ntoflib_.getRCTR()->getRunNumber() < 100000)
            {
                pulsePosition_ = pulsePosition_ +
                    (pulseLength_ * sizeof(int8_t)) + (sizeof(int32_t) * 2);
            }
            else
            {
                pulsePosition_ = pulsePosition_ +
                    (pulseLength_ * sizeof(int8_t)) + (sizeof(int64_t) * 2);
            }
            break;
        case 1:
        case 2:
            pulsePosition_ = pulsePosition_ + (pulseLength_ * sizeof(int16_t)) +
                (sizeof(int64_t) * 2);
            break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }

    if ((startPosition_ + (getLength() * 4) + 4 * 4) - (pulsePosition_) < 4)
    {
        pulsePosition_ = -1;
        return false;
    }

    switch (getRevisionNumber())
    {
    case 0:
        if (ntoflib_.getRCTR()->getRunNumber() < 100000)
        {
            pulseLength_ = swap(
                ntoflib_.getReader(header_.isOnCastor)
                    ->readInt32(pulsePosition_ + (sizeof(int32_t)),
                                header_.file));
        }
        else
        {
            pulseLength_ = ntoflib_.getReader(header_.isOnCastor)
                               ->readInt64(pulsePosition_ + (sizeof(int64_t)),
                                           header_.file);
        }
        break;
    case 1:
    case 2:
        pulseLength_ = ntoflib_.getReader(header_.isOnCastor)
                           ->readInt64(pulsePosition_ + (sizeof(int64_t)),
                                       header_.file);
        break;
    default:
        throw NtofLibException("Revision number unknown", __FILE__, __LINE__);
        break;
    }

    return true;
}

/*
 * \brief Allow to load all the data in the vector
 */
void ReaderStructACQC::readData()
{
    if (isZeroSuppressionCorrect_ && isZeroSuppressionInit_)
    {
        if (pulseId_ == -1)
        {
            throw NtofLibException("No pulse selected", __FILE__, __LINE__);
        }
        pulse16_ = correctPulseCollection_[pulseId_].data;
    }
    else
    {
        switch (getRevisionNumber())
        {
        case 0: readData8(); break;
        case 1:
        case 2: readData16(); break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
}

/*
 * \brief Allow to load all the data in the vector for revision number 0
 */
void ReaderStructACQC::readData8()
{
    // resize the vector if it can't contain all the data
    int64_t size = pulse8_.capacity();
    if (size < pulseLength_)
    {
        pulse8_.reserve(pulseLength_ * RESERVE_COEFFICIENT);
    }
    pulse8_.clear();

    int64_t pos;
    if (ntoflib_.getRCTR()->getRunNumber() < 100000)
    {
        pos = pulsePosition_ + (sizeof(uint32_t) * 2);
    }
    else
    {
        pos = pulsePosition_ + (sizeof(uint64_t) * 2);
    }
    ntoflib_.getReader(header_.isOnCastor)
        ->readVectorInt8(pos, pulseLength_, pulse8_, header_.file);
}

/*
 * \brief Allow to load all the data in the vector for revision number 1
 */
void ReaderStructACQC::readData16()
{
    // resize the vector if it can't contain all the data
    int64_t size = pulse16_.capacity();
    if (size < pulseLength_)
    {
        pulse16_.reserve(pulseLength_ * RESERVE_COEFFICIENT);
    }
    pulse16_.clear();

    int64_t pos = pulsePosition_ + (sizeof(uint64_t) * 2);
    ntoflib_.getReader(header_.isOnCastor)
        ->readVectorInt16(pos, pulseLength_, pulse16_, header_.file);
}

/*
 * \brief Get the title
 * \return Return the title.
 */
std::string ReaderStructACQC::getTitle()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readString(startPosition_, 4, header_.file);
}

/*
 * \brief Get the revision number
 * \return Return the revision number.
 */
int32_t ReaderStructACQC::getRevisionNumber()
{
    if (revisionNumber_ == -1)
    {
        revisionNumber_ = ntoflib_.getReader(header_.isOnCastor)
                              ->readInt32(startPosition_ + (sizeof(int32_t) * 1),
                                          header_.file);
    }
    return revisionNumber_;
}

/*
 * \brief Get the reserved
 * \return Return the reserved.
 */
uint32_t ReaderStructACQC::getReserved()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + (sizeof(int32_t) * 2), header_.file);
}

/*
 * \brief Get the length
 * \return Return the length
 */
uint32_t ReaderStructACQC::getLength()
{
    if (ACQCLength_ == -1)
    {
        ACQCLength_ = ntoflib_.getReader(header_.isOnCastor)
                          ->readUint32(startPosition_ + (sizeof(int32_t) * 3),
                                       header_.file);
    }
    return ACQCLength_;
}

/*
 * \brief Get the detector type
 * \return Return the detector type
 */
std::string ReaderStructACQC::getDetectorType()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readString(startPosition_ + (sizeof(int32_t) * 4), 4, header_.file);
}

/*
 * \brief Get the detector id
 * \return Return the detector id
 */
uint32_t ReaderStructACQC::getDetectorId()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + (sizeof(int32_t) * 5), header_.file);
}

/*
 * \brief Get the channel id
 * \return Return the channel id
 */
int32_t ReaderStructACQC::getChannel()
{
    return (
        ntoflib_.getReader(header_.isOnCastor)
            ->readInt32(startPosition_ + (sizeof(int32_t) * 6), header_.file) &
        0x000000FF);
}

/*
 * \brief Get the module id
 * \return Return the module id
 */
int32_t ReaderStructACQC::getModule()
{
    return (
        (ntoflib_.getReader(header_.isOnCastor)
             ->readInt32(startPosition_ + (sizeof(int32_t) * 6), header_.file) &
         0x0000FF00) >>
        8);
}

/*
 * \brief Get the chassis id
 * \return Return the chassis id.
 */
int32_t ReaderStructACQC::getChassis()
{
    return (
        (ntoflib_.getReader(header_.isOnCastor)
             ->readInt32(startPosition_ + (sizeof(int32_t) * 6), header_.file) &
         0x00FF0000) >>
        16);
}

/*
 * \brief Get the stream id
 * \return Return the stream id.
 */
int32_t ReaderStructACQC::getStream()
{
    return (
        (ntoflib_.getReader(header_.isOnCastor)
             ->readInt32(startPosition_ + (sizeof(int32_t) * 6), header_.file) &
         0xFF000000) >>
        24);
}

/*
 * \brief Get the timestamp of the pulse
 * \return Return the timestamp of the pulse.
 */
int64_t ReaderStructACQC::getPulseTimestamp()
{
    if (isZeroSuppressionCorrect_ && isZeroSuppressionInit_)
    {
        if (pulseId_ == -1)
        {
            throw NtofLibException("No pulse selected", __FILE__, __LINE__);
        }

        return correctPulseCollection_[pulseId_].startIndex;
    }
    else
    {
        if (pulsePosition_ == -1)
        {
            throw NtofLibException("No pulse selected", __FILE__, __LINE__);
        }

        switch (getRevisionNumber())
        {
        case 0:
            if (ntoflib_.getRCTR()->getRunNumber() < 100000)
            {
                return (uint64_t) swap(
                    ntoflib_.getReader(header_.isOnCastor)
                        ->readInt32(pulsePosition_, header_.file));
            }
            else
            {
                return ntoflib_.getReader(header_.isOnCastor)
                    ->readInt64(pulsePosition_, header_.file);
            }
            break;
        case 1:
        case 2:
            return ntoflib_.getReader(header_.isOnCastor)
                ->readInt64(pulsePosition_, header_.file);
            break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
}

/*
 * \brief Get the length of the pulse
 * \return Return the length of the pulse.
 */
int64_t ReaderStructACQC::getPulseLength()
{
    if (isZeroSuppressionCorrect_ && isZeroSuppressionInit_)
    {
        if (pulseId_ == -1)
        {
            throw NtofLibException("No pulse selected", __FILE__, __LINE__);
        }

        return correctPulseCollection_[pulseId_].data.size();
    }
    else
    {
        if (pulsePosition_ == -1)
        {
            throw NtofLibException("No pulse selected", __FILE__, __LINE__);
        }
        return pulseLength_;
    }
}

/*
 * \brief Get the data
 * \param id Position of the data
 * \return Return the data.
 */
int16_t &ReaderStructACQC::getData(int64_t id)
{
    if (id >= getPulseLength() || id < 0)
    {
        throw NtofLibException("Data out of the range", __FILE__, __LINE__);
    }

    if (isZeroSuppressionCorrect_ && isZeroSuppressionInit_)
    {
        return correctPulseCollection_[pulseId_].data[id];
    }
    else
    {
        switch (getRevisionNumber())
        {
        case 0:
            if (ntoflib_.getRCTR()->getRunNumber() < 100000)
            {
                data_ = (int16_t) (unsigned char) pulse8_[id];
            }
            else
            {
                data_ = pulse8_[id];
            }
            break;
        case 1:
        case 2: data_ = pulse16_[id]; break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
        return data_;
    }
}

/*
 * \brief Set the channel configuration
 * \return Return true if the configuration has been set correctly
 */
bool ReaderStructACQC::setChannelConfiguration()
{
    // return ntoflib_.getMODH()->setIndex(getChannel(), getModule(),
    // getStream(), getChassis());
    return ntoflib_.getMODH()->setIndex(getDetectorType(), getDetectorId());
}

/*
 * \brief Swap to big endian to little endian and vise versa
 * \param x data to convert
 * \return Return the converted data.
 */
uint32_t ReaderStructACQC::swap(uint32_t x)
{
    return ((x >> 24) | ((x << 8) & 0x00FF0000) | ((x >> 8) & 0x0000FF00) |
            (x << 24));
}

/*
 * \brief Operator [] allows to access to a data index
 * \param id Position of the data
 * \return Return the data.
 */
int16_t &ReaderStructACQC::operator[](int64_t index)
{
    return getData(index);
}

/*
 * \brief Get the position of the ACQC in the file
 * \return Return the position of the ACQC in the file
 */
uint64_t ReaderStructACQC::getCurrentSeek()
{
    return startPosition_;
}

/*
 * \brief Allow to know the size of the data in the file
 * \return Return the size of one data.
 */
int32_t ReaderStructACQC::getDataSize()
{
    switch (getRevisionNumber())
    {
    case 0: return 1; break;
    case 1:
    case 2: return 2; break;
    default:
        throw NtofLibException("Revision number unknown", __FILE__, __LINE__);
        break;
    }
}

/*
 * \brief Allow to know the amount of data for an event
 * \return Return the size the event.
 */
int64_t ReaderStructACQC::getPulseSize()
{
    int64_t res = 0;
    if (isZeroSuppressionCorrect_)
    {
        for (std::vector<PULSE_DATA_t>::iterator it =
                 correctPulseCollection_.begin();
             it != correctPulseCollection_.end(); ++it)
        {
            res += (*it).data.size();
        }
    }
    else
    {
        for (std::map<int32_t, PulseInfo>::iterator it = pulses_.begin();
             it != pulses_.end(); ++it)
        {
            res += (*it).second.pulseLength;
        }
    }
    return res;
}

/*
 * \brief Get the details of the actual header
 * \return Return the details of the header
 */
const Header &ReaderStructACQC::getHeader() const
{
    return header_;
}

/*
 * \brief Set the details of the header
 * \param header: new header used to replace the old one
 */
void ReaderStructACQC::setHeader(Header &header)
{
    header_ = header;
}
} /* namespace lib */
} /* namespace ntof */
