/*
 * ReaderStructADDH.cpp
 *
 *  Created on: Feb 8, 2016
 *      Author: agiraud
 */

#include "ReaderStructADDH.h"

#include "HeaderManager.h"
#include "NtofLib.h"
#include "NtofLibException.h"
#include "Reader.h"

namespace ntof {
namespace lib {

#define WORD_SIZE 4

std::string typeToStr(ADDHDATATYPE type)
{
    switch (type)
    {
    case TypeString: return "String";
    case TypeInt: return "Int";
    case TypeLong: return "Long";
    case TypeFloat: return "Float";
    case TypeDouble: return "Double";
    default: return "Unknown";
    }
}

template<>
std::string ReaderStructADDH::getData<std::string>()
{
    return getDataAsString();
}

static std::size_t padding(std::size_t length)
{
    const std::size_t padding = WORD_SIZE - (length % WORD_SIZE);
    return (padding == WORD_SIZE) ? 0 : padding;
}

/*
 * \brief Constructor
 * \param header Details of the header
 * \param ntoflib give access to the reader
 */
ReaderStructADDH::ReaderStructADDH(const Header &header, NtofLib &ntoflib) :
    ntoflib_(ntoflib),
    startPosition_(header.pos),
    ADDHLength_(-1),
    revisionNumber_(-1),
    header_(header),
    actualFieldId_(-1)
{
    initADDH();
}

/*
 * \brief Destructor
 */
ReaderStructADDH::~ReaderStructADDH() {}

/*
 * \brief Get the title
 * \return Return the title.
 */
std::string ReaderStructADDH::getTitle()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readString(startPosition_, 4, header_.file);
}

/*
 * \brief Get the revision number
 * \return Return the revision number.
 */
int32_t ReaderStructADDH::getRevisionNumber()
{
    if (revisionNumber_ == -1)
    {
        revisionNumber_ = ntoflib_.getReader(header_.isOnCastor)
                              ->readUint32(
                                  startPosition_ + (sizeof(int32_t) * 1),
                                  header_.file);
    }
    return revisionNumber_;
}

/*
 * \brief Get the reserved
 * \return Return the reserved.
 */
uint32_t ReaderStructADDH::getReserved()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + (sizeof(int32_t) * 2), header_.file);
}

/*
 * \brief Get the length
 * \return Return the length
 */
uint32_t ReaderStructADDH::getLength()
{
    if (ADDHLength_ == -1)
    {
        ADDHLength_ = ntoflib_.getReader(header_.isOnCastor)
                          ->readUint32(startPosition_ + (sizeof(int32_t) * 3),
                                       header_.file);
    }
    return ADDHLength_;
}

/*
 * \brief index all the ADDH fields
 */
void ReaderStructADDH::initADDH()
{
    int64_t position = startPosition_ + (sizeof(int32_t) * 4);
    int64_t length = startPosition_ + (getLength() * sizeof(int32_t));

    while (position < length)
    {
        Notification::notify(ntoflib_.notificationListener(),
                             Notification::ADDH_INIT, position - startPosition_,
                             length - startPosition_);
        // Add the position to the container
        addhFieldPositions_.push_back(position);

        // Calculate the position of the next field
        int32_t type = ntoflib_.getReader(header_.isOnCastor)
                           ->readUint32((position + SIZE_OF_TITLE),
                                        header_.file);
        int32_t repeat = ntoflib_.getReader(header_.isOnCastor)
                             ->readUint32(
                                 (position + SIZE_OF_TITLE + SIZE_OF_TYPE),
                                 header_.file);
        int64_t dataSize = repeat * getSizeOfData(type);
        dataSize += padding(dataSize);

        // Next position
        position += SIZE_OF_TITLE + SIZE_OF_TYPE + SIZE_OF_REPEAT + dataSize;
    }
    Notification::notify(ntoflib_.notificationListener(),
                         Notification::ADDH_INIT);
}

/*
 * \brief Jump to the next ADDH field
 * \return Return true if everything is ok and false if an error occurred
 */
bool ReaderStructADDH::getNextField()
{
    ++actualFieldId_;
    if (actualFieldId_ >= (int32_t) addhFieldPositions_.size())
    {
        actualFieldId_ = -1;
        return false;
    }
    return true;
}

/*
 * \brief Jump to the previous ADDH field
 * \return Return true if everything is ok and false if an error occurred
 */
bool ReaderStructADDH::getPreviousField()
{
    if (actualFieldId_ == -1 || actualFieldId_ == 0)
    {
        actualFieldId_ = -1;
        return false;
    }
    --actualFieldId_;
    return true;
}

/*
 * \brief Jump to the ADDH field with the given name
 * \param name: The name of the ADDH field to find
 * \return Return true if everything is ok and false if an error occurred
 */
bool ReaderStructADDH::getField(const std::string &name)
{
    int32_t i = 0;
    for (std::vector<int64_t>::iterator it = addhFieldPositions_.begin();
         it != addhFieldPositions_.end(); ++it)
    {
        const std::string name_tmp = ntoflib_.getReader(header_.isOnCastor)
                                         ->readString((*it), 64, header_.file);
        if (name == name_tmp)
        {
            actualFieldId_ = i;
            return true;
        }
        ++i;
    }
    return false;
}

/*
 * \brief Get the name of the ADDH field
 * \return The name of the ADDH field
 */
std::string ReaderStructADDH::getName()
{
    if (actualFieldId_ == -1)
    {
        throw NtofLibException("The id of the field is undefined", __FILE__,
                               __LINE__);
    }
    int64_t position = addhFieldPositions_[actualFieldId_];
    return ntoflib_.getReader(header_.isOnCastor)
        ->readString(position, 64, header_.file);
}

/*
 * \brief Get the type of the ADDH field
 * \return Return the type of the ADDH field as an enum
 */
ADDHDATATYPE ReaderStructADDH::getType()
{
    if (actualFieldId_ == -1)
    {
        throw NtofLibException("The id of the field is undefined", __FILE__,
                               __LINE__);
    }
    int64_t position = addhFieldPositions_[actualFieldId_] + SIZE_OF_TITLE;
    return static_cast<ADDHDATATYPE>(ntoflib_.getReader(header_.isOnCastor)
                                         ->readUint32(position, header_.file));
}

/*
 * \brief Get the length of the array
 * \return Return the value of the repetition factor
 */
uint32_t ReaderStructADDH::getRepetitionFactor()
{
    if (actualFieldId_ == -1)
    {
        throw NtofLibException("The id of the field is undefined", __FILE__,
                               __LINE__);
    }

    int64_t position = addhFieldPositions_[actualFieldId_] + SIZE_OF_TITLE +
        SIZE_OF_TYPE;
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(position, header_.file);
}

/*
 * \brief Get the value of the field as a string
 * \return Return the value of a string
 */
std::string ReaderStructADDH::getDataAsString()
{
    if (actualFieldId_ == -1)
    {
        throw NtofLibException("The id of the field is undefined", __FILE__,
                               __LINE__);
    }

    if (getType() != TypeString)
    {
        throw NtofLibException("The data type is not a String", __FILE__,
                               __LINE__);
    }

    int64_t position = addhFieldPositions_[actualFieldId_] + SIZE_OF_TITLE +
        SIZE_OF_TYPE + SIZE_OF_REPEAT;
    uint32_t repeat = getRepetitionFactor();
    return ntoflib_.getReader(header_.isOnCastor)
        ->readString(position, repeat, header_.file);
}

/*
 * \brief Get the value of the field as an int32_t
 * \return Return the value of an int32_t
 */
int32_t ReaderStructADDH::getDataAsInt32()
{
    if (actualFieldId_ == -1)
    {
        throw NtofLibException("The id of the field is undefined", __FILE__,
                               __LINE__);
    }

    if (getType() != TypeInt)
    {
        throw NtofLibException("The data type is not an integer", __FILE__,
                               __LINE__);
    }

    int64_t position = addhFieldPositions_[actualFieldId_] + SIZE_OF_TITLE +
        SIZE_OF_TYPE + SIZE_OF_REPEAT;
    uint32_t repeat = getRepetitionFactor();

    if (repeat > 1)
    {
        throw NtofLibException("The array are not supported", __FILE__,
                               __LINE__);
    }

    return ntoflib_.getReader(header_.isOnCastor)
        ->readInt32(position, header_.file);
}

/*
 * \brief Get the value of the field as an int64_t
 * \return Return the value of an int64_t
 */
int64_t ReaderStructADDH::getDataAsInt64()
{
    if (actualFieldId_ == -1)
    {
        throw NtofLibException("The id of the field is undefined", __FILE__,
                               __LINE__);
    }

    if (getType() != TypeLong)
    {
        throw NtofLibException("The data type is not a long", __FILE__,
                               __LINE__);
    }

    int64_t position = addhFieldPositions_[actualFieldId_] + SIZE_OF_TITLE +
        SIZE_OF_TYPE + SIZE_OF_REPEAT;
    uint32_t repeat = getRepetitionFactor();

    if (repeat > 1)
    {
        throw NtofLibException("The array are not supported", __FILE__,
                               __LINE__);
    }

    return ntoflib_.getReader(header_.isOnCastor)
        ->readInt64(position, header_.file);
}

/*
 * \brief Get the value of the field as a float
 * \return Return the value of a float
 */
float ReaderStructADDH::getDataAsFloat()
{
    if (actualFieldId_ == -1)
    {
        throw NtofLibException("The id of the field is undefined", __FILE__,
                               __LINE__);
    }

    if (getType() != TypeFloat)
    {
        throw NtofLibException("The data type is not a float", __FILE__,
                               __LINE__);
    }

    int64_t position = addhFieldPositions_[actualFieldId_] + SIZE_OF_TITLE +
        SIZE_OF_TYPE + SIZE_OF_REPEAT;
    uint32_t repeat = getRepetitionFactor();

    if (repeat > 1)
    {
        throw NtofLibException("The array are not supported", __FILE__,
                               __LINE__);
    }

    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(position, header_.file);
}

/*
 * \brief Get the value of the field as a double
 * \return Return the value of a double
 */
double ReaderStructADDH::getDataAsDouble()
{
    if (actualFieldId_ == -1)
    {
        throw NtofLibException("The id of the field is undefined", __FILE__,
                               __LINE__);
    }

    if (getType() != TypeDouble)
    {
        throw NtofLibException("The data type is not a double", __FILE__,
                               __LINE__);
    }

    int64_t position = addhFieldPositions_[actualFieldId_] + SIZE_OF_TITLE +
        SIZE_OF_TYPE + SIZE_OF_REPEAT;
    uint32_t repeat = getRepetitionFactor();

    if (repeat > 1)
    {
        throw NtofLibException("The array are not supported", __FILE__,
                               __LINE__);
    }

    return ntoflib_.getReader(header_.isOnCastor)
        ->readDouble(position, header_.file);
}

/*
 * \brief Get the details of the actual header
 * \return Return the details of the header
 */
const Header &ReaderStructADDH::getHeader() const
{
    return header_;
}

/*
 * \brief Set the details of the header
 * \param header: new header used to replace the old one
 */
void ReaderStructADDH::setHeader(Header &header)
{
    header_ = header;
}

/*
 * \brief index all the ADDH fields
 * \param type: type of the data
 * \return Return the size of the data type given
 */
int32_t ReaderStructADDH::getSizeOfData(int32_t type)
{
    int32_t res = 0;
    switch (type)
    {
    case TypeString: res = sizeof(char); break;
    case TypeInt: res = sizeof(int32_t); break;
    case TypeLong: res = sizeof(int64_t); break;
    case TypeFloat: res = sizeof(float); break;
    case TypeDouble: res = sizeof(double); break;
    default: break;
    }
    return res;
}
} // namespace lib
} // namespace ntof
