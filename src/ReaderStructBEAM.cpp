/*
 * ReaderStructBEAM.cpp
 *
 *  Created on: Jan 12, 2015
 *      Author: agiraud
 */

#include "ReaderStructBEAM.h"

#include "HeaderManager.h"
#include "NtofLib.h"
#include "Reader.h"

namespace ntof {
namespace lib {

/*
 * \brief Constructor
 * \param header: Details of the header
 * \param ntoflib: give access to the reader
 */
ReaderStructBEAM::ReaderStructBEAM(const Header &header, NtofLib &ntoflib) :
    ntoflib_(ntoflib),
    startPosition_(header.pos),
    revisionNumber_(-1),
    beamType_(0),
    header_(header)
{}

/*
 * \brief Destructor
 */
ReaderStructBEAM::~ReaderStructBEAM() {}

/*
 * \brief Get the title
 * \return Return the title.
 */
std::string ReaderStructBEAM::getTitle()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readString(startPosition_, 4, header_.file);
}

/*
 * \brief Get the revision number
 * \return Return the revision number.
 */
uint32_t ReaderStructBEAM::getRevisionNumber()
{
    if (revisionNumber_ == -1)
    {
        revisionNumber_ = ntoflib_.getReader(header_.isOnCastor)
                              ->readUint32(
                                  startPosition_ + (sizeof(int32_t) * 1),
                                  header_.file);
    }
    return revisionNumber_;
}

/*
 * \brief Get the reserved
 * \return Return the reserved.
 */
uint32_t ReaderStructBEAM::getReserved()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + sizeof(int32_t) * 2, header_.file);
}

/*
 * \brief Get the length
 * \return Return the length.
 */
uint32_t ReaderStructBEAM::getLength()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + sizeof(int32_t) * 3, header_.file);
}

/*
 * \brief Get the timestamp
 * \return Return the timestamp.
 */
float ReaderStructBEAM::getTimestamp()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 4, header_.file);
}

/*
 * \brief Get the intensity
 * \return Return the intensity.
 */
float ReaderStructBEAM::getIntensity()
{
    float intensity = ntoflib_.getReader(header_.isOnCastor)
                          ->readFloat(startPosition_ + sizeof(int32_t) * 5,
                                      header_.file);
    return (intensity < 10000) ? intensity * 1e10 : intensity;
}

/*
 * \brief Get the type
 * \return Return the type of the beam.
 */
int32_t ReaderStructBEAM::getType()
{
    if (beamType_ == 0)
    {
        std::string type;
        switch (getRevisionNumber())
        {
        case 0:
            type = ntoflib_.getReader(header_.isOnCastor)
                       ->readString(startPosition_ + (sizeof(int32_t) * 6), 4,
                                    header_.file);
            beamType_ = typeToInt(type);
            break;
        case 1:
        case 2:
            beamType_ = ntoflib_.getReader(header_.isOnCastor)
                            ->readInt32(startPosition_ + (sizeof(int32_t) * 6),
                                        header_.file);
            if (beamType_ < 0 || beamType_ > 3)
            {
                type = ntoflib_.getReader(header_.isOnCastor)
                           ->readString(startPosition_ + (sizeof(int32_t) * 6),
                                        4, header_.file);
                beamType_ = typeToInt(type);
            }
            break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return beamType_;
}

/*
 * \brief Convert the type from string to integer
 * \param type: the type as a string
 * \return Return the type name of the beam as an integer.
 */
int32_t ReaderStructBEAM::typeToInt(std::string type)
{
    if (type == "CALIBRATION")
    {
        return 1;
    }
    else if (type == "PRIMARY" || type == "TOF ")
    {
        return 2;
    }
    else if (type == "PARASITIC" || type == "EAST")
    {
        return 3;
    }
    else if (type == "NGVT" || type == "INVD" || type == "XPRD")
    {
        return -1;
    }
    else
    {
        return 0;
    }
}

/*
 * \brief Get the type name
 * \return Return the type name of the beam.
 */
std::string ReaderStructBEAM::getTypeName()
{
    std::string res;
    int32_t type;
    switch (getRevisionNumber())
    {
    case 0:
        res = ntoflib_.getReader(header_.isOnCastor)
                  ->readString(startPosition_ + (sizeof(int32_t) * 6), 4,
                               header_.file);
        break;
    case 1:
    case 2:
        type = ntoflib_.getReader(header_.isOnCastor)
                   ->readInt32(startPosition_ + (sizeof(int32_t) * 6),
                               header_.file);
        if (type >= 1 && type <= 3)
        {
            switch (type)
            {
            case 1: res = "CALIBRATION"; break;
            case 2: res = "PRIMARY"; break;
            case 3: res = "PARASITIC"; break;
            default: res = ""; break;
            }
        }
        else
        {
            res = ntoflib_.getReader(header_.isOnCastor)
                      ->readString(startPosition_ + (sizeof(int32_t) * 6), 4,
                                   header_.file);
        }
        break;
    default:
        throw NtofLibException("Revision number unknown", __FILE__, __LINE__);
        break;
    }
    return res;
}

/*
 * \brief Get the details of the actual header
 * \return Return the details of the header
 */
const Header &ReaderStructBEAM::getHeader() const
{
    return header_;
}

/*
 * \brief Set the details of the header
 * \param header: new header used to replace the old one
 */
void ReaderStructBEAM::setHeader(Header &header)
{
    header_ = header;
}

} /* namespace lib */
} /* namespace ntof */
