/*
 * ReaderStructEVEH.cpp
 *
 *  Created on: Jan 12, 2015
 *      Author: agiraud
 */

#include "ReaderStructEVEH.h"

#include "HeaderManager.h"
#include "NtofLib.h"
#include "Reader.h"

namespace ntof {
namespace lib {

/*
 * \brief Constructor
 * \param header Details of the header
 * \param ntoflib give access to the reader
 */
ReaderStructEVEH::ReaderStructEVEH(const Header &header, NtofLib &ntoflib) :
    ntoflib_(ntoflib),
    startPosition_(header.pos),
    revisionNumber_(-1),
    beamType_(0),
    header_(header)
{}

/*
 * \brief Destructor
 */
ReaderStructEVEH::~ReaderStructEVEH() {}

/*
 * \brief Get the title
 * \return Return the title.
 */
std::string ReaderStructEVEH::getTitle()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readString(startPosition_, 4, header_.file);
}

/*
 * \brief Get the revision number
 * \return Return the revision number.
 */
uint32_t ReaderStructEVEH::getRevisionNumber()
{
    if (revisionNumber_ == -1)
    {
        revisionNumber_ = ntoflib_.getReader(header_.isOnCastor)
                              ->readUint32(
                                  startPosition_ + (sizeof(int32_t) * 1),
                                  header_.file);
    }
    return revisionNumber_;
}

/*
 * \brief Get the reserved
 * \return Return the reserved.
 */
uint32_t ReaderStructEVEH::getReserved()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readInt32(startPosition_ + (sizeof(int32_t) * 2), header_.file);
}

/*
 * \brief Get the length
 * \return Return the length.
 */
uint32_t ReaderStructEVEH::getLength()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readInt32(startPosition_ + (sizeof(int32_t) * 3), header_.file);
}

/*
 * \brief Get the size of the event
 * \return Return the size of the event.
 */
uint32_t ReaderStructEVEH::getSizeOfEvent()
{
    int32_t res;
    switch (getRevisionNumber())
    {
    case 0:
    case 1:
        res = (int32_t) ntoflib_.getReader(header_.isOnCastor)
                  ->readInt32(startPosition_ + (sizeof(int32_t) * 4),
                              header_.file);
        break;
    default:
        throw NtofLibException("Revision number unknown", __FILE__, __LINE__);
        break;
    }
    return res;
}

/*
 * \brief Get the event number
 * \return Return the event number.
 */
uint32_t ReaderStructEVEH::getEventNumber()
{
    int32_t res;
    switch (getRevisionNumber())
    {
    case 0:
    case 1:
        res = ntoflib_.getReader(header_.isOnCastor)
                  ->readInt32(startPosition_ + (sizeof(int32_t) * 5),
                              header_.file);
        break;
    default:
        throw NtofLibException("Revision number unknown", __FILE__, __LINE__);
        break;
    }
    return res;
}

/*
 * \brief Get the run number
 * \return Return the run number.
 */
uint32_t ReaderStructEVEH::getRunNumber()
{
    int32_t res;
    switch (getRevisionNumber())
    {
    case 0:
    case 1:
        res = ntoflib_.getReader(header_.isOnCastor)
                  ->readInt32(startPosition_ + (sizeof(int32_t) * 6),
                              header_.file);
        break;
    default:
        throw NtofLibException("Revision number unknown", __FILE__, __LINE__);
        break;
    }
    return res;
}

/*
 * \brief Get the time of the event
 * \return Return the time of the event.
 */
uint32_t ReaderStructEVEH::getTime()
{
    int32_t res;
    switch (getRevisionNumber())
    {
    case 0:
    case 1:
        res = ntoflib_.getReader(header_.isOnCastor)
                  ->readInt32(startPosition_ + (sizeof(int32_t) * 7),
                              header_.file);
        break;
    default:
        throw NtofLibException("Revision number unknown", __FILE__, __LINE__);
        break;
    }
    return res;
}

/*
 * \brief Get the date of the event
 * \return Return the date of the event.
 */
uint32_t ReaderStructEVEH::getDate()
{
    int32_t res;
    switch (getRevisionNumber())
    {
    case 0:
    case 1:
        res = ntoflib_.getReader(header_.isOnCastor)
                  ->readInt32(startPosition_ + (sizeof(int32_t) * 8),
                              header_.file);
        break;
    default:
        throw NtofLibException("Revision number unknown", __FILE__, __LINE__);
        break;
    }
    return res;
}

/*
 * \brief Get the hardware timestamp
 * \return Return the timestamp
 */
double ReaderStructEVEH::getComputerTimestamp()
{
    double res;
    switch (getRevisionNumber())
    {
    case 0:
        throw NtofLibException(
            "This data doesn't exist for this revision number", __FILE__,
            __LINE__);
        break;
    case 1:
        res = ntoflib_.getReader(header_.isOnCastor)
                  ->readDouble(startPosition_ + (sizeof(int32_t) * 10),
                               header_.file);
        break;
    default:
        throw NtofLibException("Revision number unknown", __FILE__, __LINE__);
        break;
    }
    return res;
}

/*
 * \brief Get the BCT timestamp
 * \return Return the timestamp
 */
double ReaderStructEVEH::getBCTTimestamp()
{
    double res;
    switch (getRevisionNumber())
    {
    case 0:
        throw NtofLibException(
            "This data doesn't exist for this revision number", __FILE__,
            __LINE__);
        break;
    case 1:
        res = ntoflib_.getReader(header_.isOnCastor)
                  ->readDouble(startPosition_ + (sizeof(int32_t) * 12),
                               header_.file);
        break;
    default:
        throw NtofLibException("Revision number unknown", __FILE__, __LINE__);
        break;
    }
    return res;
}

/*
 * \brief Get the beam intensity
 * \return Return the beam intensity
 */
float ReaderStructEVEH::getBeamIntensity()
{
    float res;
    switch (getRevisionNumber())
    {
    case 0:
        throw NtofLibException(
            "This data doesn't exist for this revision number", __FILE__,
            __LINE__);
        break;
    case 1:
        res = ntoflib_.getReader(header_.isOnCastor)
                  ->readFloat(startPosition_ + (sizeof(int32_t) * 14),
                              header_.file);
        break;
    default:
        throw NtofLibException("Revision number unknown", __FILE__, __LINE__);
        break;
    }
    return res;
}

/*
 * \brief Get the beam type
 * \return Return the type of the beam as an enum.
 */
uint32_t ReaderStructEVEH::getBeamType()
{
    if (beamType_ == 0)
    {
        switch (getRevisionNumber())
        {
        case 0:
            throw NtofLibException(
                "This data doesn't exist for this revision number", __FILE__,
                __LINE__);
            break;
        case 1:
            beamType_ = ntoflib_.getReader(header_.isOnCastor)
                            ->readUint32(startPosition_ + (sizeof(int32_t) * 15),
                                         header_.file);
            break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return beamType_;
}

/*
 * \brief Convert the type of beam from int to string
 * \return Return the type of the beam as an enum.
 */
int32_t ReaderStructEVEH::typeToInt(std::string type)
{
    if (type == "CALIBRATION")
        return 1;
    else if (type == "PRIMARY" || type == "TOF ")
        return 2;
    else if (type == "PARASITIC" || type == "EAST")
        return 3;
    else if (type == "NGVT" || type == "INVD" || type == "XPRD")
        return -1;
    return 0;
}

/*
 * \brief Get the beam type
 * \return Return the beam type as a string
 */
std::string ReaderStructEVEH::getBeamTypeName()
{
    std::string res;
    switch (getRevisionNumber())
    {
    case 0:
        throw NtofLibException(
            "This data doesn't exist for this revision number", __FILE__,
            __LINE__);
        break;
    case 1:
        switch (getBeamType())
        {
        case 1: res = "CALIBRATION"; break;
        case 2: res = "PRIMARY"; break;
        case 3: res = "PARASITIC"; break;
        default:
            throw NtofLibException("Beam type unknown", __FILE__, __LINE__);
            break;
        }
        break;
    default:
        throw NtofLibException("Revision number unknown", __FILE__, __LINE__);
        break;
    }
    return res;
}

/*
 * \brief Get the position of the event in the file
 * \return Return the position of the event in the file
 */
uint64_t ReaderStructEVEH::getCurrentSeek()
{
    return startPosition_;
}

/*
 * \brief Get the details of the actual header
 * \return Return the details of the header
 */
const Header &ReaderStructEVEH::getHeader() const
{
    return header_;
}

/*
 * \brief Set the details of the header
 * \param header: new header used to replace the old one
 */
void ReaderStructEVEH::setHeader(Header &header)
{
    header_ = header;
}

} /* namespace lib */
} /* namespace ntof */
