/*
 * ReaderStructINDX.cpp
 *
 *  Created on: Jan 12, 2015
 *      Author: agiraud
 */

#include "ReaderStructINDX.h"

#include <boost/scoped_array.hpp>

#include "HeaderManager.h"
#include "NtofLib.h"
#include "Reader.h"

namespace ntof {
namespace lib {

/*
 * \brief Constructor
 * \param header Details of the header
 * \param ntoflib give access to the reader
 */
ReaderStructINDX::ReaderStructINDX(const Header &header, NtofLib &ntoflib) :
    ntoflib_(ntoflib),
    startPosition_(header.pos),
    revisionNumber_(-1),
    header_(header)
{
    if (getRevisionNumber() == 1)
    {
        setIndexFields();
    }
}

/*
 * \brief Destructor
 */
ReaderStructINDX::~ReaderStructINDX() {}

/*
 * \brief Get the title
 * \return Return the title.
 */
std::string ReaderStructINDX::getTitle()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readString(startPosition_, 4, header_.file);
}

/*
 * \brief Get the revision number
 * \return Return the revision number.
 */
uint32_t ReaderStructINDX::getRevisionNumber()
{
    if (revisionNumber_ == -1)
    {
        revisionNumber_ = ntoflib_.getReader(header_.isOnCastor)
                              ->readUint32(
                                  startPosition_ + (sizeof(int32_t) * 1),
                                  header_.file);
    }
    return revisionNumber_;
}

/*
 * \brief Get the reserved
 * \return Return the reserved.
 */
uint32_t ReaderStructINDX::getReserved()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + sizeof(int32_t) * 2, header_.file);
}

/*
 * \brief Get the length
 * \return Return the length.
 */
uint32_t ReaderStructINDX::getLength()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + sizeof(int32_t) * 3, header_.file);
}

/*
 * \brief Get the list of the position of the ACQC in the raw file
 * \return Return the list of the position of the ACQC in the raw file.
 */
std::list<int32_t> ReaderStructINDX::getIndexList()
{
    if (getRevisionNumber() != 0)
    {
        throw NtofLibException("Revision number incorrect (expected 0)",
                               __FILE__, __LINE__);
    }

    std::list<int32_t> indexList;
    for (uint32_t i = 0; i < getLength(); ++i)
    {
        indexList.push_back(
            ntoflib_.getReader(header_.isOnCastor)
                ->readInt32(startPosition_ + sizeof(int32_t) * (4 + i),
                            header_.file));
    }
    return indexList;
}

/*
 * \brief Get the details of the actual header
 * \return Return the details of the header
 */
const Header &ReaderStructINDX::getHeader() const
{
    return header_;
}

/*
 * \brief Set the details of the header
 * \param header: new header used to replace the old one
 */
void ReaderStructINDX::setHeader(Header &header)
{
    header_ = header;
}

/*
 * \brief Get the list of the positions of the EVEH in the raw files
 * \return Return the list of the positions of the EVEH in the raw files.
 */
std::vector<INDEX_FIELD> ReaderStructINDX::getIndexFields()
{
    return index_;
}

/*
 * \brief Initialize the list of the positions of the EVEH in the raw files
 */
void ReaderStructINDX::setIndexFields()
{
    if (getRevisionNumber() != 1)
    {
        throw NtofLibException("Revision number incorrect (expected 1)",
                               __FILE__, __LINE__);
    }

    uint64_t position = startPosition_ + (sizeof(int32_t) * 4);
    uint64_t size = getLength() * sizeof(int32_t); // Number of bytes used
    uint64_t nbFields = size / sizeof(INDEX_FIELD);

    boost::scoped_array<char> dataPtr(new char[size]);
    char *data = dataPtr.get();
    ntoflib_.getReader(header_.isOnCastor)
        ->readArrayChar(position, size, data, header_.file);

    // index_ = (std::vector<INDEX_FIELD>)(*data);
    for (uint64_t i = 0; i <= nbFields; ++i)
    {
        INDEX_FIELD *field = (INDEX_FIELD *) (data + (i * sizeof(INDEX_FIELD)));
        index_.push_back(*field);
    }

    //            std::vector<int8_t> indexHeader;
    //            ntoflib_.getReader(header_.isOnCastor)->readVectorInt8(position,
    //            nbBytes, indexHeader, header_.file);

    // index_.clear();
    // index_.reserve(nbFields);
    // index_(indexHeader.begin(), indexHeader.end());
    /*
    uint64_t position = startPosition_ + (sizeof(int32_t)*4);
    uint64_t numberOfIndexes = (getLength()*sizeof(int32_t)) /
    sizeof(INDEX_FIELD);
    ntoflib_.getReader(header_.isOnCastor)->readVector(position,
    numberOfIndexes, index_, header_.file);

    while(position < length){
        //Create the field
        uint32_t streamNumber =
    ntoflib_.getReader(header_.isOnCastor)->readUint32(position+(sizeof(int32_t)*0),
    header_.file); uint32_t segmentNumber =
    ntoflib_.getReader(header_.isOnCastor)->readUint32(position+(sizeof(int32_t)*1),
    header_.file); uint32_t validatedNumber =
    ntoflib_.getReader(header_.isOnCastor)->readUint32(position+(sizeof(int32_t)*2),
    header_.file); uint32_t triggerNumber =
    ntoflib_.getReader(header_.isOnCastor)->readUint32(position+(sizeof(int32_t)*3),
    header_.file); uint64_t offset =
    ntoflib_.getReader(header_.isOnCastor)->readUint64(position+(sizeof(int32_t)*4),
    header_.file); INDEX_FIELD index = {streamNumber, segmentNumber,
    validatedNumber, triggerNumber, offset}; index_.push_back(index);

        position += (sizeof(int32_t)*4) + sizeof(int64_t);
    }*/
}
} /* namespace lib */
} /* namespace ntof */
