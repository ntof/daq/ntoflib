/*
 * ReaderStructINFO.cpp
 *
 *  Created on: Jan 12, 2015
 *      Author: agiraud
 */

#include "ReaderStructINFO.h"

#include <sstream> // std::ostringstream

#include "HeaderManager.h"
#include "NtofLib.h"
#include "Reader.h"

namespace ntof {
namespace lib {

/*
 * \brief Constructor
 * \param header Details of the header
 * \param ntoflib give access to the reader
 */
ReaderStructINFO::ReaderStructINFO(const Header &header, NtofLib &ntoflib) :
    ntoflib_(ntoflib),
    startPosition_(header.pos),
    revisionNumber_(-1),
    header_(header)
{}

/*
 * \brief Destructor
 */
ReaderStructINFO::~ReaderStructINFO() {}

/*
 * \brief Get the title
 * \return Return the title.
 */
std::string ReaderStructINFO::getTitle()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readString(startPosition_, 4, header_.file);
}

/*
 * \brief Get the revision number
 * \return Return the revision number.
 */
uint32_t ReaderStructINFO::getRevisionNumber()
{
    if (revisionNumber_ == -1)
    {
        revisionNumber_ = ntoflib_.getReader(header_.isOnCastor)
                              ->readUint32(
                                  startPosition_ + (sizeof(int32_t) * 1),
                                  header_.file);
    }
    return revisionNumber_;
}

/*
 * \brief Get the reserved
 * \return Return the reserved.
 */
uint32_t ReaderStructINFO::getReserved()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + sizeof(int32_t) * 2, header_.file);
}

/*
 * \brief Get the length
 * \return Return the length.
 */
uint32_t ReaderStructINFO::getLength()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + sizeof(int32_t) * 3, header_.file);
}

/*
 * \brief Get the description
 * \return Return the description.
 */
std::string ReaderStructINFO::getData()
{
    std::ostringstream oss;
    for (uint32_t i = 0; i < getLength(); ++i)
    {
        oss << ntoflib_.getReader(header_.isOnCastor)
                   ->readUint32(startPosition_ + sizeof(int32_t) * (4 + i),
                                header_.file);
    }
    return oss.str();
}

/*
 * \brief Get the details of the actual header
 * \return Return the details of the header
 */
const Header &ReaderStructINFO::getHeader() const
{
    return header_;
}

/*
 * \brief Set the details of the header
 * \param header: new header used to replace the old one
 */
void ReaderStructINFO::setHeader(Header &header)
{
    header_ = header;
}
} /* namespace lib */
} /* namespace ntof */
