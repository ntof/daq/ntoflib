/*
 * ReaderStructMODH.cpp
 *
 *  Created on: Jan 12, 2015
 *      Author: agiraud
 */

#include "ReaderStructMODH.h"

#include <iostream>
#include <sstream>

#include "HeaderManager.h"
#include "NtofLib.h"
#include "Reader.h"

namespace ntof {
namespace lib {

/*
 * \brief Constructor
 * \param header Details of the header
 * \param ntoflib give access to the reader
 */
ReaderStructMODH::ReaderStructMODH(const Header &header, NtofLib &ntoflib) :
    ntoflib_(ntoflib),
    startPosition_(header.pos),
    revisionNumber_(-1),
    acqcExpected_(-1),
    numberOfChannel_(0),
    channelId_(-1),
    header_(header)
{
    getChannelsConfig();
}

/*
 * \brief Destructor
 */
ReaderStructMODH::~ReaderStructMODH() {}

/*
 * \brief Get the title
 * \return Return the title.
 */
std::string ReaderStructMODH::getTitle()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readString(startPosition_, 4, header_.file);
}

/*
 * \brief Get the revision number
 * \return Return the revision number.
 */
int32_t ReaderStructMODH::getRevisionNumber()
{
    if (revisionNumber_ == -1)
    {
        int64_t position = startPosition_ + (sizeof(int32_t) * 1);
        revisionNumber_ = ntoflib_.getReader(header_.isOnCastor)
                              ->readUint32(position, header_.file);
    }
    return revisionNumber_;
}

/*
 * \brief Get the reserved
 * \return Return the reserved.
 */
uint32_t ReaderStructMODH::getReserved()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + (sizeof(uint32_t) * 2), header_.file);
}

/*
 * \brief Get the length
 * \return Return the length.
 */
uint32_t ReaderStructMODH::getLength()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + (sizeof(uint32_t) * 3), header_.file);
}

/*
 * \brief Get the number of channels in the run
 * \return Return the number of channels in the run.
 */
uint32_t ReaderStructMODH::getNbChannels()
{
    if (numberOfChannel_ == 0)
    {
        numberOfChannel_ = ntoflib_.getReader(header_.isOnCastor)
                               ->readUint32(
                                   startPosition_ + (sizeof(uint32_t) * 4),
                                   header_.file);
    }
    return numberOfChannel_;
}

/*
 * \brief Initialize the next channel configuration
 * \return Return true if the next channel is ready to be read or false if there
 * is no more channel configuration.
 */
bool ReaderStructMODH::getNextChannel()
{
    bool res = false;
    ++channelId_;
    // >= because the channelId begin from 0
    if (channelId_ >= (int32_t) getNbChannels())
    {
        channelId_ = -1;
        res = false;
    }
    else
    {
        switch (getRevisionNumber())
        {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4: res = true; break;
        default:
            channelId_ = -1;
            res = false;
            break;
        }
    }
    return res;
}

/*
 * \brief Get the index of the channel configuration
 * \param detectorType The type of detector on 4 digits
 * \param detectorId The id of detector
 * \return Return the index value
 */
uint32_t ReaderStructMODH::getIndex(std::string detectorType,
                                    uint32_t detectorId)
{
    int32_t i = 0;
    for (std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
         it != listConfig_.end(); ++it)
    {
        if ((*it).detectorType == detectorType && (*it).detectorId == detectorId)
        {
            // if the good channel has been found we stop the loop.
            break;
        }
        ++i;
    }
    return i;
}

/*
 * \brief Set the index of the channel configuration
 * \param detectorType The type of detector on 4 digits
 * \param detectorId The id of detector
 * \return Return true if the channel has been found or false if it's not.
 */
bool ReaderStructMODH::setIndex(std::string detectorType, uint32_t detectorId)
{
    bool res = false;
    int32_t i = 0;

    for (std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
         it != listConfig_.end(); ++it)
    {
        if ((*it).detectorType == detectorType && (*it).detectorId == detectorId)
        {
            switch (getRevisionNumber())
            {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
                channelId_ = i;
                res = true;
                break;
            default:
                channelId_ = 0;
                res = false;
                break;
            }

            // if the good channel has been found we stop the loop.
            break;
        }
        ++i;
    }

    return res;
}

/*
 * \brief Initialize the actual channel according the id of the DAQ elements
 * \param channelId The id of the channel
 * \param moduleId The id of the card
 * \param streamId The id of the stream
 * \param chassisId The id of the chassis
 * \return Return true if the channel configuration has been found or false in
 * the other case.
 */
bool ReaderStructMODH::setIndex(uint32_t channelId,
                                uint32_t moduleId,
                                uint32_t streamId,
                                uint32_t chassisId)
{
    bool res = false;
    int32_t i = 0;

    for (std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
         it != listConfig_.end(); ++it)
    {
        if ((*it).channel == channelId && (*it).module == moduleId &&
            (*it).stream == streamId && (*it).chassis == chassisId)
        {
            switch (getRevisionNumber())
            {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
                channelId_ = i;
                res = true;
                break;
            default:
                channelId_ = 0;
                res = false;
                break;
            }

            // if the good channel has been found we stop the loop.
            break;
        }
        ++i;
    }

    return res;
}

/*
 * \brief Get the detector type
 * \return Return the value of the detector type
 */
std::string ReaderStructMODH::getDetectorType()
{
    if (channelId_ == -1)
    {
        throw NtofLibException("No channel selected", __FILE__, __LINE__);
    }
    else
    {
        std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
        std::advance(it, channelId_);
        switch (getRevisionNumber())
        {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4: return (*it).detectorType; break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return "";
}

/*
 * \brief Get the detector id
 * \return Return the value of the detector id
 */
uint32_t ReaderStructMODH::getDetectorId()
{
    if (channelId_ == -1)
    {
        throw NtofLibException("No channel selected", __FILE__, __LINE__);
    }
    else
    {
        std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
        std::advance(it, channelId_);
        switch (getRevisionNumber())
        {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4: return (*it).detectorId; break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return -1;
}

/*
 * \brief Get the module type
 * \return Return the value of the module type
 */
std::string ReaderStructMODH::getModuleType()
{
    if (channelId_ == -1)
    {
        throw NtofLibException("No channel selected", __FILE__, __LINE__);
    }
    else
    {
        std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
        std::advance(it, channelId_);
        switch (getRevisionNumber())
        {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4: return (*it).moduleType; break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return "";
}

/*
 * \brief Get the channel id
 * \return Return the value of the channel id
 */
uint32_t ReaderStructMODH::getChannel()
{
    if (channelId_ == -1)
    {
        throw NtofLibException("No channel selected", __FILE__, __LINE__);
    }
    else
    {
        std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
        std::advance(it, channelId_);
        switch (getRevisionNumber())
        {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4: return (*it).channel; break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return -1;
}

/*
 * \brief Get the module id
 * \return Return the value of the module id
 */
uint32_t ReaderStructMODH::getModule()
{
    if (channelId_ == -1)
    {
        throw NtofLibException("No channel selected", __FILE__, __LINE__);
    }
    else
    {
        std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
        std::advance(it, channelId_);
        switch (getRevisionNumber())
        {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4: return (*it).module; break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return -1;
}

/*
 * \brief Get the chassis id
 * \return Return the value of the chassis id
 */
uint32_t ReaderStructMODH::getChassis()
{
    if (channelId_ == -1)
    {
        throw NtofLibException("No channel selected", __FILE__, __LINE__);
    }
    else
    {
        std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
        std::advance(it, channelId_);
        switch (getRevisionNumber())
        {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4: return (*it).chassis; break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return -1;
}

/*
 * \brief Get the stream id
 * \return Return the value of the stream id
 */
uint32_t ReaderStructMODH::getStream()
{
    if (channelId_ == -1)
    {
        throw NtofLibException("No channel selected", __FILE__, __LINE__);
    }
    else
    {
        std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
        std::advance(it, channelId_);
        switch (getRevisionNumber())
        {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4: return (*it).stream; break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return -1;
}

/*
 * \brief Get the sample rate
 * \return Return the value of the sample rate
 */
float ReaderStructMODH::getSampleRate()
{
    if (channelId_ == -1)
    {
        throw NtofLibException("No channel selected", __FILE__, __LINE__);
    }
    else
    {
        std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
        std::advance(it, channelId_);
        switch (getRevisionNumber())
        {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4: return (*it).sampleRate; break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return -1;
}

/*
 * \brief Get the sample size
 * \return Return the value of the sample size
 */
float ReaderStructMODH::getSampleSize()
{
    if (channelId_ == -1)
    {
        throw NtofLibException("No channel selected", __FILE__, __LINE__);
    }
    else
    {
        std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
        std::advance(it, channelId_);
        switch (getRevisionNumber())
        {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4: return (*it).sampleSize; break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return -1;
}

/*
 * \brief Get the full scale
 * \return Return the value of the full scale
 */
float ReaderStructMODH::getFullScale()
{
    if (channelId_ == -1)
    {
        throw NtofLibException("No channel selected", __FILE__, __LINE__);
    }
    else
    {
        std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
        std::advance(it, channelId_);
        switch (getRevisionNumber())
        {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4: return (*it).fullScale; break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return -1;
}

/*
 * \brief Get the full scale in millivolts
 * \param FullScale to convert
 * \return Return the value of the full scale in millivolts
 */
float ReaderStructMODH::getFullScaleInMilliVolts(float fullScale)
{
    float res = -1;
    float fs = fullScale == -1 ? getFullScale() : fullScale;
    int32_t index = fs;
    switch (getRevisionNumber())
    {
    case 0:
    case 1:
    case 2:
        switch (index)
        {
        case 0: res = 50; break;
        case 1: res = 100; break;
        case 2: res = 200; break;
        case 3: res = 500; break;
        case 4: res = 1000; break;
        case 5: res = 2000; break;
        case 6: res = 5000; break;
        default:
            throw NtofLibException("The value of the full scale is wrong.",
                                   __FILE__, __LINE__);
            break;
        }
        break;
    case 3:
    case 4: res = fs; break;
    default:
        throw NtofLibException("Revision number unknown", __FILE__, __LINE__);
        break;
    }
    return res;
}

/*
 * \brief Get the delay time
 * \return Return the value of the delay time
 */
int32_t ReaderStructMODH::getDelayTime()
{
    if (channelId_ == -1)
    {
        throw NtofLibException("No channel selected", __FILE__, __LINE__);
    }
    else
    {
        std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
        std::advance(it, channelId_);
        switch (getRevisionNumber())
        {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4: return (*it).delayTime; break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return -1;
}

/*
 * \brief Get the maximal value that the data can have
 * \param moduleType the type of card
 * \return Return the maximal value for a card type
 */
int32_t ReaderStructMODH::getMaxDataValue(std::string moduleType)
{
    return (1 << (getNbBits(moduleType) - 1)) - 1;
    //            std::string moduleType_ = (moduleType == "" ? getModuleType()
    //            : moduleType); if (moduleType_ == "ETEP" || moduleType_ ==
    //            "ACQC" || moduleType_ == "DC240" || moduleType_ == "DC270"){
    //                return 127;
    //            } else if (moduleType_ == "DC282"){
    //                return 32767;
    //            } else if (moduleType_ == "ADQ412"){
    //                return 2047;
    //            } else{
    //                throw NtofLibException("Type of card unknown");
    //            }
    //            return -1;
}

/*
 * \brief Get the minimal value that the data can have
 * \param moduleType the type of card
 * \return Return the minimal value for a card type
 */
int32_t ReaderStructMODH::getMinDataValue(std::string moduleType)
{
    return -(1 << (getNbBits(moduleType) - 1));
    //            std::string moduleType_ = (moduleType == "" ? getModuleType()
    //            : moduleType); if (moduleType_ == "ETEP" || moduleType_ ==
    //            "ACQC" || moduleType_ == "DC240" || moduleType_ == "DC270"){
    //                return -128;
    //            } else if (moduleType_ == "DC282"){
    //                return -32768;
    //            } else if (moduleType_ == "ADQ412"){
    //                return -2048;
    //            } else{
    //                throw NtofLibException("Type of card unknown");
    //            }
    //            return -1;
}

/*
 * \brief Get number of bits for a card type
 * \param moduleType the type of card
 * \return Return the number of bits for a card type
 */
int32_t ReaderStructMODH::getNbBits(std::string moduleType)
{
    std::string moduleType_ = (moduleType == "" ? getModuleType() : moduleType);
    if (moduleType_ == "ETEP" || moduleType_ == "ACQC" ||
        moduleType_ == "DC240" || moduleType_ == "DC270" ||
        moduleType_ == "A240" || moduleType_ == "A270")
    {
        return 8;
    }
    else if (moduleType_ == "DC282" || moduleType_ == "A282" ||
             moduleType_ == "S014")
    {
        return 16;
    }
    else if (moduleType_ == "ADQ412" || moduleType_ == "S412")
    {
        return 12;
        //            } else if (moduleType_ == "S014"){
        //                return 14;
    }
    else
    {
        throw NtofLibException("Type of card unknown : " + moduleType_,
                               __FILE__, __LINE__);
    }
    return -1;
}

/*
 * \brief Get number of bits for a card type
 * \param moduleType the type of card
 * \return Return the ADC range for a card type
 */
int32_t ReaderStructMODH::getADCRange(std::string moduleType)
{
    return (1 << getNbBits(moduleType));
}

/*
 * \brief Get the threshold in ADC
 * \return Return the value of the threshold in ADC
 */
int32_t ReaderStructMODH::getThresholdADC()
{
    if (channelId_ == -1)
    {
        throw NtofLibException("No channel selected", __FILE__, __LINE__);
    }
    else
    {
        std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
        std::advance(it, channelId_);
        switch (getRevisionNumber())
        {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4: return (*it).thresholdADC; break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return -1;
}

/*
 * \brief Get the threshold in millivolts
 * \return Return the value of the threshold in millivolts
 */
float ReaderStructMODH::getThresholdMilliVolts()
{
    if (channelId_ == -1)
    {
        throw NtofLibException("No channel selected", __FILE__, __LINE__);
    }
    else
    {
        std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
        std::advance(it, channelId_);
        switch (getRevisionNumber())
        {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4: return (*it).thresholdMV; break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return -1;
}

/*
 * \brief Get the threshold sign
 * \return Return the value of the threshold sign
 */
int32_t ReaderStructMODH::getThresholdSign()
{
    if (channelId_ == -1)
    {
        throw NtofLibException("No channel selected", __FILE__, __LINE__);
    }
    else
    {
        std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
        std::advance(it, channelId_);
        switch (getRevisionNumber())
        {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4: return (*it).thresholdSign; break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return -1;
}

/*
 * \brief Get the offset
 * \return Return the value of the offset
 */
float ReaderStructMODH::getOffset()
{
    if (channelId_ == -1)
    {
        throw NtofLibException("No channel selected", __FILE__, __LINE__);
    }
    else
    {
        std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
        std::advance(it, channelId_);
        switch (getRevisionNumber())
        {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4: return (*it).offset; break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return -1;
}

/*
 * \brief Get the presample
 * \return Return the value of the presample
 */
uint32_t ReaderStructMODH::getPreSample()
{
    if (channelId_ == -1)
    {
        throw NtofLibException("No channel selected", __FILE__, __LINE__);
    }
    else
    {
        std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
        std::advance(it, channelId_);
        switch (getRevisionNumber())
        {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4: return (*it).preSample; break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return -1;
}

/*
 * \brief Get the postsample
 * \return Return the value of the postsample
 */
uint32_t ReaderStructMODH::getPostSample()
{
    if (channelId_ == -1)
    {
        throw NtofLibException("No channel selected", __FILE__, __LINE__);
    }
    else
    {
        std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
        std::advance(it, channelId_);
        switch (getRevisionNumber())
        {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4: return (*it).postSample; break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return -1;
}

/*
 * \brief Get the clock state
 * \return Return the value of the clock state
 */
std::string ReaderStructMODH::getClockState()
{
    if (channelId_ == -1)
    {
        throw NtofLibException("No channel selected", __FILE__, __LINE__);
    }
    else
    {
        std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
        std::advance(it, channelId_);
        switch (getRevisionNumber())
        {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4: return (*it).clockState; break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return "";
}

/*
 * \brief Get the zero suppression start
 * \return Return the value of the zero suppression start
 */
uint32_t ReaderStructMODH::getZeroSuppressionStart()
{
    if (channelId_ == -1)
    {
        throw NtofLibException("No channel selected", __FILE__, __LINE__);
    }
    else
    {
        std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
        std::advance(it, channelId_);
        switch (getRevisionNumber())
        {
        case 0:
            throw NtofLibException(
                "This data doesn't exist for this revision number", __FILE__,
                __LINE__);
            break;
        case 1:
        case 2:
        case 3:
        case 4: return (*it).zeroSuppressionStart; break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return -1;
}

/*
 * \brief Get the master detector type
 * \return Return the master detector type
 */
std::string ReaderStructMODH::getMasterDetectorType()
{
    if (channelId_ == -1)
    {
        throw NtofLibException("No channel selected", __FILE__, __LINE__);
    }
    else
    {
        std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
        std::advance(it, channelId_);
        switch (getRevisionNumber())
        {
        case 0:
        case 1:
        case 2:
        case 3:
            throw NtofLibException(
                "This data doesn't exist for this revision number", __FILE__,
                __LINE__);
            break;
        case 4: return (*it).masterDetectorType; break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return "";
}

/*
 * \brief Get the master detector ID
 * \return Return the master detector ID
 */
uint32_t ReaderStructMODH::getMasterDetectorId()
{
    if (channelId_ == -1)
    {
        throw NtofLibException("No channel selected", __FILE__, __LINE__);
    }
    else
    {
        std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
        std::advance(it, channelId_);
        switch (getRevisionNumber())
        {
        case 0:
        case 1:
        case 2:
        case 3:
            throw NtofLibException(
                "This data doesn't exist for this revision number", __FILE__,
                __LINE__);
            break;
        case 4: return (*it).masterDetectorId; break;
        default:
            throw NtofLibException("Revision number unknown", __FILE__,
                                   __LINE__);
            break;
        }
    }
    return -1;
}

/*
 * \brief Get all the channel configuration as a list
 * \return Return the value of the get the channel configuration as a list
 */
std::list<CHAN_CONFIG> ReaderStructMODH::getChannelsConfig()
{
    if (listConfig_.empty())
    {
        //                std::cout << "Revision number = " <<
        //                getRevisionNumber() << std::endl;
        uint32_t nbChannel = getNbChannels();
        int32_t loc = startPosition_ + (sizeof(uint32_t) * 5);
        for (uint32_t i = 0; i < nbChannel; ++i)
        {
            Notification::notify(ntoflib_.notificationListener(),
                                 Notification::MODH_INIT, i, nbChannel);
            channelId_ = i;
            CHAN_CONFIG config;

            config.detectorType = ntoflib_.getReader(header_.isOnCastor)
                                      ->readString(loc, 4, header_.file);
            loc += sizeof(uint32_t);
            config.detectorId = ntoflib_.getReader(header_.isOnCastor)
                                    ->readUint32(loc, header_.file);
            loc += sizeof(uint32_t);

            switch (getRevisionNumber())
            {
            case 0:
            case 2:
            case 3:
            case 4:
                config.moduleType = ntoflib_.getReader(header_.isOnCastor)
                                        ->readString(loc, 4, header_.file);
                loc += sizeof(uint32_t);
                break;
            case 1:
                config.moduleType = ntoflib_.getReader(header_.isOnCastor)
                                        ->readString(loc, 8, header_.file);
                loc += sizeof(uint64_t);
                break;
            default:
                std::ostringstream message;
                message << "Revision number unknown : " << getRevisionNumber();
                throw NtofLibException(message.str().c_str(), __FILE__,
                                       __LINE__);
                break;
            }

            int32_t str_ch_mod = ntoflib_.getReader(header_.isOnCastor)
                                     ->readInt32(loc, header_.file);
            config.channel = ((str_ch_mod & 0x000000FF));
            config.module = ((str_ch_mod & 0x0000FF00) >> 8);
            config.chassis = ((str_ch_mod & 0x00FF0000) >> 16);
            config.stream = ((str_ch_mod & 0xFF000000) >> 24);
            loc += sizeof(int32_t);

            switch (getRevisionNumber())
            {
            case 0:
                // TODO find a better way to do this
                config.sampleRate = (float) ntoflib_
                                        .getReader(header_.isOnCastor)
                                        ->readInt32(loc, header_.file);
                loc += sizeof(uint32_t);
                break;
            case 1:
            case 2:
            case 3:
            case 4:
                config.sampleRate = ntoflib_.getReader(header_.isOnCastor)
                                        ->readFloat(loc, header_.file);
                loc += sizeof(uint32_t);
                break;
            default:
                throw NtofLibException("Revision number unknown", __FILE__,
                                       __LINE__);
                break;
            }

            switch (getRevisionNumber())
            {
            case 0:
            case 2:
            case 3:
            case 4:
                // TODO find a better way to do this
                config.sampleSize = (float) ntoflib_
                                        .getReader(header_.isOnCastor)
                                        ->readInt32(loc, header_.file);
                loc += sizeof(uint32_t);
                break;
            case 1:
                config.sampleSize = ntoflib_.getReader(header_.isOnCastor)
                                        ->readFloat(loc, header_.file);
                loc += sizeof(uint32_t);
                break;
            default:
                throw NtofLibException("Revision number unknown", __FILE__,
                                       __LINE__);
                break;
            }

            switch (getRevisionNumber())
            {
            case 0:
            case 1:
            case 2:
                config.fullScale = ntoflib_.getReader(header_.isOnCastor)
                                       ->readUint32(loc, header_.file);
                break;
            case 3:
            case 4:
                config.fullScale = ntoflib_.getReader(header_.isOnCastor)
                                       ->readFloat(loc, header_.file);
                break;
            default:
                throw NtofLibException("Revision number unknown", __FILE__,
                                       __LINE__);
                break;
            }
            loc += sizeof(uint32_t);

            config.delayTime = ntoflib_.getReader(header_.isOnCastor)
                                   ->readInt32(loc, header_.file);
            loc += sizeof(uint32_t);
            switch (getRevisionNumber())
            {
            case 0:
            case 2:
            case 3:
            case 4:
                config.thresholdADC = ntoflib_.getReader(header_.isOnCastor)
                                          ->readInt32(loc, header_.file);
                config.thresholdMV = config.thresholdADC *
                    ((float) getFullScaleInMilliVolts(config.fullScale) /
                     (float) getADCRange(config.moduleType));
                loc += sizeof(uint32_t);
                break;
            case 1: {
                config.thresholdMV = ntoflib_.getReader(header_.isOnCastor)
                                         ->readFloat(loc, header_.file);
                float result = (config.thresholdMV *
                                (float) getADCRange(config.moduleType)) /
                    (float) getFullScaleInMilliVolts(config.fullScale);
                //                            std::cerr << "config.thresholdMV =
                //                            " << config.thresholdMV <<
                //                            std::endl; std::cerr <<
                //                            "(float)getADCRange(config.moduleType)
                //                            = " <<
                //                            (float)getADCRange(config.moduleType)
                //                            << std::endl; std::cerr <<
                //                            "getFullScaleInMilliVolts(config.fullScale)
                //                            = " <<
                //                            getFullScaleInMilliVolts(config.fullScale)
                //                            << std::endl; std::cerr << "Result
                //                            = " << result << std::endl;
                //                            std::cerr <<
                //                            "**********************************"
                //                            << std::endl;
                config.thresholdADC = (int32_t) result;
                loc += sizeof(uint32_t);
            }
            break;
            default:
                throw NtofLibException("Revision number unknown", __FILE__,
                                       __LINE__);
                break;
            }

            config.thresholdSign = ntoflib_.getReader(header_.isOnCastor)
                                       ->readInt32(loc, header_.file);
            loc += sizeof(uint32_t);
            config.offset = ntoflib_.getReader(header_.isOnCastor)
                                ->readFloat(loc, header_.file);

            // update of the threshold
            switch (getRevisionNumber())
            {
            case 0:
            case 2:
            case 3:
            case 4: config.thresholdMV -= config.offset; break;
            case 1:
                config.thresholdADC += config.offset *
                    ((float) getADCRange(config.moduleType) /
                     (float) getFullScaleInMilliVolts(config.fullScale));
                break;
            default:
                throw NtofLibException("Revision number unknown", __FILE__,
                                       __LINE__);
                break;
            }

            loc += sizeof(uint32_t);
            config.preSample = ntoflib_.getReader(header_.isOnCastor)
                                   ->readUint32(loc, header_.file);
            loc += sizeof(uint32_t);
            config.postSample = ntoflib_.getReader(header_.isOnCastor)
                                    ->readUint32(loc, header_.file);
            loc += sizeof(uint32_t);
            config.clockState = ntoflib_.getReader(header_.isOnCastor)
                                    ->readString(loc, 4, header_.file);
            loc += sizeof(uint32_t);

            switch (getRevisionNumber())
            {
            case 0: config.zeroSuppressionStart = 0; break;
            case 1:
            case 2:
            case 3:
            case 4:
                config.zeroSuppressionStart = ntoflib_
                                                  .getReader(header_.isOnCastor)
                                                  ->readUint32(loc,
                                                               header_.file);
                loc += sizeof(uint32_t);
                break;
            default:
                throw NtofLibException("Revision number unknown", __FILE__,
                                       __LINE__);
                break;
            }

            switch (getRevisionNumber())
            {
            case 0:
            case 1:
            case 2:
            case 3: config.masterDetectorType = ""; break;
            case 4:
                config.masterDetectorType = ntoflib_
                                                .getReader(header_.isOnCastor)
                                                ->readString(loc, 4,
                                                             header_.file);
                loc += sizeof(uint32_t);
                break;
            default:
                throw NtofLibException("Revision number unknown", __FILE__,
                                       __LINE__);
                break;
            }

            switch (getRevisionNumber())
            {
            case 0:
            case 1:
            case 2:
            case 3: config.masterDetectorId = 0; break;
            case 4:
                config.masterDetectorId = ntoflib_
                                              .getReader(header_.isOnCastor)
                                              ->readUint32(loc, header_.file);
                loc += sizeof(uint32_t);
                break;
            default:
                throw NtofLibException("Revision number unknown", __FILE__,
                                       __LINE__);
                break;
            }

            // Jump the reserved elements
            switch (getRevisionNumber())
            {
            case 4: loc += (sizeof(uint32_t) * 5); break;
            default: break;
            }

            //                    std::cout << "detectorType = " <<
            //                    config.detectorType << std::endl; std::cout <<
            //                    "detectorId = " << config.detectorId <<
            //                    std::endl; std::cout << "moduleType = " <<
            //                    config.moduleType << std::endl; std::cout <<
            //                    "channel = " << config.channel << std::endl;
            //                    std::cout << "module = " << config.module <<
            //                    std::endl; std::cout << "chassis = " <<
            //                    config.chassis << std::endl; std::cout <<
            //                    "stream = " << config.stream << std::endl;
            //                    std::cout << "sampleRate = " <<
            //                    config.sampleRate << std::endl; std::cout <<
            //                    "sampleSize = " << config.sampleSize <<
            //                    std::endl; std::cout << "fullScale = " <<
            //                    config.fullScale << std::endl; std::cout <<
            //                    "delayTime = " << config.delayTime <<
            //                    std::endl; std::cout << "thresholdADC = " <<
            //                    config.thresholdADC << std::endl; std::cout <<
            //                    "thresholdMV = " << config.thresholdMV <<
            //                    std::endl; std::cout << "thresholdSign = " <<
            //                    config.thresholdSign << std::endl; std::cout
            //                    << "offset = " << config.offset << std::endl;
            //                    std::cout << "preSample = " <<
            //                    config.preSample << std::endl; std::cout <<
            //                    "postSample = " << config.postSample <<
            //                    std::endl; std::cout << "clockState = " <<
            //                    config.clockState << std::endl; std::cout <<
            //                    "zeroSuppressionStart = " <<
            //                    config.zeroSuppressionStart << std::endl;

            listConfig_.push_back(config);
        }
    }
    channelId_ = -1;
    Notification::notify(ntoflib_.notificationListener(),
                         Notification::MODH_INIT);
    return listConfig_;
}

/*
 * \brief Get the index of the channel configuration
 * \return Return the index value
 */
uint32_t ReaderStructMODH::getIndex() const
{
    return channelId_;
}

/*
 * \brief Set the id of the channel to access to the configuration data
 * \param id channel's index
 */
void ReaderStructMODH::setIndex(uint32_t id)
{
    channelId_ = id;
}

/*
 * \brief Allow to know the number of ACQC by EVEH expected
 * \return Return the number of ACQC by EVEH
 */
int32_t ReaderStructMODH::getNumberOfAcqcByEvent()
{
    if (acqcExpected_ == -1)
    {
        uint32_t streamId = ntoflib_.getRCTR()->getStreamNumber();
        acqcExpected_ = 0;
        for (std::list<CHAN_CONFIG>::iterator it = listConfig_.begin();
             it != listConfig_.end(); ++it)
        {
            if ((*it).stream == streamId)
            {
                ++acqcExpected_;
            }
        }
    }
    return acqcExpected_;
}

/*
 * \brief Get the details of the actual header
 * \return Return the details of the header
 */
const Header &ReaderStructMODH::getHeader() const
{
    return header_;
}

/*
 * \brief Set the details of the header
 * \param header: new header used to replace the old one
 */
void ReaderStructMODH::setHeader(Header &header)
{
    header_ = header;
}
} /* namespace lib */
} /* namespace ntof */
