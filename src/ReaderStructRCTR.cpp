/*
 * ReaderStructRCTR.cpp
 *
 *  Created on: Jan 12, 2015
 *      Author: agiraud
 */

#include "ReaderStructRCTR.h"

#include <sstream>
#include <string>

#include "HeaderManager.h"
#include "NtofLib.h"
#include "Reader.h"

namespace ntof {
namespace lib {

/*
 * \brief Constructor
 * \param header Details of the header
 * \param ntoflib give access to the reader
 */
ReaderStructRCTR::ReaderStructRCTR(const Header &header, NtofLib &ntoflib) :
    ntoflib_(ntoflib),
    startPosition_(header.pos),
    revisionNumber_(-1),
    runNumber_(-1),
    extensionNumber_(-1),
    header_(header)
{}

/*
 * \brief Destructor
 */
ReaderStructRCTR::~ReaderStructRCTR() {}

/*
 * \brief Get the title
 * \return Return the title.
 */
std::string ReaderStructRCTR::getTitle()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readString(startPosition_, 4, header_.file);
}

/*
 * \brief Get the revision number
 * \return Return the revision number.
 */
int32_t ReaderStructRCTR::getRevisionNumber()
{
    if (revisionNumber_ == -1)
    {
        revisionNumber_ = ntoflib_.getReader(header_.isOnCastor)
                              ->readUint32(
                                  startPosition_ + (sizeof(int32_t) * 1),
                                  header_.file);
    }
    return revisionNumber_;
}

/*
 * \brief Get the reserved
 * \return Return the reserved.
 */
uint32_t ReaderStructRCTR::getReserved()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + (sizeof(uint32_t) * 2), header_.file);
}

/*
 * \brief Get the length
 * \return Return the length.
 */
uint32_t ReaderStructRCTR::getLength()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + (sizeof(uint32_t) * 3), header_.file);
}

/*
 * \brief Get the run number
 * \return Return the run number.
 */
int32_t ReaderStructRCTR::getRunNumber()
{
    if (runNumber_ == -1)
    {
        runNumber_ = ntoflib_.getReader(header_.isOnCastor)
                         ->readUint32(startPosition_ + (sizeof(int32_t) * 4),
                                      header_.file);
    }
    return runNumber_;
}

/*
 * \brief Get the event number
 * \return Return the event number.
 */
int32_t ReaderStructRCTR::getExtensionNumber()
{
    if (extensionNumber_ == -1)
    {
        if (getRevisionNumber() < 3)
        {
            std::string filename = ntoflib_.getFilename();
            std::size_t found = filename.rfind("/");
            std::string name;
            if (found != std::string::npos)
            {
                name = filename.substr(found + 1, filename.size() - found);
            }
            else
            {
                name = filename;
            }
            // std::cerr << "The name of the file is : " << name << std::endl;

            size_t posStart;
            posStart = name.find("_");
            if (posStart != std::string::npos)
            {
                size_t posEnd;
                if (ntoflib_.isIDX())
                {
                    posEnd = name.find(".", posStart + 1);
                }
                else
                {
                    posEnd = name.find("_", posStart + 1);
                }
                if (posEnd != std::string::npos)
                {
                    std::string extensionNumber = name.substr(
                        (posStart + 1), posEnd - (posStart + 1));
                    // std::cerr << "The extension number is : " <<
                    // extensionNumber << std::endl;

                    std::istringstream buffer(extensionNumber);
                    buffer >> extensionNumber_;
                }
            }
        }
        else
        {
            extensionNumber_ = ntoflib_.getReader(header_.isOnCastor)
                                   ->readUint32(
                                       startPosition_ + (sizeof(uint32_t) * 5),
                                       header_.file);
        }
    }
    return extensionNumber_;
}

/*
 * \brief Get the stream number
 * \return Return the stream number.
 */
uint32_t ReaderStructRCTR::getStreamNumber()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + (sizeof(uint32_t) * 6), header_.file);
}

/*
 * \brief Get the experiment
 * \return Return the experiment.
 */
std::string ReaderStructRCTR::getExperiment()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readString(startPosition_ + (sizeof(uint32_t) * 7), 4, header_.file);
}

/*
 * \brief Get the date of start
 * \return Return the date of start.
 */
uint32_t ReaderStructRCTR::getDate()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + (sizeof(uint32_t) * 8), header_.file);
}

/*
 * \brief Get the time of start
 * \return Return the time of start.
 */
uint32_t ReaderStructRCTR::getTime()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + (sizeof(uint32_t) * 9), header_.file);
}

/*
 * \brief Get the number of modules
 * \return Return the number of modules.
 */
uint32_t ReaderStructRCTR::getNumberOfModules()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + (sizeof(uint32_t) * 10), header_.file);
}

/*
 * \brief Get the number of channels
 * \return Return the number of channels.
 */
uint32_t ReaderStructRCTR::getNumberOfChannels()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + (sizeof(uint32_t) * 11), header_.file);
}

/*
 * \brief Get the numbers of streams
 * \return Return the numbers of streams.
 */
uint32_t ReaderStructRCTR::getTotalNumberOfStreams()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + (sizeof(uint32_t) * 12), header_.file);
}

/*
 * \brief Get the number of chassis
 * \return Return the number of chassis.
 */
uint32_t ReaderStructRCTR::getTotalNumberOfChassis()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + (sizeof(uint32_t) * 13), header_.file);
}

/*
 * \brief Get the number of modules
 * \return Return the number of modules.
 */
uint32_t ReaderStructRCTR::getTotalNumberOfModules()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + (sizeof(uint32_t) * 14), header_.file);
}

/*
 * \brief Get the list of the channel used by module
 * \return Return the list of the channel used by module.
 */
std::list<uint32_t> ReaderStructRCTR::getNumberOfUsedChannelByModule()
{
    std::list<uint32_t> channelByModule;
    uint32_t nbModule = getTotalNumberOfModules();
    for (uint32_t i = 0; i < nbModule; ++i)
    {
        channelByModule.push_back(
            ntoflib_.getReader(header_.isOnCastor)
                ->readUint32(startPosition_ + (sizeof(uint32_t) * (15 + i)),
                             header_.file));
    }
    return channelByModule;
}

/*
 * \brief Get the details of the actual header
 * \return Return the details of the header
 */
const Header &ReaderStructRCTR::getHeader() const
{
    return header_;
}

/*
 * \brief Set the details of the header
 * \param header: new header used to replace the old one
 */
void ReaderStructRCTR::setHeader(Header &header)
{
    header_ = header;
}

} /* namespace lib */
} /* namespace ntof */
