/*
 * ReaderStructSLOW.cpp
 *
 *  Created on: Jan 12, 2015
 *      Author: agiraud
 */

#include "ReaderStructSLOW.h"

#include "HeaderManager.h"
#include "NtofLib.h"
#include "Reader.h"

namespace ntof {
namespace lib {

/*
 * \brief Constructor
 * \param header Details of the header
 * \param ntoflib give access to the reader
 */
ReaderStructSLOW::ReaderStructSLOW(const Header &header, NtofLib &ntoflib) :
    ntoflib_(ntoflib),
    startPosition_(header.pos),
    revisionNumber_(-1),
    header_(header)
{}

/*
 * \brief Destructor
 */
ReaderStructSLOW::~ReaderStructSLOW() {}

/*
 * \brief Get the title
 * \return Return the title.
 */
std::string ReaderStructSLOW::getTitle()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readString(startPosition_, 4, header_.file);
}

/*
 * \brief Get the revision number
 * \return Return the revision number.
 */
uint32_t ReaderStructSLOW::getRevisionNumber()
{
    if (revisionNumber_ == -1)
    {
        revisionNumber_ = ntoflib_.getReader(header_.isOnCastor)
                              ->readUint32(
                                  startPosition_ + (sizeof(int32_t) * 1),
                                  header_.file);
    }
    return revisionNumber_;
}

/*
 * \brief Get the reserved
 * \return Return the reserved.
 */
uint32_t ReaderStructSLOW::getReserved()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + sizeof(int32_t) * 2, header_.file);
}

/*
 * \brief Get the length
 * \return Return the length.
 */
uint32_t ReaderStructSLOW::getLength()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readUint32(startPosition_ + sizeof(int32_t) * 3, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getCT1()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 4, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getCT2()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 5, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getCT3()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 6, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getCT4()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 7, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getCT5()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 8, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getFT1()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 9, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getFT2()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 10, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getO2T1()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 11, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getO2T2()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 12, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPT1()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 13, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPT2()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 14, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPT3()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 15, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPT4()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 16, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPT5()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 17, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPT6()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 18, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPT7()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 19, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPH1()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 20, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPH2()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 21, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPH3()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 22, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPH4()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 23, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getTT2()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 24, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getTT3()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 25, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getTT4()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 26, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getTT5()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 27, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getTT6()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 28, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getTT7()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 29, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getTT8()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 30, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getTT9()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 31, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getTT10()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 32, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPAXTOF01()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 33, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPAXTOF02()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 34, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPAXTOF03()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 35, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPAXTOF04()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 36, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPAXTOF05()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 37, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPMIBL01()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 38, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPMIBL02()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 39, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPMITOF01()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 40, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPMITOF02()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 41, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPMITOF03()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 42, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPMITOF04()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 43, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getPMITOF05()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 44, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getMagnetIntensity()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 45, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
int32_t ReaderStructSLOW::getMagnetStatus()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readInt32(startPosition_ + sizeof(int32_t) * 46, header_.file);
}

// Vacuum definition for EAR1
/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF_VGR_1_PR()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 47, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF_VGR_1_Stat()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 48, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF_VGR_1A_PR()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 49, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF_VGR_1A_Stat()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 50, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF_VGR_2_PR()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 51, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF_VGR_2_Stat()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 52, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF_VGR_2A_PR()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 53, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF_VGR_2A_Stat()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 54, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF_VGR_3_PR()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 55, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF_VGR_3_Stat()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 56, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF_VGR_3A_PR()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 57, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF_VGR_3A_Stat()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 58, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF_VGR_4_PR()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 59, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF_VGR_4_Stat()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 60, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF_VGR_4A_PR()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 61, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF_VGR_4A_Stat()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 62, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF_EXP_PR()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 63, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF_EXP_Stat()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 64, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF_EXP_A_PR()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 65, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF_EXP_A_Stat()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 66, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF_EXP_B_PR()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 67, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF_EXP_B_Stat()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 68, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOFVVS01_Stat()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 69, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOFVVS02_Stat()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 70, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOFVVS01()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 71, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOFVVS02()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 72, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF2VVS01()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 73, header_.file);
}

// Vacuum definition for EAR2
/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF2_VGR_1_PR()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 74, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF2_VGR_2_PR()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 75, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF2_VGR_3_PR()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 76, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getVAC_TOF2_VGR_4_PR()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 77, header_.file);
}

// Exp area temperature:
/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getExpTemp_EAR1_IN()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 78, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getExpTemp_EAR1_OUT()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 79, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getExpTemp_EAR2_IN()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 80, header_.file);
}

/*
 * \brief Get the slow parameter
 * \return Return the slow parameter.
 */
float ReaderStructSLOW::getExpTemp_EAR2_OUT()
{
    return ntoflib_.getReader(header_.isOnCastor)
        ->readFloat(startPosition_ + sizeof(int32_t) * 81, header_.file);
}

/*
 * \brief Get the details of the actual header
 * \return Return the details of the header
 */
const Header &ReaderStructSLOW::getHeader() const
{
    return header_;
}

/*
 * \brief Set the details of the header
 * \param header: new header used to replace the old one
 */
void ReaderStructSLOW::setHeader(Header &header)
{
    header_ = header;
}

} /* namespace lib */
} /* namespace ntof */
