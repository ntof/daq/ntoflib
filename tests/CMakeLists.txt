
if(TESTS)

link_directories(
    ${CMAKE_BINARY_DIR}/src
    ${CPPUNIT_LIBRARY_DIRS})
include_directories(
    ${CMAKE_SOURCE_DIR}/include
    ${CMAKE_BINARY_DIR}
    ${CMAKE_BINARY_DIR}/include)
include_directories(SYSTEM
    ${CPPUNIT_INCLUDE_DIRS}
    ${XROOTD_INCLUDE_DIRS}
    ${Boost_INCLUDE_DIRS})

set(test_SRCS test_main.cc
    test_helpers.hpp test_helpers.cpp
    test_FileCache.cpp
    test_ReaderLocal.cpp
    test_NtofLib.cpp
    test_ReaderStructADDH.cpp
)

if(CASTOR)
    list(APPEND test_SRCS test_ReaderCastorCached.cpp)
endif(CASTOR)

add_executable(test_all ${test_SRCS})
target_link_libraries(test_all ntoflib ${CPPUNIT_LIBRARIES} ${Boost_LIBRARIES} ${XROOTD_LIBRARIES})

add_test(test_all ${CMAKE_CURRENT_BINARY_DIR}/test_all)

endif(TESTS)
