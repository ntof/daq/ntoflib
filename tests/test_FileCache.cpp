/*
** Copyright (C) 2018 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-10-16T09:46:54+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <boost/chrono.hpp>
#include <boost/filesystem.hpp>
#include <boost/interprocess/sync/named_semaphore.hpp>
#include <boost/thread/thread.hpp>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "FileCache.h"

namespace bfs = boost::filesystem;
namespace bip = boost::interprocess;
using namespace ntof::lib;

class TestFileCache : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestFileCache);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(noBaseDir);
    CPPUNIT_TEST(cleanup);
    CPPUNIT_TEST(childExclusive);
    CPPUNIT_TEST_SUITE_END();

    bfs::path m_tmp;
    pid_t m_child;

public:
    void setUp()
    {
        m_tmp = bfs::temp_directory_path();
        m_tmp /= bfs::path("TestFileCache");

        bfs::remove_all(m_tmp);

        m_child = 0;
    }

    void tearDown()
    {
        bfs::remove_all(m_tmp);
        bip::named_semaphore::remove("TestFileCache");
        if (m_child > 0)
        {
            ::kill(m_child, SIGTERM);
            int stat;
            ::waitpid(m_child, &stat, 0);
        }
    }

    void simple()
    {
        FileCache cache(4 * 1024, m_tmp);
        int err = -1;

        FileCache::SharedLock lock = cache.create("test", 1024, err);
        CPPUNIT_ASSERT_EQUAL(0, err);
        CPPUNIT_ASSERT(lock);

        CPPUNIT_ASSERT(bfs::exists(m_tmp / "test"));

        CPPUNIT_ASSERT(!cache.readLock("test", err));
        CPPUNIT_ASSERT_EQUAL(-EAGAIN, err);

        CPPUNIT_ASSERT(!cache.create("test", 1024, err));
        CPPUNIT_ASSERT_EQUAL(-EAGAIN, err);
        lock.reset();

        lock = cache.readLock("test", err);
        CPPUNIT_ASSERT_EQUAL(0, err);
        CPPUNIT_ASSERT(lock);

        FileCache::SharedLock lock2 = cache.readLock("test", err);
        CPPUNIT_ASSERT_EQUAL(0, err);
        CPPUNIT_ASSERT(lock == lock2);
    }

    void cleanup()
    {
        FileCache cache(3 * 1024, m_tmp);
        int err = -1;

        // cppcheck-suppress unreadVariable
        FileCache::SharedLock lock1 = cache.create("test1", 1024, err);
        cache.create("test2", 1024, err); /* this one is not locked */

        // cppcheck-suppress unreadVariable
        FileCache::SharedLock lock3 = cache.create("test3", 1024, err);

        CPPUNIT_ASSERT(bfs::exists(m_tmp / "test2"));
        cache.create("test4", 1024, err);
        CPPUNIT_ASSERT(!bfs::exists(m_tmp / "test2"));
        CPPUNIT_ASSERT(bfs::exists(m_tmp / "test3"));
    }

    void noBaseDir()
    {
        FileCache cache(1024, "");
        int err = 42;

        CPPUNIT_ASSERT(!cache.create("test1", 1024, err));
        CPPUNIT_ASSERT_EQUAL(-ENOENT, err);
        CPPUNIT_ASSERT(!cache.readLock("test1", err));
        CPPUNIT_ASSERT_EQUAL(-ENOENT, err);

        cache.setBaseDir(m_tmp);
        CPPUNIT_ASSERT(cache.create("test1", 1024, err));
    }

    /* child process for childExclusive test */
    void childLocker(bip::named_semaphore &sem)
    {
        FileCache cache(4 * 1024, m_tmp);
        int err;
        // cppcheck-suppress unreadVariable
        FileCache::SharedLock lock1 = cache.create("test1", 1024, err);
        sem.post();
        while (true)
            ::sleep(1);
    }

    /*
     * check that inter-process locks are effective
     */
    void childExclusive()
    {
        bip::named_semaphore::remove("TestFileCache");
        bip::named_semaphore sem(bip::create_only, "TestFileCache", 0);
        m_child = fork();
        if (m_child > 0)
        {
            FileCache cache(4 * 1024, m_tmp);
            sem.wait();

            int err;
            CPPUNIT_ASSERT(!cache.readLock("test1", err));
            CPPUNIT_ASSERT_EQUAL(-EAGAIN, err);
            CPPUNIT_ASSERT(!cache.create("test1", 1024, err));
            CPPUNIT_ASSERT_EQUAL(-EAGAIN, err);

            ::kill(m_child, SIGTERM);
            ::waitpid(m_child, &err, 0);
            m_child = 0;
            CPPUNIT_ASSERT(cache.readLock("test1", err));
        }
        else
        {
            childLocker(sem);
            exit(0);
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestFileCache);
