/*
** Copyright (C) 2018 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-10-16T18:25:32+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <iomanip>
#include <iostream>

#include <boost/chrono.hpp>
#include <boost/filesystem.hpp>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Notification.h"
#include "NtofLib.h"
#include "ReaderCastorCached.h"
#include "test_helpers.hpp"

namespace bfs = boost::filesystem;
namespace bch = boost::chrono;
using namespace ntof::lib;

class ProgressDisplay : public Notification::Listener
{
public:
    ProgressDisplay() : m_state(static_cast<Notification::State>(-1)) {}

    bool onProgress(Notification::State state, size_t progress, size_t total)
    {
        total >>= 10; /* use KB */
        progress >>= 10;
        if (m_state != state)
        {
            std::cout << std::endl;
            if (state == Notification::FILE_FETCH)
                std::cout << "Downloading file: ";
            else if (state == Notification::HEADER_PARSE)
                std::cout << "Parsing headers: ";
            else
                std::cout << "Unknown state (" << state << "): ";

            std::cout << "    ";
        }
        else if (total == 0)
            std::cout << " ..." << std::flush;
        else
        {
            std::ios::fmtflags f(std::cout.flags());
            size_t pcent = (total) ? (progress * 100 / total) : 0;
            std::cout << "\b\b\b\b" << std::setfill(' ') << std::setw(3)
                      << pcent << "%" << std::flush;
            std::cout.flags(f);
        }
        m_state = state;
        return true;
    }
    Notification::State m_state;
};

class TestReaderCastorCached : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestReaderCastorCached);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(uncached);
    CPPUNIT_TEST_SUITE_END();

    bfs::path m_tmp;

public:
    void setUp()
    {
        m_tmp = bfs::temp_directory_path();
        m_tmp /= bfs::path("TestReaderCastorCached");

        bfs::remove_all(m_tmp);
    }

    void tearDown() { bfs::remove_all(m_tmp); }

    void simple()
    {
        bch::steady_clock::time_point start;
        boost::chrono::duration<double> dur;

        NtofLib ntoflib;
        ProgressDisplay display;
        CastorBrowser castor;
        CastorBrowser::SharedFileInfo castorFile;

        try
        {
            castorFile = findRecentRecord(castor);
        }
        catch (...)
        {
            std::cerr << "Failed to retrieve castor file --> skipping test !";
            return;
        }

        std::shared_ptr<FileCache> cache(
            new FileCache(1024 * 1024 * 800, m_tmp));
        ReaderCastorCached reader((cache));

        start = bch::steady_clock::now();
        FILE_INFO ffi = ntoflib.getFileInfo(castorFile->fullpath());
        ffi = reader.openFile(ffi, &display);
        CPPUNIT_ASSERT_EQUAL(castorFile->name, ffi.filename);
        CPPUNIT_ASSERT_EQUAL((m_tmp / castorFile->name).string(), ffi.fullpath);
        reader.closeFile(ffi.file);
        dur = bch::steady_clock::now() - start;
        std::cout << std::endl << "Castor file loaded in " << dur << std::endl;
        std::cout << "File size " << bfs::file_size(ffi.fullpath) << std::endl;

        display.m_state = static_cast<Notification::State>(-1);

        /* check that file is in the cache (parsed quickly) */
        start = bch::steady_clock::now();
        ffi = ntoflib.getFileInfo(castorFile->fullpath());
        ffi = reader.openFile(ffi, &display);
        reader.closeFile(ffi.file);
        dur = bch::steady_clock::now() - start;
        std::cout << std::endl << "Cached file loaded in " << dur << std::endl;
        CPPUNIT_ASSERT(dur <= bch::seconds(2));
    }

    void uncached()
    {
        bch::steady_clock::time_point start;
        boost::chrono::duration<double> dur;

        NtofLib ntoflib;
        ProgressDisplay display;
        CastorBrowser castor;
        CastorBrowser::SharedFileInfo castorFile;

        try
        {
            castorFile = findRecentRecord(castor);
        }
        catch (...)
        {
            std::cerr << "Failed to retrieve castor file --> skipping test !";
            return;
        }

        /* no cache on purpose */
        std::shared_ptr<FileCache> cache(
            new FileCache(1024 * 1024 * 800, bfs::path()));
        ReaderCastorCached reader((cache));

        start = bch::steady_clock::now();
        FILE_INFO ffi = ntoflib.getFileInfo(castorFile->fullpath());
        ffi = reader.openFile(ffi, &display);
        CPPUNIT_ASSERT_EQUAL(castorFile->name, ffi.filename);
        CPPUNIT_ASSERT_EQUAL(castorFile->fullpath(), ffi.fullpath);
        reader.closeFile(ffi.file);
        dur = bch::steady_clock::now() - start;
        std::cout << std::endl
                  << "Castor file (uncached) loaded in " << dur << std::endl;
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestReaderCastorCached);
