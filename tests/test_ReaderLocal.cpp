/*
** Copyright (C) 2018 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-10-18T15:27:52+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <iostream>
#include <iterator>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "NtofLib.h"
#include "config.h"
#include "test_helpers.hpp"

using namespace ntof::lib;

class TestReaderLocal : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestReaderLocal);
    CPPUNIT_TEST(index);
    CPPUNIT_TEST_SUITE_END();

    FILE_INFO m_ffi;
    NtofLib *m_lib;

public:
    void setUp()
    {
        m_ffi.file = -1;
        m_lib = new NtofLib;
    }

    void tearDown()
    {
        m_lib->closeAllFiles();
        delete m_lib;
    }

    void index()
    {
        m_lib->readFile(SRCDIR "/tests/data/run110603.idx.finished");
        m_ffi = m_lib->getFileInfo(SRCDIR "/tests/data/run110603.idx.finished");
        CPPUNIT_ASSERT_EQUAL(std::string("run110603.idx.finished"),
                             m_ffi.filename);
        m_lib->toString();
        CPPUNIT_ASSERT(m_lib->isIDX());
        CPPUNIT_ASSERT(m_lib->isDefinedRCTR());
        CPPUNIT_ASSERT(m_lib->isDefinedMODH());
        CPPUNIT_ASSERT(m_lib->isDefinedINDX());

        CPPUNIT_ASSERT(m_lib->getEvent(0));
        CPPUNIT_ASSERT(m_lib->isDefinedEVEH());
        CPPUNIT_ASSERT(m_lib->isDefinedADDH());

        ReaderStructRCTR *RCTR = m_lib->getRCTR();
        CPPUNIT_ASSERT(RCTR);
        CPPUNIT_ASSERT_EQUAL(std::string("RCTR"), RCTR->getTitle());
        CPPUNIT_ASSERT_EQUAL(3, RCTR->getRevisionNumber());
        CPPUNIT_ASSERT_EQUAL((uint32_t) 12, RCTR->getLength());
        CPPUNIT_ASSERT_EQUAL(110603, RCTR->getRunNumber());
        CPPUNIT_ASSERT_EQUAL(std::string("EAR1"), RCTR->getExperiment());

        ReaderStructEVEH *EVEH = m_lib->getEVEH();
        CPPUNIT_ASSERT(EVEH);
        CPPUNIT_ASSERT_EQUAL(std::string("EVEH"), EVEH->getTitle());
        CPPUNIT_ASSERT_EQUAL((uint32_t) 1, EVEH->getRevisionNumber());
        CPPUNIT_ASSERT_EQUAL((uint32_t) 12, EVEH->getLength());
        CPPUNIT_ASSERT_EQUAL((uint32_t) 2545, EVEH->getSizeOfEvent());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestReaderLocal);
