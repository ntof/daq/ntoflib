/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-09-15T11:31:54+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <iostream>
#include <iterator>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "NtofLib.h"
#include "config.h"
#include "test_helpers.hpp"

using namespace ntof::lib;

class TestReaderStructADDH : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestReaderStructADDH);
    CPPUNIT_TEST(dataType);
    CPPUNIT_TEST(getData);
    CPPUNIT_TEST(getDataArray);
    CPPUNIT_TEST_SUITE_END();

    FILE_INFO m_ffi;
    NtofLib *m_lib;

public:
    void setUp()
    {
        m_ffi.file = -1;
        m_lib = new NtofLib;
    }

    void tearDown()
    {
        m_lib->closeAllFiles();
        delete m_lib;
    }

    void dataType()
    {
        EQ(ADDHDATATYPE::TypeFloat, ReaderStructADDH::dataType<float>::value);
        EQ(ADDHDATATYPE::TypeDouble, ReaderStructADDH::dataType<double>::value);
        EQ(ADDHDATATYPE::TypeInt, ReaderStructADDH::dataType<int32_t>::value);
        EQ(ADDHDATATYPE::TypeLong, ReaderStructADDH::dataType<int64_t>::value);
        EQ(ADDHDATATYPE::TypeString,
           ReaderStructADDH::dataType<std::string>::value);
        EQ(static_cast<ADDHDATATYPE>(-1),
           ReaderStructADDH::dataType<uint32_t>::value);
    }

    void getData()
    {
        m_lib->readFiles({SRCDIR "/tests/data/run211816/run211816_12.event",
                          SRCDIR "/tests/data/run211816/run211816.run"});

        ASSERT(m_lib->getEvent(0));
        ASSERT(m_lib->isDefinedADDH());

        ReaderStructADDH *ADDH = m_lib->getADDH();
        ASSERT(ADDH);

        ASSERT(ADDH->getField("BCT372"));
        EQ(ADDHDATATYPE::TypeFloat, ADDH->getType());
        DBL_EQ(0.236200004816055, ADDH->getData<float>(), 1e-4);

        ASSERT(ADDH->getField("HV_slot1_chan0_channelName"));
        EQ(ADDHDATATYPE::TypeString, ADDH->getType());
        EQ(std::string("MESH1"), ADDH->getData<std::string>());

        ASSERT(ADDH->getField("HV_slot5_aqcStamp"));
        EQ(ADDHDATATYPE::TypeLong, ADDH->getType());
        EQ(int64_t(1631032435247551000), ADDH->getData<int64_t>());

        // even if this is an array we can retrieve first value
        ASSERT(ADDH->getField("FTN.BSF484H/Position"));
        EQ(ADDHDATATYPE::TypeDouble, ADDH->getType());
        DBL_EQ(-41.25, ADDH->getData<double>(), 1e-4);
    }

    void getDataArray()
    {
        m_lib->readFiles({SRCDIR "/tests/data/run211816/run211816_12.event",
                          SRCDIR "/tests/data/run211816/run211816.run"});

        ASSERT(m_lib->getEvent(0));
        ASSERT(m_lib->isDefinedADDH());

        ReaderStructADDH *ADDH = m_lib->getADDH();
        ASSERT(ADDH);
        EQ(true, ADDH->getField("FTN.BSF484H/Data"));

        const std::vector<double> ref{
            -144, -141, -152, -92,  -100, -130, -92,  -96,  -136, -116,
            -127, -96,  -149, -124, -101, -79,  -123, -83,  -114, -80,
            -82,  -110, -96,  -72,  -72,  -96,  -110, -118, -120, -71,
            -88,  -58,  -72,  -100, -83,  -38,  -92,  -82,  -108, -36,
            -101, -84,  -64,  -88,  -56,  -96,  -89,  -110};

        { /* basic vector retrival */
            std::vector<double> ret;
            ADDH->getDataArray(ret);
            EQ(ref, ret);
        }

        { /* ensure any container is supported */
            std::list<double> ret;
            ADDH->getDataArray(ret);
            // converting to vector, just for the test
            EQ(ref, std::vector<double>(ret.begin(), ret.end()));
        }

        { /* test insert_iterator api */
            std::vector<double> ret;
            ADDH->getDataIterator(std::back_inserter(ret));
            EQ(ref, ret);
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestReaderStructADDH);
