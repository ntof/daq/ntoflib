/*
** Copyright (C) 2018 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-10-09T13:29:47+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <cstddef>
#include <memory>
#include <set>
#include <sstream>

#include <boost/chrono.hpp>
#include <boost/thread/thread.hpp>

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>

#include "NtofLibConfig.h"

#ifdef CASTOR
#include <XrdCl/XrdClFileSystem.hh>
#include <XrdPosix/XrdPosixXrootd.hh>
#include <XrdPosix/XrdPosixXrootdPath.hh>
#endif

#include <cppunit/TestAssert.h>
#include <unistd.h>

#include "test_helpers.hpp"

#ifdef CASTOR

const std::string CastorBrowser::serverDefault = "castorpublic.cern.ch";
const std::string CastorBrowser::svcClassDefault = "default";

CastorBrowser::CastorBrowser(const std::string &server,
                             const std::string &svcClass) :
    m_fs(0),
    m_server(new std::string(server)),
    m_path(new std::string()),
    m_svcClass(svcClass)
{
    m_fs = new XrdCl::FileSystem(
        XrdCl::URL("xroot://" + server + "/?svcClass=" + svcClass));
    // chdir("/");
}

CastorBrowser::~CastorBrowser()
{
    if (m_fs)
        delete m_fs;
}

void CastorBrowser::chdir(const std::string &dir)
{
    std::string path;
    if (dir.empty())
        path = *m_path;
    else if (dir == "..")
    {
        size_t sz = m_path->find_last_of('/');
        if (sz == std::string::npos)
            throw Exception("Can't find parent directory");
        path = *m_path;
        path.resize(sz);
    }
    else if (dir[0] == '/')
        path = dir;
    else
        path = *m_path + "/" + dir;
    if (path.empty())
        path = "/";
    SharedPath sPath(new std::string(path));
    load(sPath, m_files);
    m_path = sPath;
}

void CastorBrowser::chdir(const SharedFileInfo &info)
{
    if (!info)
        throw Exception("Invalid FileInfo");
    chdir(info->fullpath());
}

void CastorBrowser::prepare(const SharedFileInfo &info)
{
    if (!info)
        throw Exception("Invalid FileInfo");
    std::vector<std::string> files(1, info->fullpath(m_svcClass));
    XrdCl::Buffer *buff;
    XrdCl::XRootDStatus status = m_fs->Prepare(
        files, XrdCl::PrepareFlags::Stage, 0, buff);
    if (!status.IsOK())
        throw Exception(status.ToStr());
    delete buff;
}

struct FileInfoCompare
{
    bool operator()(const CastorBrowser::SharedFileInfo &i1,
                    const CastorBrowser::SharedFileInfo &i2) const
    {
        if (i1->mod != i2->mod)
            return i1->mod < i2->mod;
        else
            return i1->name < i2->name;
    }
};

void CastorBrowser::load(const SharedPath &path, FileInfoList &out)
{
    XrdCl::DirectoryList *dirList;
    XrdCl::XRootDStatus status = m_fs->DirList(
        *path + "?svcClass=" + m_svcClass, XrdCl::DirListFlags::Stat, dirList);
    if (!status.IsOK())
        throw Exception(status.ToStr());

    std::unique_ptr<XrdCl::DirectoryList> dirListPtr(dirList);

    std::set<SharedFileInfo, FileInfoCompare> list;
    XrdCl::DirectoryList::Iterator it;
    for (it = dirList->Begin(); it != dirList->End(); ++it)
    {
        SharedFileInfo info(new FileInfo(m_server, path));
        XrdCl::StatInfo *stat = (*it)->GetStatInfo();
        info->name = (*it)->GetName();
        info->flags = stat->GetFlags();
        info->size = stat->GetSize();
        info->mod = stat->GetModTime();
        list.insert(info);
    }
    out.clear();
    out.insert(out.begin(), list.rbegin(), list.rend());
}

CastorBrowser::SharedFileInfo &CastorBrowser::updateInfo(SharedFileInfo &info)
{
    if (!info)
        throw Exception("Invalid FileInfo");
    XrdCl::StatInfo *stat;
    XrdCl::XRootDStatus status = m_fs->Stat(info->fullpath(m_svcClass), stat);

    if (!status.IsOK())
        throw Exception(status.ToStr());

    info->flags = stat->GetFlags();
    info->size = stat->GetSize();
    info->mod = stat->GetModTime();
    return info;
}

CastorBrowser::FileInfo::FileInfo(const CastorBrowser::SharedPath &server,
                                  const CastorBrowser::SharedPath &path) :
    server(server), path(path), flags(0), size(0), mod(0)
{}

std::string CastorBrowser::FileInfo::fullpath(const std::string &svcClass) const
{
    if (svcClass.empty())
        return *path + "/" + name;
    else
        return *path + "/" + name + "?svcClass=" + svcClass;
}

std::string CastorBrowser::FileInfo::url(const std::string &svcClass) const
{
    return "xroot://" + *server + "/" + fullpath(svcClass);
}

bool CastorBrowser::FileInfo::isDir() const
{
    return flags & XrdCl::StatInfo::IsDir;
}

bool CastorBrowser::FileInfo::isOffline() const
{
    return flags & XrdCl::StatInfo::Offline;
}

CastorBrowser::Exception::Exception(const std::string &what) : m_what(what) {}

const char *CastorBrowser::Exception::what() const throw()
{
    return m_what.c_str();
}

static bool findStreamDir(CastorBrowser &castor)
{
    CastorBrowser::FileInfoList project = castor.files();

    for (CastorBrowser::FileInfoList::const_iterator projectIt = project.begin();
         projectIt != project.end(); ++projectIt)
    {
        CastorBrowser::SharedFileInfo projectInfo = *projectIt;

        if (!projectInfo->isDir())
            continue;

        std::cerr << "Entering " << projectInfo->fullpath() << std::endl;
        castor.chdir(projectInfo->name);

        CastorBrowser::FileInfoList acq = castor.files();
        for (CastorBrowser::FileInfoList::const_iterator acqIt = acq.begin();
             acqIt != acq.end(); ++acqIt)
        {
            CastorBrowser::SharedFileInfo acqInfo = *acqIt;

            if (!acqInfo->isDir())
                continue;

            std::cerr << "Entering " << acqInfo->fullpath() << std::endl;
            castor.chdir(acqInfo->name);
            if (std::any_of(castor.files().begin(), castor.files().end(),
                            [](CastorBrowser::SharedFileInfo &info) {
                                return info->name == "stream1";
                            }))
            {
                castor.chdir("stream1");
                if (!castor.files().empty())
                    return true;
                else
                    castor.chdir("..");
            }
            castor.chdir("..");
        }
        castor.chdir("..");
    }
    return false;
}

CastorBrowser::SharedFileInfo findRecentRecord(CastorBrowser &castor)
{
    std::cerr << "Looking for file on castor" << std::endl;
    castor.chdir("/castor/cern.ch/ntof");
    CPPUNIT_ASSERT(!castor.files().empty());
    castor.chdir(castor.files().front()->name);
    castor.chdir("ear1");

    CPPUNIT_ASSERT(findStreamDir(castor));

    CastorBrowser::SharedFileInfo info = castor.files().front();
    CPPUNIT_ASSERT(info);
    castor.updateInfo(info);
    std::cerr << "Selected file " << info->fullpath() << std::endl;
    if (info->isOffline())
    {
        std::cerr << "Preparing file" << std::endl;
        castor.prepare(info);
        for (int i = 0; info->isOffline() && (i < 2 * 60 * 5);
             ++i) /* wait up to 5 minutes */
        {
            std::cerr << "Waiting for file to be ready ..." << std::endl;
            boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
            castor.updateInfo(info);
        }
        CPPUNIT_ASSERT(!info->isOffline());
    }
    std::cerr << "File ready: " << info->fullpath() << std::endl;
    return info;
}

#endif
