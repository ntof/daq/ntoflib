/*
** Copyright (C) 2018 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-10-09T13:27:50+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef TEST_HELPERS_HPP__
#define TEST_HELPERS_HPP__

#include <exception>
#include <list>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include <cppunit/Portability.h>
#include <cppunit/TestAssert.h>

#include "NtofLibConfig.h"

#define EQ CPPUNIT_ASSERT_EQUAL
#define ASSERT CPPUNIT_ASSERT
#define ASSERT_THROW CPPUNIT_ASSERT_THROW
#define DBL_EQ CPPUNIT_ASSERT_DOUBLES_EQUAL

CPPUNIT_NS_BEGIN

#define CPPUNIT_TRAITS_OVERRIDE(TYPE) \
    template<>                        \
    std::string assertion_traits<TYPE>::toString(const TYPE &t)

    template<typename T>
    struct assertion_traits<std::vector<T>>
    {
        static bool equal(const std::vector<T> &x, const std::vector<T> &y)
        {
            return x == y;
        }

        static std::string toString(const std::vector<T> &t)
        {
            std::ostringstream oss;

            oss << "std::vector({";
            bool first = true;
            for (std::int32_t i : t)
            {
                if (first)
                    oss << i;
                else
                    oss << ", " << i;
                first = false;
            }
            oss << "})";
            return oss.str();
        }
    };

CPPUNIT_NS_END

#ifdef CASTOR

namespace XrdCl {
class FileSystem;
}

class CastorBrowser
{
public:
    CastorBrowser(const std::string &server = serverDefault,
                  const std::string &svcClass = svcClassDefault);
    ~CastorBrowser();

    static const std::string serverDefault;
    static const std::string svcClassDefault;

    typedef std::shared_ptr<std::string> SharedPath;

    /**
     * @brief file information
     */
    struct FileInfo
    {
        FileInfo(const SharedPath &server, const SharedPath &path);

        std::string name;
        const std::shared_ptr<std::string> server;
        const std::shared_ptr<std::string> path;
        uint32_t flags;
        uint64_t size;
        uint64_t mod;

        std::string fullpath(const std::string &svcClass = std::string()) const;
        std::string url(const std::string &svcClass = svcClassDefault) const;
        bool isDir() const;
        bool isOffline() const;
    };

    typedef std::shared_ptr<FileInfo> SharedFileInfo;
    typedef std::list<SharedFileInfo> FileInfoList;

    /**
     * @brief get current path
     */
    inline const std::string &path() const { return *m_path; };

    /**
     * @brief list files and directories
     */
    inline const FileInfoList &files() const { return m_files; };
    inline FileInfoList &files() { return m_files; };

    /**
     * @brief change directory
     * @param[in] directory where to go
     *
     * @details if directory starts with a "/" the path is assumed to be absolute
     *
     * @details special value ".." goes up one directory
     */
    void chdir(const std::string &directory);
    void chdir(const SharedFileInfo &file);

    /**
     * @brief send a file staging request
     * @param[in] file the file to check
     */
    void prepare(const SharedFileInfo &file);

    /**
     * @brief stat a file and update its information
     * @param[in,out]  file the file to stat
     * @return file
     *
     * @details Offline information is not set when listing a directory, thus
     * it must be upadted with this method.
     */
    SharedFileInfo &updateInfo(SharedFileInfo &file);

    class Exception : public std::exception
    {
    public:
        explicit Exception(const std::string &what);

        const char *what() const throw();

    protected:
        std::string m_what;
    };

private:
    explicit CastorBrowser(const CastorBrowser &other);
    CastorBrowser &operator=(const CastorBrowser &other);

protected:
    void load(const SharedPath &path, FileInfoList &out);

    XrdCl::FileSystem *m_fs;
    FileInfoList m_files;
    SharedPath m_server;
    SharedPath m_path;
    std::string m_svcClass;
};

CastorBrowser::SharedFileInfo findRecentRecord(CastorBrowser &browser);

#endif

#endif // TEST_HELPERS_HPP__
